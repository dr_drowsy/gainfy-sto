const civicSip = require('civic-sip-api');
const express = require('express');
const fs = require('fs');
const https = require('https');
const path = require('path');
const mysql = require('mysql');

var connection = mysql.createConnection({
  host: process.env.DBCONNECTION,
  port:  process.env.DBPORT,
  user:  process.env.USER,
  password:  process.env.PASS,
  database:  process.env.DATABASE
})

const app = express()
const port = 3000;

const httpsOptions = {
cert: fs.readFileSync(process.env.SSLCERT),
key: fs.readFileSync(process.env.SSLKEY)
}
const server = https.createServer(httpsOptions, app);

server.on('request', (request, response) => {

const {headers, method, url} = request;
let jwtToken = [];

request.on('error', (err) => {
    console.error(err);
  }).on('data', (chunk) => {
    jwtToken.push(chunk);

  }).on('end', () => {
    jwtToken = Buffer.concat(jwtToken).toString();
    console.log("jwtToken: ", jwtToken);
    var removeJwt = jwtToken.split("=");
    //console.log("removeJwt: ",removeJwt);
    var removeUserID =removeJwt[1].split("%25%25%25%25");
//    console.log("removeUserID: ",removeUserID);

    var userID = removeUserID[0];
    jwtToken = removeUserID[1].toString();
    //console.log("User ID: ", userID);
    console.log("Received a token: ",jwtToken);


    //console.log("UserID: ",removeUserID[0]);
   //console.log('Token Received: ', removeUserID[1]);
    // At this point, we have the headers, method, url and body, and can now
    // do whatever we need to in order to respond to this request.
    const civicClient = civicSip.newClient({
      appId: 'WrAWOowL_',
      prvKey: process.env.PRIVATE_KEY,
      appSecret: process.env.APP_SECRET,
    });

    // Step 5: Exchange authorization code for user data.
    civicClient.exchangeCode(jwtToken)
        .then((userData) => {
            // store user data and userId as appropriate
            var userDataStr = JSON.stringify(userData,null,4);
            var userDataObj = JSON.parse(userDataStr);

            var uniqueID= userID;
            var vLevel = userDataObj.data[0].value;
            var idType = userDataObj.data[1].value;
            var idNumber = userDataObj.data[2].value;
            var idName = userDataObj.data[3].value;
            var idDOB =  userDataObj.data[4].value;
            var idDOI = userDataObj.data[5].value;
            var idDOE = userDataObj.data[6].value;
            var idImage = userDataObj.data[7].value;
            var idImageMd5 = userDataObj.data[8].value;
            var country = userDataObj.data[9].value;
            var email = userDataObj.data[10].value;
            var phone = userDataObj.data[11].value;
            console.log('returned information: ');
            console.log('User Id: ', uniqueID);
            console.log('Verification Level: ', vLevel);
            console.log('ID Type: ', idType);
            console.log('ID Number: ', idNumber);
            console.log('ID Name: ', idName);
            console.log('ID Date of Birth: ', idDOB);
            console.log('ID Date of Issue: ', idDOI);
            console.log('ID Date of Expiration: ', idDOE);
            //console.log('ID base 64: ', idImage);
            console.log('ID md5: ', idImageMd5);
            console.log('ID Country: ', country);
            console.log('Email: ', email);
            console.log('Phone Number: ', phone);

            // const check = 'Select VerificationLevel Where UniqueID = ?'
            // connection.query(check, uniqueID, function(err, result) {
            //
            //

            const queryPart1 = 'INSERT INTO KycCivic (`UniqueID`,`VerificationLevel`,`IdType`,`IdNumber`,`IdName`,`IdDOB`,`IdDOI`,`IdDOE`,`IdImage`,`idImageMd5`,`Country`,`email`,`phone`)'
            const queryPart2 = 'Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
            connection.query((queryPart1+queryPart2),[uniqueID, vLevel,  idType, idNumber, idName, idDOB, idDOI, idDOE, idImage, idImageMd5, country, email, phone], function(err, result) {
                //if (err) throw err
                    });
            //    });
        }).catch((error) => {
            console.log(error);
        });
  });
}).listen(3000, () => {
  console.log(`Server running at https:18.223.144.105 port 3001`);
});

//const app = require('express');



// Step 4: Initialize instance passing your appId and secret.
