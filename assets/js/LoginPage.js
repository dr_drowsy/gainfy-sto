(function($) {

    'use strict';

    // Check that the Firebase app has not already been initialized
    if (!firebase.apps.length) {
        console.warn("Firebase not initialized!");
    }

    const db = firebase.firestore();
    // // Turn off deprecated settings
    db.settings({
        timestampsInSnapshots: true
    });

    const loginEmail = $('#loginEmail');
    const loginPassword = $('#loginPassword');
    const note = $('#note');
    const noteBox = $('#noteBox');
    const auth = firebase.auth();
    const loginButton = $('#loginButton');

    $(document).ready(() => {
        $(window).keydown((e) => {
            if (e.which === 13) {
                e.preventDefault();
                e.stopPropagation();
                if (loginEmail.val() && loginPassword.val()) {
                    loginButton.click();
                    return false;
                }
            }
        });
    });

    loginButton.click((event) => {
        const email = escapeHtml(loginEmail.val());
        const pass = escapeHtml(loginPassword.val());

        if (auth.currentUser) {
            //If user is already logged in and tries to log in again, just reauthenticate them.
            //Changing accounts will require them to log off first.
            const credential = firebase.auth.EmailAuthProvider.credential(email, pass);
            auth.currentUser.reauthenticateAndRetrieveDataWithCredential(credential).then(() => {
                window.location.replace('index.html');
            }).catch((err) => {
                note.text(err.message || err);
                noteBox.show('inline-block');
            });
        }
        else {
            // IMPORTANT: Setting Firebase auth persistence. SESSION will log the user out when the current tab or window is closed.
            auth.setPersistence(firebase.auth.Auth.Persistence.SESSION).then(() => {
                auth.signInWithEmailAndPassword(email, pass).then(() => {
                    const accountRef = db.collection('accounts').doc(auth.currentUser.uid);
                    accountRef.get().then((snapshot) => {
                        if (snapshot.exists) {
                            const twoFactorEnabled = snapshot.get('two_factor_enabled');
                            if (twoFactorEnabled) {
                                accountRef.set({
                                    two_factor_validated: false,
                                }, {merge: true}).then(() => {
                                    //    Route to 2factor input
                                    window.location.replace('2factor.html');
                                }).catch((err) => {
                                    signOut('login.html', 'Unable to set validation status, try logging in again');
                                });
                            }
                            else {
                                window.location.replace('index.html');
                            }
                        }
                        else {
                            accountRef.set({
                                two_factor_validated: false,
                            }, {merge: true}).then(() => {
                                signOut('login.html', 'Problem fetching user account entry, try logging in again');
                            }).catch((err) => {
                                signOut('login.html', `Problem fetching user account entry, try logging in again --> ${err.message || err}`);
                            });
                        }
                    });
                }).catch((err) => {
                    note.text(err);
                    noteBox.show('inline-block');
                });
            }).catch((err) => {
                // console.warn(`Error ${err.code}, setting user state persistence :: ${err.message || err}`);
            });
        }
    });

    loginEmail.keyup(() => {
        note.text('');
        noteBox.hide('inline-block');
    });

    loginPassword.keyup(() => {
        note.text('');
        noteBox.hide('inline-block');
    });

}(jQuery));
