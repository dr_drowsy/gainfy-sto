if (!firebase.apps.length) {
    const config = {
        apiKey: "AIzaSyDEOgJi9La3__DTHn9ZLeN9TTj_5P9eCh8",
        authDomain: "gainfytokenoffering.firebaseapp.com",
        databaseURL: "https://gainfytokenoffering.firebaseio.com",
        projectId: "gainfytokenoffering",
        storageBucket: "gainfytokenoffering.appspot.com",
        messagingSenderId: "777830695232"
    };
    // Initialize Firebase
    firebase.initializeApp(config);
}

// To get firebase user object, just call firebaseUser.then((firebaseUser) => { user user object here } );
let firebaseUser = new Promise((resolve, reject) => {
    firebase.auth().onAuthStateChanged((user) => {
        if (user)
            resolve(user);
    })
});