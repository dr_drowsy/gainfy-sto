(($) => {

    const ethApiUrl = "https://api.coinbase.com/v2/prices/ETH-USD/spot";
    const btcApiUrl = "https://api.coinbase.com/v2/prices/BTC-USD/spot";

    const getExchangeRate = (currency, callback = null) => {
        const ajax = $.ajax({
            url : (currency === 'btc' ? btcApiUrl : currency === 'eth' ? ethApiUrl : null),
            dataType : 'json',
            method : 'GET',
            async : true,
            cache : false,
        });
        ajax.fail((response) => {
            if (!exchangeRate) {
                hasRate = false;
                if (window.confirm("Problem fetching the market price of your selected currency. Click OK to refresh the page and try again, or CANCEL to close this alert.")) {
                    window.location.reload();
                }
            }
        });
        ajax.done((response) => {
            if (callback)
                callback(response.data.amount);
            hasRate = true;
            return response.data.amount;
        });
    };

    const ethPaymentMethod = $('#pay-eth'),
        btcPaymentMethod = $('#pay-btc'),
        // usdPaymentMethod = $('#payusd'),
        paymentCalcSpan = $('#num-payment-span'),
        currentRateSpan = $('#currentRateSpan'),
        desiredTokens = $('#num-tokens'),
        requiredContribution = $('#num-payment'),
        contributionSummary = $('#contribution-summary'),
        bonusSummary = $('#contribution-bonus'),
        tokensReservedSummary = $('#contribution-reserved-tokens'),
        agree = $('#agree'),
        makeContributionBtn = $('#make-contribution-btn'),
        tokensTooLow = $('#token-too-low'),
        tokensTooHigh = $('#token-too-high'),
        contrTooLow = $('#cont-too-low'),
        contrTooHigh = $('#cont-too-high'),
        ethModal = $('#eth-modal'),
        btcModal = $('#btc-modal'),
        btcModalBody = $('#btc-modal-body'),
        ethTotalReserved = $('#eth-total-reserved'),
        ethExpectedBonus = $('#eth-expected-bonus'),
        ethExpectedContr = $('#eth-expected-contribution'),
        ethExpectedRate = $('#eth-expected-rate'),
        ethTranxQrCode = $('#eth-tranx-qr-code'),
        ethAssignedAddr = $('#eth-assigned-address'),
        btcTotalReserved = $('#btc-total-reserved'),
        btcExpectedBonus = $('#btc-expected-bonus'),
        btcExpectedContr = $('#btc-expected-contribution'),
        btcExpectedRate = $('#btc-expected-rate'),
        btcTranxQrCode = $('#btc-tranx-qr-code'),
        btcAssignedAddr = $('#btc-assigned-address'),
        contributionTtl = $('#contribution-time-to-live');

    contributionTtl.text(`${Constants.CONTRIBUTION_TTL} day(s)`);

    const minContributionUsd = Constants.MIN_CONTRIBUTION_USD,
        maxContributionUsd = Constants.MAX_CONTRIBUTION_USD,
        usdToGainRate = Constants.USD_TO_GAIN,
        gainToUsdRate = Constants.GAIN_TO_USD,
        bonusRate = Constants.BONUS_RATE,
        rateQueryTimeout = Constants.RATE_QUERY_TIMEOUT; // 60 Seconds


    //Create Globals
    const type = {
        eth : 'eth',
        btc : 'btc',
    };
    let currencyType = type.eth;
    let exchangeRate;
    let tokens = Constants.DEFAULT_CONTRIBUTION;
    let bonus = tokens * bonusRate;
    let reserved = tokens + bonus;
    let ethPress = false, btcPress = false, hasRate = false, isChecked = true, withinBounds = true;
    const isButtonReady = () => {
        if ((ethPress || btcPress) && hasRate && isChecked && withinBounds)
            makeContributionBtn.enable(true);
        else
            makeContributionBtn.enable(false);
    };
    const updateRate = () => {
        getExchangeRate(currencyType, (response) => {
            exchangeRate = response;
            requiredContribution.val(((tokens * gainToUsdRate) / exchangeRate).toFixed(7));
            currentRateSpan.text(exchangeRate);
            paymentCalcSpan.text(currencyType);
            isButtonReady();
        });
    };
    updateRate();
    setInterval(updateRate, rateQueryTimeout);

    //Set Defaults
    isButtonReady();
    // makeContributionBtn.prop( "disabled", true );
    desiredTokens.val(tokens);
    paymentCalcSpan.text(currencyType);
    contributionSummary.text(tokens * gainToUsdRate);
    bonusSummary.text(bonus);
    tokensReservedSummary.text(reserved);

    //Set Global Events
    desiredTokens.keyup((event) => {
        const val = parseFloat(escapeHtml(event.target.value));
        contrTooLow.hide();
        contrTooHigh.hide();

        if (val >= (minContributionUsd * usdToGainRate)) {
            withinBounds = true;
            tokensTooLow.hide();
            tokensTooHigh.hide();
            tokens = val;
            bonus = (tokens * bonusRate);
            reserved = tokens + bonus;

            const cryptoAmount = (tokens * gainToUsdRate) / exchangeRate;
            requiredContribution.val(cryptoAmount.toFixed(7));
            contributionSummary.text((tokens * gainToUsdRate).toFixed(2));
            bonusSummary.text(bonus.toFixed(2));
            tokensReservedSummary.text(reserved.toFixed(2));
            isButtonReady();
        }
        else {
            withinBounds = false;
            requiredContribution.val('');
            contributionSummary.text('');
            bonusSummary.text('');
            tokensReservedSummary.text('');

            if (!(val > (minContributionUsd * usdToGainRate))) {
                //    Too Low
                tokensTooHigh.hide();
                tokensTooLow.show();
            }
            else {
                //    Too High
                tokensTooLow.hide();
                tokensTooHigh.show();
            }
            isButtonReady();
        }
    });

    requiredContribution.keyup((event) => {
        const val = parseFloat(escapeHtml(event.target.value));
        const valUsd = val * exchangeRate;
        tokensTooLow.hide();
        tokensTooHigh.hide();


        if (valUsd >= minContributionUsd && valUsd <= maxContributionUsd) {
            withinBounds = true;
            contrTooLow.hide();
            contrTooHigh.hide();

            tokens = (valUsd * usdToGainRate);
            bonus = (tokens * bonusRate);
            reserved = parseFloat(tokens + bonus);

            desiredTokens.val(tokens.toFixed(4));
            contributionSummary.text(valUsd.toFixed(2));
            bonusSummary.text(bonus.toFixed(2));
            tokensReservedSummary.text(reserved.toFixed(2));
            isButtonReady();
        }
        else {
            withinBounds = false;
            desiredTokens.val('');
            contributionSummary.text('');
            bonusSummary.text('');
            tokensReservedSummary.text('');

            if (!(valUsd > minContributionUsd)) {
                //    Too Low
                contrTooHigh.hide();
                contrTooLow.show();
            }
            else {
                //    Too High
                contrTooLow.hide();
                contrTooHigh.show();
            }
            isButtonReady();
        }
    });

    //Disable Make Contribution button is terms are not checked
    agree.change(() => {
        isChecked = agree.is(':checked');
        isButtonReady();
    });

    //Promise from firebase.js
    firebaseUser.then((user) => {
        makeContributionBtn.click(() => {
            const expectedTokens = Number(desiredTokens.val()),
                expectedCrypto = Number(requiredContribution.val()),
                expectedUsd = (expectedTokens * gainToUsdRate);

            if (expectedUsd >= minContributionUsd && expectedUsd <= maxContributionUsd) {
                if (isChecked) {
                    submitContributionInfo(currencyType, user.uid, expectedTokens, expectedCrypto);
                }
                else {
                    alert('Please agree to our Terms and Conditions in order to make a contribution.');
                }
            }
            else {
                alert('Your contribution must be between the minimum of 200 GAIN, and the maximum of 200,000 GAIN.');
            }
        });
    });

    const submitContributionInfo = (currencyType, uid, expectedTokens, expectedCrypto) => {
        $.ajax({
            url : '/backend/php/Tokens.php',
            method : 'POST',
            dataType : 'json',
            data : {method : currencyType === type.eth ? 'newEthTranx' : 'newBtcTranx', uid : uid, expectedTokens : expectedTokens, expectedCrypto : expectedCrypto},
            cache : false,
            async : true,
        })
         .done((response) => {
             const data = response.data, log = response.log, error = response.errorMsg;
             const errorCode = response.errorCode || '';

             if (errorCode === ErrorCode.CONTRIBUTION_TOO_LOW) {
                alert('You submitted a contribution that was below the minimum value');
                return;
             }

             const totalReserve = data['totalReserved'], expectedReserve = data['expectedReserve'],
                 expectedBonus = data['expectedBonus'], expectedContribution = data['expectedContribution'],
                 expectedRate = data['expectedRate'], qrCode = data['qrCodeUrl'],
                 assignedWallet = data['assignedWallet'];

             if (currencyType === type.eth) {
                 ethTotalReserved.text(parseFloat(totalReserve).toFixed(4));
                 ethExpectedBonus.text(parseFloat(expectedBonus).toFixed(2));
                 ethExpectedContr.text(parseFloat(expectedContribution).toFixed(4));
                 ethExpectedRate.text(parseFloat(expectedRate).toFixed(2));
                 ethAssignedAddr.val(assignedWallet);
                 ethTranxQrCode.attr('src', qrCode);
                 ethModal.modal('show');
             }
             else if (currencyType === type.btc) {
                 btcTotalReserved.text(parseFloat(totalReserve).toFixed(4));
                 btcExpectedBonus.text(parseFloat(expectedBonus).toFixed(2));
                 btcExpectedContr.text(parseFloat(expectedContribution).toFixed(4));
                 btcExpectedRate.text(parseFloat(expectedRate).toFixed(2));
                 btcAssignedAddr.val(assignedWallet);
                 btcTranxQrCode.attr('src', qrCode);
                 btcModal.modal('show');
             }
         })
         .fail((response) => {
         });

    };

    ethPaymentMethod.click(() => {
        currencyType = type.eth;
        ethPress = true;
        btcPress = false;
        updateRate();
    });

    btcPaymentMethod.click(() => {
        currencyType = type.btc;
        btcPress = true;
        ethPress = false;
        updateRate();
    });

})(jQuery);
