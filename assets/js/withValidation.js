(($) => {
    if (!firebase.apps.length) {
        console.warn("Firebase not initialized!");
        window.location.replace('login.html');
    }

    // // Initialize Firestore
    const db = firebase.firestore();
    // // Turn off deprecated settings
    db.settings({
        timestampsInSnapshots: true
    });

    const auth = firebase.auth();

    auth.onAuthStateChanged((firebaseUser) => {
        if (firebaseUser) {
            const accountRef = db.collection('accounts').doc(firebaseUser.uid);
            accountRef.get().then((snapshot) => {
                if (snapshot.exists) {
                    const twoFactorEnabled = snapshot.get('two_factor_enabled');
                    if (twoFactorEnabled) {
                        const twoFactorValidated = snapshot.get('two_factor_validated');
                        if (!twoFactorValidated) {
                            window.location.replace('2factor.html');
                        }
                    }
                }
                else {
                    console.warn('Error fetching user account entry');
                    signOut('login.html', 'Unable to determine 2factor settings, please try logging in again');
                }
            }).catch((err) => {
                console.warn(`Error getting user account entry --> ${err.message || err}`);
                window.location.reload(true);
            });
        }
        else {
            signOut('login.html', 'You must be logged in to view this page!');
        }
    });

})(jQuery);