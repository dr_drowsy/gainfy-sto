(($) => {
    'use strict';

    // // Initialize Firestore
    const db = firebase.firestore();
    // // Turn off deprecated settings
    db.settings({
        timestampsInSnapshots: true
    });

    const auth = firebase.auth();


    // Ath Open
    const $enableTwoFactor = $('#enable-two-factor'), $ath_content = $('.ath-content'),
        $twoFactorEnabledDisp = $('#two-factor-enabled'), $twoFactorModal = $('#twoFactorModal'), $showTwoFactorInfo = $('#showTwoFactorInfo'),
        $twoFactorSetupComplete = $('#2factor-setup-complete'), $keyValid = $('#key-valid'), $keyInvalid = $('#key-invalid'), $qrCode = $('#qr-code'),
        $encClue = $('#enc-clue'), $validateTwoFactorKey = $('#validate-2factor-key'), $code0 = $('#code-0'), $code1 = $('#code-1'), $code2 = $('#code-2'),
        $code3 = $('#code-3'), $code4 = $('#code-4'), $code5 = $('#code-5'), $codes = [$code0, $code1, $code2, $code3, $code4, $code5],
        $scratchCodeComplete = $('#scratch-codes-btn'), $scratchCodeModal = $('#scratchCodeModal');
    $enableTwoFactor.prop('disabled', true);

     firebaseUser.then((firebaseUser) => {
         if (firebaseUser) {
             let Clue;
             $enableTwoFactor.prop('disabled', false);
             db.collection('accounts').doc(firebaseUser.uid).get().then((snapshot) => {
                 if (snapshot.exists) {
                     const twoFactorEnabled = snapshot.get('two_factor_enabled');
                     if (twoFactorEnabled) {
                         //    Set saved 2-factor state
                         $enableTwoFactor.prop('checked', true);
                         $twoFactorEnabledDisp.text('Enabled');
                         $twoFactorEnabledDisp.attr('class', 'alert alert-xs alert-success');
                         Clue = snapshot.get('two_factor_clue');
                     }
                     else {
                         $enableTwoFactor.prop('checked', false);
                         $twoFactorEnabledDisp.text('Disabled');
                         $twoFactorEnabledDisp.attr('class', 'alert alert-xs alert-dark');
                     }
                 }
             }).catch((err) => {
             });

             $enableTwoFactor.change((event) => {
                 if ($enableTwoFactor.is(':checked')) {
                     $twoFactorModal.modal('show');
                     $twoFactorEnabledDisp.text('Enabled');
                     $twoFactorEnabledDisp.attr('class', 'alert alert-xs alert-success');
                     var ajax = $.ajax({
                         url: '/backend/php/2factor-methods.php',
                         type: 'POST',
                         data: {
                             method: 'getQrCode',
                             domain: 'GainfySTO',
                             issuer: 'Gainfy',
                             username: firebaseUser.email
                         },
                         dataType: 'JSON',
                         cache: false,
                         async: true,
                     });
                     ajax.always(() => {
                     });
                     ajax.done((response) => {
                         if (response.success) {
                             const QrCode = response.data['barcode'];
                             Clue = response.data['clue'];
                             $qrCode.attr('src', QrCode);
                             $encClue.html(Clue);
                         }
                     });
                     ajax.fail((response) => {
                     });
                 }
                 else {
                     event.preventDefault();
                     event.stopPropagation();
                     if (confirm("Are you sure you want to turn off 2-factor authentication? If you choose to re-enable this feature at a later time, you will be required to link this account to your authenticator app again.")) {
                         $twoFactorModal.modal('hide');
                         $enableTwoFactor.prop('checked', false);
                         $twoFactorEnabledDisp.text('Disabled');
                         $twoFactorEnabledDisp.attr('class', 'alert alert-xs alert-dark');
                         db.collection('accounts').doc(firebaseUser.uid)
                           .update({
                               two_factor_enabled: false,
                               two_factor_setup: firebase.firestore.FieldValue.delete(),
                               two_factor_validated: firebase.firestore.FieldValue.delete(),
                               two_factor_clue: firebase.firestore.FieldValue.delete(),
                               two_factor_scratch_codes: firebase.firestore.FieldValue.delete(),
                           })
                           .then(() => {
                           })
                           .catch((err) => {
                           });
                     }
                     else {
                         $enableTwoFactor.prop('checked', true);
                         $twoFactorEnabledDisp.text('Enabled');
                         $twoFactorEnabledDisp.attr('class', 'alert alert-xs alert-success');
                     }
                 }
             });

             $twoFactorSetupComplete.click(() => {
                 $scratchCodeModal.modal('show');
                 const accountRef = db.collection('accounts').doc(firebaseUser.uid);
                 if (!Clue) {
                 }
                 accountRef.set({
                     two_factor_enabled: true,
                     two_factor_setup: true,
                     two_factor_validated: true,
                     two_factor_clue: Clue,
                 }, {merge: true}).then(() => {
                     $twoFactorModal.modal('hide');
                     $enableTwoFactor.prop('checked', true);
                     $twoFactorEnabledDisp.text('Enabled');
                     $twoFactorEnabledDisp.attr('class', 'alert alert-xs alert-success');
                 }).catch((err) => {
                     accountRef.set({
                         two_factor_enabled: true,
                         two_factor_setup: true,
                         two_factor_validated: true,
                         two_factor_clue: Clue,
                         got_scratch_codes: false,
                     }, {merge: true}).then(() => {
                     }).catch((err) => {
                     });
                 });

                 const noteBox = $('#scratch-code-notebox'), note = $('#scratch-code-note');

                 $.ajax({
                     url: '/backend/php/2factor-methods.php',
                     data: {method: 'getScratchCodes'},
                     dataType: 'JSON',
                     type: 'POST',
                     cache: false,
                     async: true
                 })
                  .always(() => {
                      noteBox.hide();
                  })
                  .fail((response) => console.log(JSON.stringify(response)))
                  .done((response) => {
                      //Save scratch codes to Firebase
                      let codes = response.scratchCodes;

                      accountRef.set({
                          two_factor_scratch_codes: codes,
                          got_scratch_codes: true,
                      }, { merge: true })
                      .then(() => {
                          codes.map((code, index) => {
                              $codes[index].text(code);
                          });
                      })
                      .catch((err) => {
                          note.html('There was a problem saving your 2-factor recovery scratch codes. ' +
                              'Without these scratch codes, you will be unable to access your account without your Authenticator App. ' +
                              'To generate new scratch codes, disable and re-enable 2-factor authentication. If you continue to receive this ' +
                              'message, please contact the support team at <strong>support@gainfy.org</strong>');
                          noteBox.show('inline-block');
                          $code1.text('There was a problem saving these scratch codes');
                          $code4.text('Please try re-enabling 2-factor auth to attempt saving again');
                      });
                  });
             });

             $scratchCodeComplete.click(() => {
                 $scratchCodeModal.modal('hide');
             });

             // Gives validation feedback when user enters their 2Factor key after setup
             $validateTwoFactorKey.keyup((event) => {
                 // Hide all key feedback if input box is empty
                 if ($validateTwoFactorKey.val() === '') {
                     $keyValid.hide();
                     $keyInvalid.hide();
                     $twoFactorSetupComplete.prop('disabled', true);
                     return;
                 }

                 // Send key validation request
                 const ajax = $.ajax({
                     url: '/backend/php/2factor-methods.php',
                     type: 'POST',
                     data: {method: 'validateKey', key: event.target.value, clue: Clue},
                     dataType: 'JSON',
                     cache: false,
                     async: true,
                 });
                 ajax.always(() => {
                     console.log("Ajax function for Validating Key completed");
                 });
                 ajax.done((response) => {
                     // Check internal request success status
                     if (response.success) {
                         // If key is valid, show valid feedback, enable submit button
                         let valid = response.data['valid'];
                         if (valid) {
                             $keyValid.show();
                             $keyInvalid.hide();
                             $twoFactorSetupComplete.prop('disabled', false);
                         }
                         // If key not valid, keep submit disabled, show invalid feedback
                         else {
                             $keyValid.hide();
                             $keyInvalid.show();
                             $twoFactorSetupComplete.prop('disabled', true);
                         }
                     }
                     else {
                         console.warn(response.errorMsg);
                     }
                 });
                 ajax.fail((response) => {
                     console.warn(`Ajax func Validate Key failed :: ${JSON.stringify(response)}`);
                 });
             });

             $twoFactorModal.blur(() => {
                 // If setup was not completed, turn switch back off
                 $enableTwoFactor.prop('checked', false);
                 $twoFactorEnabledDisp.text('Disabled');
                 $twoFactorEnabledDisp.attr('class', 'alert alert-xs alert-dark');
             });


         }
     });

    const mobileOs = getMobileOperatingSystem(), gAuthLink = $('#g-auth-app'), authyLink = $('#authy-app'), mAuthLink = $('#micro-auth-app');

    gAuthLink.attr('href', mobileOs==="Android"?"https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2":mobileOs==="iOS"?"https://itunes.apple.com/us/app/google-authenticator/id388497605":mobileOs==="Windows Phone"?"https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2":"https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2");

    authyLink.attr('href', mobileOs==="Android"?"https://play.google.com/store/apps/details?id=com.authy.authy":mobileOs==="iOS"?"https://itunes.apple.com/us/app/authy/id494168017":mobileOs==="Windows Phone"?"https://authy.com/features/setup/":"https://play.google.com/store/apps/details?id=com.authy.authy");

    mAuthLink.attr('href', mobileOs==="Android"?"https://play.google.com/store/apps/details?id=com.azure.authenticator":mobileOs==="iOS"?"https://itunes.apple.com/us/app/microsoft-authenticator/id983156458":mobileOs==="Windows Phone"?"https://www.microsoft.com/en-us/p/microsoft-authenticator/9nblgggzmcj6":"https://play.google.com/store/apps/details?id=com.azure.authenticator");

    /**
     * Determine the mobile operating system.
     * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
     *
     * @returns {String}
     */
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }

    const form = $('form');
    // Disable up and down keys on a number input
    form.on('keydown', 'input[type=number]', function (e) {
        if (e.which === 38 || e.which === 40)
            e.preventDefault();
    });

    // Enable Bootstrap Popover
    $("[data-toggle=popover]").popover();

    // Dismiss Popover on next click
    $('.popover-dismiss').popover({
        trigger: 'focus'
    });

})(jQuery);