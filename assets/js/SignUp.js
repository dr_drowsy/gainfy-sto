(function ($) {

    'use strict';

    if (!firebase.apps.length) {
        return;
    }

    const db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });

    const referredById = getParametersByName()['ref'];

    const signUpEmail = $('#signUpEmail');
    const signUpPassword = $('#signUpPassword');
    const signUpPasswordRepeated = $('#signUpPasswordRepeated');
    const signUpButton = $('#signUpButton');
    const noteBox = $('#noteBox');
    const note = $('#note');
    const noteBoxSuccess = $('#noteBoxSuccess');
    const noteSuccess = $('#noteSuccess');
    const auth = firebase.auth();
    let validPass = false;
    signUpButton.enable(false);

    $(document).ready(() => {
        if (referredById) {
            const ajaxCheckRef = $.ajax({
                url: '/backend/php/SignUp.php',
                type: 'POST',
                data: {method: 'isReferrerValid', referredBy: escapeHtml(referredById)},
                dataType: 'JSON',
                cache: false,
                async: true,
            });
            ajaxCheckRef.fail((response) => {
            });
            ajaxCheckRef.done((response) => {
                if (!response.success) {
                    if (window.confirm('The referral ID could not be found. Click OK to refresh the page and try again, or Cancel to continue registration without using a referral code. Keep in mind that using a referral code to sign up can have benefits for both parties in the form of bonus GAIN tokens upon contributing. If refreshing this page does not fix this issue, you could try entering the code manually, or requesting a new one to ensure it wasn\'t malformed.')) {
                        window.location.reload();
                    }
                    else {
                        window.location.replace('signup.html');
                    }
                }
            });
        }

        $(window).keydown((e) => {
            if (e.which === 13) {
                e.preventDefault();
                e.stopPropagation();
                if (signUpEmail.val() && signUpPassword.val() && signUpPasswordRepeated.val() && signUpPassword.val() === signUpPasswordRepeated.val()) {
                    signUpButton.click();
                    return false;
                }
            }
        });
    });

    signUpEmail.keyup(() => {
        noteBox.hide('inline-block');
        note.text('');
        noteBoxSuccess.hide('inline-block');
        noteSuccess.text('');
        signUpPasswordRepeated.text('');
    });


    signUpPassword.keyup(() => {
        if (signUpPassword.val() === '') {
            validPass = false;
            noteBox.hide('inline-block');
            note.text('');
            noteBoxSuccess.hide('inline-block');
            noteSuccess.text('');
        }
        else if(signUpPassword.val().length < 8) {
            validPass = false;
            noteBox.show('inline-block');
            noteBoxSuccess.hide('inline-block');
            note.text('Password must be greater than 8 characters!');
            noteSuccess.text('');
        }
        else if (!(/^[a-zA-Z0-9]*$/.test(signUpPassword.val()))) {
            validPass = false;
            noteBox.show('inline-block');
            noteBoxSuccess.hide('inline-block');
            note.text("Password contains illegal characters. Please use only alphanumeric characters.");
            noteSuccess.text('');
        }
        else {
            validPass = true;
            noteBox.hide('inline-block');
            noteBoxSuccess.hide('inline-block');
            note.text('');
            noteSuccess.text('');
            signUpButton.enable(true);
        }
    });

    signUpPasswordRepeated.keyup(() => {
        if ((signUpPassword.val() === signUpPasswordRepeated.val()) && validPass) {
            noteBoxSuccess.show('inline-block');
            noteSuccess.text('Passwords Match and are valid!');
            noteBox.hide('inline-block');
            note.text('');
        }
        else {
            noteBoxSuccess.hide('inline-block');
            noteSuccess.text('');
            noteBox.show('inline-block');
            note.text('Passwords do not match!');
        }
    });

    signUpButton.click(() => {
        const email = signUpEmail.val();
        noteBoxSuccess.hide('inline-block');
        noteSuccess.text('');
        noteBox.hide('inline-block');
        note.text('');

        if ((signUpPassword.val() === signUpPasswordRepeated.val()) && validPass === true) {
            if (!(/^[a-zA-Z0-9]*$/.test(signUpPasswordRepeated.val()))) {
                noteBox.show('inline-block');
                noteBoxSuccess.hide('inline-block');
                note.text("Password contains illegal characters. Please use only alphanumeric characters.");
                noteSuccess.text('');
            }
            else {
                noteBox.hide('inline-block');
                noteBoxSuccess.hide('inline-block');
                note.text("");
                const pass = signUpPassword.val();

                auth.setPersistence(firebase.auth.Auth.Persistence.SESSION).then(() => {
                    auth.createUserWithEmailAndPassword(email, pass).then(() => {
                        const firebaseUser = auth.currentUser;
                        const accountRef = db.collection('accounts').doc(firebaseUser.uid);
                        accountRef.set({
                            two_factor_setup: false,
                            two_factor_enabled: false,
                            two_factor_validated: false,
                        }, { merge: true }).then(() => {
                            firebaseUser.sendEmailVerification().then(() => {
                                const ajax = $.ajax({
                                    url: '/backend/php/SignUp.php',
                                    type: 'POST',
                                    data: {
                                        method: 'registerNewUser',
                                        uid: firebaseUser.uid,
                                        email: firebaseUser.email,
                                        firstName: '',
                                        lastName: '',
                                        referredBy: referredById ? referredById : ''},
                                    dataType: 'JSON',
                                    cache: false,
                                    async: true,
                                });
                                ajax.fail((response) => {
                                });
                                ajax.done((response) => {
                                    signOut('signup-success.html', "User registration complete. Begin first time login.");
                                });
                            }).catch((err) => {
                                signOut('signup-success.html', "User registration complete. Begin first time login.");
                            });
                        }).catch((err) => {
                            signOut('signup-success.html', "User registration complete. Begin first time login.");
                        });

                    }).catch((err) => {
                        noteBoxSuccess.hide('inline-block');
                        note.text(err.message || err);
                        noteBox.show('inline-block');
                    });
                });
            }
        }
        else {
            alert("You have attempted to submit an invalid password!");
        }
    });

}(jQuery));
