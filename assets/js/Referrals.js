(($) => {
    'use strict';

    const bonusMultiplier = 25;

    if (!firebase.apps.length)
        console.error("Firebase not initialized!");
    if (!db)
        console.error("DB not initialized");


    const referralLink = $('#referralLink'), signupsCount = $('#signupsCount'),
        contributionsCount = $('#contributionsCount'), bonusCount = $('#bonusCount'), referredBy = $('#referredByText'),
        referredByHeader = $('#referredByHeader');

    firebaseUser.then((user) => {
        if (user) {
            referralLink.val(`https://sto.gainfy.com/signup.html?ref=${user.uid}`);

            const ajax = $.ajax({
                url: '/backend/php/Referrals.php',
                type: 'POST',
                data: {method: 'getUserReferrals', uid: user.uid},
                dataType: 'JSON',
                cache: false,
                async: true,
            });
            ajax.always(() => {
                console.log('POST fetch user referral info sent');
            });
            ajax.fail((response) => {
                console.warn(JSON.stringify(response));
            });
            ajax.done((response) => {
                if (response.success) {
                    if (response.referredBySuccess) {
                        let firstName = response.data['referredBy']['FirstName'],
                            lastName = response.data['referredBy']['LastName'],
                            email = response.data['referredBy']['Email'],
                            referredById = response.data['referredBy']['UniqueID'];
                        console.log(`ReferredByID: ${referredById}`);
                        referredBy.text(firstName && lastName ? `${firstName} ${lastName}` : email ? email : referredById);
                        referredByHeader.show('inline-block');
                    }
                    if (response.referralSuccess) {
                        let referrals = response.data['referrals'], signups = response.data['signups'],
                            contributions = response.data['contributions'], bonus = response.data['bonus'];
                        signupsCount.text(signups);
                        contributionsCount.text(contributions);
                        bonusCount.text(bonus);

                        const referralTable = $('#table-body');
                        let count = 0;

                        $.each(referrals, (key, value) => {
                            let firstName = value['FirstName'], lastName = value['LastName'],
                                email = value['EmailAddress'], uid = value['UniqueID'],
                                contributionCount = value['ContributionCount'],
                                contributionValue = value['ContributionValue'], timestamp = value['Timestamp'];

                            let evenOrOdd = count % 2 === 0 ? 'even' : 'odd';
                            count++;
                            let row = `
<tr class="${evenOrOdd}">
<td class="data-table referral-username">${firstName && lastName ? `${firstName} ${lastName}` : email ? email : uid}</td>
<td class="data-table referral-contribute">${contributionCount}</td>
<td class="data-table referral-bonus">${contributionCount * bonusMultiplier}</td>
<td class="data-table referral-date">${timestamp}</td>
</tr>
`;
                            referralTable.append($.parseHTML(row));

                        });

                        // Activity Data Table
                        var $refferal_table = $('.refferal-table');
                        if ($refferal_table.length > 0) {
                            $refferal_table.DataTable({
                                ordering: false,
                                autoWidth: false,
                                dom: '<"row"<"col-12"<"overflow-x-auto"t>>><"row align-items-center"<"col-sm-6 text-left"p><"col-sm-6 text-sm-right text-center"i>>',
                                pageLength: 5,
                                bPaginate: $('.data-table tbody tr').length > 5,
                                iDisplayLength: 5,
                                language: {
                                    info: "_START_ -_END_ of _TOTAL_",
                                    infoEmpty: "No records",
                                    infoFiltered: "( Total _MAX_  )",
                                    paginate: {
                                        first: "First",
                                        last: "Last",
                                        next: "Next",
                                        previous: "Prev"
                                    },
                                },
                            });
                        }
                        //endregion


                    }
                    else {
                        if (response.errorMsg === "No Referrals") {
                            console.log('User has no referrals to list');
                        }
                        else {
                            console.warn(`Unknown Error fetching referrals --> ${JSON.stringify(response.errorMsg)}`);
                        }
                    }
                }
                else {
                    console.log(`Unable to display referrals --> ${response.errorMsg}`);
                }
            });
        }
    });

})(jQuery);