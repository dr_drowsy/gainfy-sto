(($) => {
    $(document).ready(() => {

        const mode = getParametersByName()['mode'];
        const actionCode = getParametersByName()['oobCode'];
        const apiKey = getParametersByName()['apiKey'];
        const continueUrl = getParametersByName()['continueUrl'];

        if (!firebase.apps.length) {
            const config = {'apiKey': apiKey};
            firebase.initializeApp(config);
        }
        const auth = firebase.auth();

        switch (mode) {
            case 'resetPassword':
                console.log("Resetting Password");
                setTimeout(() => {
                    window.location.replace(`reset-password.html?oobCode=${actionCode}&apiKey=${apiKey}&continueUrl=${continueUrl}`);
                }, 1000);
                break;
            case 'recoverEmail':
                console.log("Recover Email");
                setTimeout(() => {
                    window.location.replace(`recover-email.html?oobCode=${actionCode}&apiKey=${apiKey}&continueUrl=${continueUrl}`);
                    // location.replace('login.html');
                }, 1000);
                break;
            case 'verifyEmail':
                console.log("Verifying Email");
                setTimeout(() => {
                    window.location.replace(`verify-email.html?oobCode=${actionCode}&apiKey=${apiKey}&continueUrl=${continueUrl}`);
                }, 500);
                break;
            default:
                break;
        }
    });

})(jQuery);
