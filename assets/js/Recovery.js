(($) => {

    'use strict';

    if (!firebase.apps.length) {
        console.error('FIREBASE NOT INITIALIZED!');
    }

    // // Initialize Firestore
    const db = firebase.firestore();
    // // Turn off deprecated settings
    db.settings({
        timestampsInSnapshots: true
    });

    $(document).ready(() => {
        const noteBox = $('#noteBox'), noteBoxSuccess = $('#noteBoxSuccess'), note = $('#note'), noteSuccess = $('#noteSuccess');
        const recoveryEmailInput = $('#recoveryEmailInput'), recoveryPasswordButton = $('#recoveryPasswordButton');

        recoveryPasswordButton.click(() => {
            const userEmail = recoveryEmailInput.val();
            firebase.auth().sendPasswordResetEmail(userEmail).then(() => {
                console.log('Password reset email sent');
                noteBox.hide('inline-block');
                noteBoxSuccess.show('inline-block');
            }).catch((err) => {
                console.warn(`Error ${err.code} sending password reset email: ${err.message}`);
                note.html(`Error ${err.code}: ${err.message}`);
                noteBoxSuccess.hide('inline-block');
                noteBox.show('inline-block');
            });
        });

        recoveryEmailInput.keyup(() => {
            noteBox.hide('inline-block');
            noteBoxSuccess.hide('inline-block');
        });
    });

})(jQuery);