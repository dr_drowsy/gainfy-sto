(($) => {

    firebaseUser.then((user) => {
        const uid = user.uid, email = user.email;

        $.ajax({
            url : "/backend/php/activityLogger.php",
            method : "post",
            dataType: "JSON",
            data : {
                method: "logActivity",
                uid: uid,
                email: email,
            }
        })
            .fail(response => console.warn(JSON.stringify(response)))
            .done(response => {
                console.log(JSON.stringify(response));
            });

        $.ajax({
            url : "/backend/php/UserContribution.php",
            method : "post",
            dataType : 'JSON',
            data : {method : 'getContributions', uid : user.uid},
        })
            .fail(response => console.log(JSON.stringify(response)))
            .done(response => {
                const EthContr = response.EthContributions, BtcContr = response.BtcContributions;
                $('#stoYourContribution').html(`
                 <div class='tile-item tile-light'>
                     <div class='tile-bubbles'></div>
                     <h6 class='tile-title'>YOUR CONTRIBUTION</h6>
                     <ul class='tile-info-list'>
                         <li><span> ${EthContr > 0 ? EthContr % 1 === 0 ? EthContr.toLocaleString() : EthContr.toFixed(4) : 0}</span> ETH</li>
                         <li><span>	${BtcContr > 0 ? BtcContr % 1 === 0 ? BtcContr.toLocaleString() : BtcContr.toLocaleString() : 0}</span>	BTC</li>
                         <li><span>	0</span> USD</li>
                     </ul>
                 </div>
                 `);
            });

        $.ajax({
            url : "/backend/php/tokenSaleSupply.php",
            method : "post",
            data : {method : 'getTokenSaleSupply'},
            dataType : 'JSON',
        })
            .fail(response => console.log(JSON.stringify(response)))
            .done(response => {
                const tokenSupply = response.TokenSupply, btcContr = response.BtcContributed, ethContr = response.EthContributed, usContr = response.UsContributed;

                $('#stoTokenSupply').html(`
                 <div class='tile-item tile-primary'>
                     <div class='tile-bubbles'></div>
                     <h6 class='tile-title' >STO TOKEN SUPPLY</h6>
                     <h1 class='tile-info'>${tokenSupply > 0 ? tokenSupply % 1 === 0 ? tokenSupply.toLocaleString() : parseFloat(tokenSupply.toFixed(2)).toLocaleString() : 0} GAIN</h1>
                     <ul class='tile-list-inline'>
                         <li>${btcContr > 0 ? btcContr % 1 === 0 ? btcContr.toLocaleString() : parseFloat(btcContr.toFixed(2)).toLocaleString() : 0} BTC</li>
                         <li>${ethContr > 0 ? ethContr % 1 === 0 ? ethContr.toLocaleString() : parseFloat(ethContr.toFixed(2)).toLocaleString() : 0} ETH</li>
                         <li>${usContr} USD</li>
                     </ul>
                 </div>
                 `);

                const progress = response.Progress, reserved = response.Reserved;

                $('#progressBar').html(`
                 <div class='progress-bar'>
                     <div class='progress-hcap' style='width:100%;'>
                        <div style='width: 110px; text-align: start'>Hard Cap <span></span></div>
                     </div>
                     <div class='progress-scap' style='width:50%'>
                        <div>Soft cap <span>25,000,000</span></div>
                     </div>
                     <div class='progress-psale' style='width: 0%'>
                        <div style='width: 85px; text-align: end;'>Pre Sale <span></span></div>
                     </div>
                     <div class='progress-percent' style='width: ${progress.toFixed(2)}%'></div>
                 </div>
                 <ul class='progress-info'>
                     <li><span>Reserved - </span>${reserved > 0 ? reserved % 1 === 0 ? reserved.toLocaleString() : parseFloat(reserved.toFixed(2)).toLocaleString() : 0} GAIN</li>
                     <li><span>Available - </span>${tokenSupply > 0 ? tokenSupply % 1 === 0 ? tokenSupply.toLocaleString() : parseFloat(tokenSupply.toFixed(2)).toLocaleString() : 0} GAIN</li>
                 </ul>
                 `);
            });
    });

})(jQuery);
