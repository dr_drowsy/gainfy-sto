(($) => {

    'use strict';

    if (!firebase.apps.length) {
        return;
    }

    const db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });

    const auth = firebase.auth();
    const submitButton = $('#submit-2factor-key');
    const keyInput = $('#validate-2factor-key');
    const note = $('#note');
    const noteBox = $('#noteBox');
    const scratchCodeModal = $('#scratchCodeModal'), scratchCodeModalOpen = $('#toggle-scratch-code-modal'), submitScratchCode = $('#scratch-code-complete'), scratchCodeInput = $('#validate-2factor-scratch');
    const scratchCodeNoteBox = $('#scratch-code-notebox'), scrtachCodeNote = $('#scratch-code-note'), scratchCodeNoteBoxSuccess = $('#scratch-code-notebox-success'), scratchCodeNoteSuccess = $('#scratch-code-note-success');

    $(document).ready(() => {
        $(window).keydown((e) => {
            if (e.which === 13) {
                e.preventDefault();
                e.stopPropagation();
                if (!submitButton.prop('disabled')) {
                    submitButton.click();
                    return false;
                }
            }
        });
    });



    firebaseUser.then((user) => {
        if (user) {
            const accountRef = db.collection('accounts').doc(user.uid);
            accountRef.get().then(snapshot => {
                if (snapshot.exists) {
                    const twoFactorEnabled = snapshot.get('two_factor_enabled');
                    if (twoFactorEnabled) {
                        scratchCodeModalOpen.click((event) => {
                            event.preventDefault();
                            event.stopPropagation();
                            submitScratchCode.click(() => {
                                const inputCode = escapeHtml(scratchCodeInput.val().toUpperCase());
                                if (snapshot.get('two_factor_scratch_codes').includes(inputCode)) {
                                    scratchCodeNoteBox.hide();
                                    scratchCodeNoteSuccess.text('Scratch Code Validated');
                                    scratchCodeNoteBoxSuccess.show('inline-block');
                                    accountRef.update({
                                        two_factor_scratch_codes: firebase.firestore.FieldValue.arrayRemove(inputCode),
                                        two_factor_validated: true,
                                    })
                                    .then(() => {
                                        window.location.replace('index.html');
                                    })
                                    .catch((err) => {
                                        signOut('login.html', 'Unable to update user scratch codes');
                                    });
                                }
                                else {
                                    scratchCodeNoteBoxSuccess.hide();
                                    scrtachCodeNote.text('Invalid Scratch Code!');
                                    scratchCodeNoteBox.show('inline-block');
                                }
                            });
                            scratchCodeModal.modal('show');
                        });

                        const Clue = snapshot.get('two_factor_clue');
                        if (Clue) {
                            submitButton.click((event) => {
                                event.preventDefault();
                                event.stopPropagation();
                                const ajax = $.ajax({
                                    url: '/backend/php/2factor-methods.php',
                                    type: 'POST',
                                    data: {method: 'validateKey', key: escapeHtml(keyInput.val()), clue: Clue},
                                    dataType: 'JSON',
                                    cache: false,
                                    async: true,
                                });
                                ajax.done((response) => {
                                    if (response.success) {
                                        const valid = response.data['valid'];
                                        if (valid) {
                                            accountRef.set({
                                                two_factor_validated: true,
                                            }, { merge: true }).then(() => {
                                                window.location.replace('index.html');
                                            }).catch(() => {
                                                signOut('login.html', 'Error setting user validation status to Firestore! Logging out...');
                                            });
                                        }
                                        else {
                                            note.text('Invalid authentication key');
                                            noteBox.show('inline-block');
                                        }
                                    }
                                    else {
                                        note.text(`Something went wrong with 2-factor validation :: ${response.errorMsg}`);
                                        noteBox.show('inline-block');
                                    }
                                });
                                ajax.fail((response) => {
                                })
                            });
                        }
                    }
                    else {
                        window.location.replace('index.html');
                    }
                }
                else {
                    signOut('login.html', 'There was an error fetching your account data. Try again.');
                }
            })
        }
        else {
            window.location.replace('login.html');
        }
    });

    keyInput.keyup((event) => {
        const value = event.target.value;
        note.text('');
        noteBox.hide('inline-block');
        if (value.length === 6) {
            submitButton.prop('disabled', false);
        }
        else {
            submitButton.prop('disabled', true);
        }
    });

    scratchCodeInput.keyup((event) => {
        const value = event.target.value;
        scratchCodeNoteBox.hide();
        if (value.length === 6) {
            submitScratchCode.prop('disabled', false);
        }
        else {
            submitScratchCode.prop('disabled', true);
        }
    });

    // Enable Bootstrap Popover
    $("[data-toggle=popover]").popover();

    // Dismiss Popover on next click
    $('.popover-dismiss').popover({
        trigger: 'focus'
    });

    const form = $('form');
    // Disable up and down keys on a number input
    form.on('keydown', 'input[type=number]', function(e) {
        if ( e.which === 38 || e.which === 40 )
            e.preventDefault();
    });

})(jQuery);
