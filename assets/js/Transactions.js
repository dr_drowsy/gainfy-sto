(($) => {

    const ethTranxTable = $('#eth-tranx'),
        btcTranxTable = $('#btc-tranx'),
        ethTranxBody = $('#eth-table-body'),
        btcTranxBody = $('#btc-table-body'),
        ethLoader = $('#eth-loader'),
        btcLoader = $('#btc-loader'),
        ethTableCont= $('#eth-table-cont'),
        btcTableCont = $('#btc-table-cont'),
        detailsModalBody = $('#details-modal-body'),
        detailsModal = $('#details-modal');

    let ethReturned = false, btcReturned = false;


    firebaseUser.then((firebaseUser) => {
        //Get Eth Tranx
        $.ajax({
            url : '/backend/php/Transactions.php',
            type : 'POST',
            data : {
                method : 'getEthTransactions',
                uid : firebaseUser.uid,
                crypto : 'eth',
            },
            dataType : 'JSON',
            cache : false,
            async : true,
            success: applyTableStyle(),
        })
         .fail((response) => {
         })
         .done((response) => {
             //    Insert data
             const completedTranx = response.transactions.Complete,
                 pendingTranx = response.transactions.Pending,
                 canceledTranx = response.transactions.Canceled;
             let count = 0;

             if (completedTranx) {
                 completedTranx.map((tranx) => {
                     let trxNo = `GAINTXETH${tranx['TransactionID']}`, tokens = tranx['ActualReservedTokens'], contribution = tranx['ContributionAmountReceived'], status = tranx['TransactionStatus'], timestamp = tranx['DateAndTime'], bonus = tranx['ActualBonusTokens'], sender = tranx['SendingAddress'], receiver = tranx['ReceivingAddress'], rate = tranx['EthereumRate'];
                     let evenOrOdd = count % 2 === 0 ? 'even' : 'odd';
                     count++;
                     let conversion = `1 USD = 0.5 GAIN = ~${(1 / tranx['EthereumRate']).toFixed(8)} ETH`;
                     let row = `
            <tr title='${conversion}'>
            <td class="tranx-status tranx-status-approved" ><span class="d-none" > Approved</span ><em class="ti ti-check" ></em ></td >
            <td class="tranx-no" ><span >${trxNo}</span >${timestamp}</td >
            <td class="tranx-token" ><span > +${parseFloat(tokens).toFixed(2)}</span > GAIN</td >
            <td class="tranx-amount" ><span > ${parseFloat(contribution).toFixed(6)}</span > ETH </td >
            <td class="tranx-from" ><span > Contribution Received </span > _</td >
            <td class="tranx-action" id='${trxNo}' >
                <a href="#" data-toggle="modal" data-target="#myModal" ><em class="ti ti-more-alt trxDetails" ></em > </a >
            </td>
            </tr >
                    `;

                     ethTranxBody.append($.parseHTML(row));

                     $(`#${trxNo}`).click(() => {
                         injectModalData(trxNo, 'eth', tokens, bonus, contribution, status, timestamp, sender, receiver, rate);
                     });
                 });
             }

             if (pendingTranx) {
                 pendingTranx.map((tranx) => {
                     let trxNo = `GAINTXETH${tranx['TransactionID']}`, tokens = tranx['ExpectedReservedTokens'], contribution = tranx['ExpectedContributionAmount'], status = tranx['TransactionStatus'], timestamp = tranx['DateAndTime'], bonus = tranx['ExpectedReservedBonus'], sender = 'Pending', receiver = tranx['ReceivingAddress'], rate = tranx['EthereumRate'], trxId = tranx['TransactionID'];
                     let evenOrOdd = count % 2 === 0 ? 'even' : 'odd';
                     count++;
                     let conversion = `1 USD = 0.5 GAIN = ~${(1 / tranx['EthereumRate']).toFixed(8)} ETH`;
                     let row = `
            <tr title='${conversion}'>
            <td class="tranx-status tranx-status-pending"><span class="d-none"> Pending</span ><em class="ti ti-alert"></em></td>
            <td class="tranx-no" ><span>${trxNo}</span>${timestamp}</td>
            <td class="tranx-token" ><span> +${parseFloat(tokens).toFixed(2)}</span> GAIN</td>
            <td class="tranx-amount" ><span> ${parseFloat(contribution).toFixed(6)}</span> ETH </td>
            <td class="tranx-from" ><span> Contribution Pending </span> _</td>
            <td class="tranx-action" id='${trxNo}'>
                <a href="#" data-toggle="modal" data-target="#myModal"><em class="ti ti-more-alt trxDetails" ></em></a>
            </td>
            </tr>
                    `;

                     ethTranxBody.append($.parseHTML(row));
                     const thisTranx = $(`#${trxNo}`);

                     thisTranx.click(() => {
                         const cancelContribution = (event) => {
                             if (window.confirm(`Are you sure you want to cancel ${trxNo}? If you've already initiated this transaction, do not cancel this scheduled contribution.`)) {
                                 $.ajax({
                                     url: '/backend/php/Transactions.php',
                                     type: 'POST',
                                     dataType: 'JSON',
                                     cache: false,
                                     async: true,
                                     data: { method: 'cancelEthContribution', trxId: trxId }
                                 })
                                     .fail(() => {})
                                     .done(response => {
                                         if (response.success) {
                                             $('#details-modal').modal('hide');
                                             thisTranx.parent().html(`
                                        <td class="tranx-status tranx-status-cancled"><span class="d-none"> Canceled</span ><em class="ti ti-close"></em></td>
                                        <td class="tranx-no" ><span>${trxNo}</span>${timestamp}</td>
                                        <td class="tranx-token" ><span> +${parseFloat(tokens).toFixed(2)}</span> GAIN</td>
                                        <td class="tranx-amount" ><span> ${parseFloat(contribution).toFixed(6)}</span> ETH </td>
                                        <td class="tranx-from" ><span> Contribution Canceled </span> _</td>
                                        <td class="tranx-action" id='${trxNo}'>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><em class="ti ti-more-alt trxDetails" ></em></a>
                                        </td>
                                         `);
                                             $(`#${trxNo}`).click(() => {
                                                 injectModalData(trxNo, 'eth', tokens, bonus, contribution, 'Canceled', timestamp, sender, receiver, rate);
                                             });
                                         }
                                     });
                             }
                             else {

                             }
                         };
                         injectModalData(trxNo, 'eth', tokens, bonus, contribution, status, timestamp, sender, receiver, rate, cancelContribution);
                     });
                 });
             }

             if (canceledTranx) {
                 canceledTranx.map((tranx) => {
                     let trxNo = `GAINTXETH${tranx['TransactionID']}`, tokens = tranx['ExpectedReservedTokens'], contribution = tranx['ExpectedContributionAmount'], status = tranx['TransactionStatus'], timestamp = tranx['DateAndTime'], bonus = tranx['ExpectedReservedBonus'], sender = 'Canceled', receiver = 'Canceled', rate = tranx['EthereumRate'];
                     let evenOrOdd = count % 2 === 0 ? 'even' : 'odd';
                     count++;
                     let conversion = `1 USD = 0.5 GAIN = ~${(1 / tranx['EthereumRate']).toFixed(8)} ETH`;
                     let row = `
            <tr title='${conversion}'>
            <td class="tranx-status tranx-status-cancled"><span class="d-none"> Canceled</span ><em class="ti ti-close"></em></td>
            <td class="tranx-no" ><span>${trxNo}</span>${timestamp}</td>
            <td class="tranx-token" ><span> +${parseFloat(tokens).toFixed(2)}</span> GAIN</td>
            <td class="tranx-amount" ><span> ${parseFloat(contribution).toFixed(6)}</span> ETH </td>
            <td class="tranx-from" ><span> Contribution Canceled </span> _</td>
            <td class="tranx-action" id='${trxNo}'>
                <a href="#" data-toggle="modal" data-target="#myModal"><em class="ti ti-more-alt trxDetails" ></em></a>
            </td>
            </tr>
                    `;

                     ethTranxBody.append($.parseHTML(row));
                     const thisTranx = $(`#${trxNo}`);

                     thisTranx.click(() => {
                         injectModalData(trxNo, 'eth', tokens, bonus, contribution, status, timestamp, sender, receiver, rate);
                     });
                 });
             }

            ethReturned = true;
            applyTableStyle();
         });

        //Get Bitcoin Tranx
        $.ajax({
            url : '/backend/php/Transactions.php',
            type : 'POST',
            data : {
                method : 'getBtcTransactions',
                uid : firebaseUser.uid,
                crypto : 'btc',
            },
            dataType : 'JSON',
            cache : false,
            async : true,
            success: applyTableStyle(),
        })
         .fail((response) => {

         })
         .done((response) => {
             //    Insert data
             //    Insert data
             const completedTranx = response.transactions.Complete,
                 pendingTranx = response.transactions.Pending,
                 canceledTranx = response.transactions.Canceled;
             let count = 0;

             if (completedTranx) {
                 completedTranx.map((tranx) => {
                     let trxNo = `GAINTXBTC${tranx['TransactionID']}`, tokens = tranx['ActualReservedTokens'], contribution = tranx['ContributionAmountReceived'], status = tranx['TransactionStatus'], timestamp = tranx['DateAndTime'], bonus = tranx['ActualBonusTokens'], sender = tranx['SendingAddress'], receiver = tranx['ReceivingAddress'], rate = tranx['BitcoinRate'];
                     let evenOrOdd = count % 2 === 0 ? 'even' : 'odd';
                     count++;
                     let conversion = `1 USD = 0.5 GAIN = ~${(1 / tranx['BitcoinRate']).toFixed(8)} BTC`;
                     let row = `
            <tr title='${conversion}'>
            <td class="tranx-status tranx-status-approved" ><span class="d-none" > Approved</span ><em class="ti ti-check" ></em ></td >
            <td class="tranx-no" ><span >${trxNo}</span >${timestamp}</td >
            <td class="tranx-token" ><span > +${parseFloat(tokens).toFixed(2)}</span > GAIN</td >
            <td class="tranx-amount" ><span > ${parseFloat(contribution).toFixed(6)}</span > BTC </td >
            <td class="tranx-from" ><span > Contribution Received </span > _</td >
            <td class="tranx-action" id='${trxNo}'>
                <a id='${trxNo}'><em class="ti ti-more-alt trxDetails" ></em > </a >
            </td>
            </tr >
                    `;

                     btcTranxBody.append($.parseHTML(row));

                     $(`#${trxNo}`).click(() => {
                         injectModalData(trxNo, 'btc', tokens, bonus, contribution, status, timestamp, sender, receiver, rate);
                     });
                 });
             }

             if (pendingTranx) {
                 pendingTranx.map((tranx) => {
                     let trxNo = `GAINTXBTC${tranx['TransactionID']}`, tokens = tranx['ExpectedReservedTokens'], contribution = tranx['ExpectedContributionAmount'], status = tranx['TransactionStatus'], timestamp = tranx['DateAndTime'], bonus = tranx['ExpectedReservedBonus'], sender = 'Pending', receiver = tranx['ReceivingAddress'], rate = tranx['BitcoinRate'], trxId = tranx['TransactionID'];
                     let evenOrOdd = count % 2 === 0 ? 'even' : 'odd';
                     count++;
                     let conversion = `1 USD = 0.5 GAIN = ~${(1 / tranx['BitcoinRate']).toFixed(8)} BTC`;
                     let row = `
            <tr title='${conversion}'>
            <td class="tranx-status tranx-status-pending"><span class="d-none"> Pending</span ><em class="ti ti-alert"></em></td>
            <td class="tranx-no" ><span>${trxNo}</span>${timestamp}</td>
            <td class="tranx-token" ><span> +${parseFloat(tokens).toFixed(2)}</span> GAIN</td>
            <td class="tranx-amount" ><span> ${parseFloat(contribution).toFixed(6)}</span> BTC </td>
            <td class="tranx-from" ><span> Contribution Pending </span> _</td>
            <td class="tranx-action" id='${trxNo}'>
                <a href="#" data-toggle="modal" data-target="#myModal"><em class="ti ti-more-alt trxDetails" ></em></a>
            </td>
            </tr>
                    `;

                     btcTranxBody.append($.parseHTML(row));
                     const thisTranx = $(`#${trxNo}`);

                     thisTranx.click(() => {
                         const cancelContribution = (event) => {
                             if (window.confirm(`Are you sure you want to cancel ${trxNo}? If you've already initiated this transaction, do not cancel this scheduled contribution.`)) {
                                 $.ajax({
                                     url: '/backend/php/Transactions.php',
                                     type: 'POST',
                                     dataType: 'JSON',
                                     cache: false,
                                     async: true,
                                     data: { method: 'cancelBtcContribution', trxId: trxId }
                                 })
                                     .done(response => {
                                         if (response.success) {
                                             $('#details-modal').modal('hide');
                                             thisTranx.parent().html(`
                                        <td class="tranx-status tranx-status-cancled"><span class="d-none"> Canceled</span ><em class="ti ti-close"></em></td>
                                        <td class="tranx-no" ><span>${trxNo}</span>${timestamp}</td>
                                        <td class="tranx-token" ><span> +${parseFloat(tokens).toFixed(2)}</span> GAIN</td>
                                        <td class="tranx-amount" ><span> ${parseFloat(contribution).toFixed(6)}</span> BTC </td>
                                        <td class="tranx-from" ><span> Contribution Canceled </span> _</td>
                                        <td class="tranx-action" id='${trxNo}'>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><em class="ti ti-more-alt trxDetails" ></em></a>
                                        </td>
                                         `);
                                             $(`#${trxNo}`).click(() => {
                                                 injectModalData(trxNo, 'btc', tokens, bonus, contribution, 'Canceled', timestamp, sender, receiver, rate);
                                             });
                                         }
                                     });
                             }
                             else {

                             }
                         };

                         injectModalData(trxNo, 'btc', tokens, bonus, contribution, status, timestamp, sender, receiver, rate, cancelContribution);
                     });
                 });
             }

             if (canceledTranx) {
                 canceledTranx.map((tranx) => {
                     let trxNo = `GAINTXBTC${tranx['TransactionID']}`, tokens = tranx['ExpectedReservedTokens'], contribution = tranx['ExpectedContributionAmount'], status = tranx['TransactionStatus'], timestamp = tranx['DateAndTime'], bonus = tranx['ExpectedReservedBonus'], sender = 'Canceled', receiver = 'Canceled', rate = tranx['BitcoinRate'];
                     let evenOrOdd = count % 2 === 0 ? 'even' : 'odd';
                     count++;
                     let conversion = `1 USD = 0.5 GAIN = ~${(1 / tranx['BitcoinRate']).toFixed(8)} BTC`;
                     let row = `
            <tr title='${conversion}'>
            <td class="tranx-status tranx-status-cancled"><span class="d-none"> Canceled</span ><em class="ti ti-close"></em></td>
            <td class="tranx-no" ><span>${trxNo}</span>${timestamp}</td>
            <td class="tranx-token" ><span> +${parseFloat(tokens).toFixed(2)}</span> GAIN</td>
            <td class="tranx-amount" ><span> ${parseFloat(contribution).toFixed(6)}</span> BTC </td>
            <td class="tranx-from" ><span> Contribution Canceled </span> _</td>
            <td class="tranx-action" id='${trxNo}'>
                <a href="#" data-toggle="modal" data-target="#myModal"><em class="ti ti-more-alt trxDetails" ></em></a>
            </td>
            </tr>
                    `;

                     btcTranxBody.append($.parseHTML(row));

                     $(`#${trxNo}`).click(() => {
                         injectModalData(trxNo, 'btc', tokens, bonus, contribution, status, timestamp, sender, receiver, rate);
                     });
                 });
             }

             btcReturned = true;
             applyTableStyle();
         });
    });

    const applyTableStyle = () => {
        if (ethReturned && btcReturned) {
            // Must go after both AJAX calls return their data or the style wont take effect
            // Transation Data Table
            var $tranx_table = $('.tranx-table');

            if ($tranx_table.length > 0) {
                var $tranx_table_fltr = $tranx_table.DataTable({
                    "ordering": false,
                    autoWidth: false,
                    "dom":'<"row"<"col-10 text-left"f><"col-2 text-right"<"data-table-filter dropdown">>><"row"<"col-12"<"overflow-x-auto"t>>><"row"<"col-sm-6 text-left"p><"col-sm-6 text-sm-right"i>>',
                    "pageLength": 7,
                    "bPaginate" : $('.data-table tbody tr').length>7,
                    "iDisplayLength": 7,
                    "language": {
                        "search": "",
                        "searchPlaceholder": "Type in to Search",
                        "info": "_START_ -_END_ of _TOTAL_",
                        "infoEmpty": "No records",
                        "infoFiltered": "( Total _MAX_  )",
                        "paginate": {
                            "first":      "First",
                            "last":       "Last",
                            "next":       "Next",
                            "previous":   "Prev"
                        },
                    },
                });

                $(".data-table-filter").append('<a href="#" data-toggle="dropdown"><em class="ti ti-settings"></em></a><ul class="dropdown-menu dropdown-menu-right"><li><input class="data-filter data-filter-approved" type="radio" name="filter"' +
                    ' id="all" checked value=""><label for="all">All</label></li><li><input class="data-filter data-filter-approved" type="radio" name="filter" id="approved" value="approved"><label' +
                    ' for="approved">Approved</label></li><li><input class="data-filter data-filter-pending" type="radio" name="filter" value="pending" id="pending"><label for="pending">Pending</label></li><li><input class="data-filter data-filter-cancled" type="radio" name="filter" value="canceled" id="canceled"><label for="canceled">Canceled</label></li></ul>');

                var $tranx_filter = $('.data-filter');
                $tranx_filter.on('change', function(){
                    var _thisval = $(this).val();
                    $tranx_table_fltr.columns('.tranx-status').search( _thisval ? _thisval : '', true, false ).draw();
                });
            }
        }

        ethLoader.hide();
        btcLoader.hide();
        ethTableCont.show();
        btcTableCont.show();
    };

    const toggleModalView = () => {
        const contDetails = $('#contribution-details'), makeContribution = $('#make-contribution');

        if (makeContribution.is(':hidden')) {
            contDetails.hide();
            makeContribution.show();
        }
        else {
            makeContribution.hide();
            contDetails.show();
        }
    };

    const injectModalData = (trxNo, crypto, tokens, bonus, contribution, status, timestamp, sender, receiver, rate, cancelContribution = null) => {
        crypto = crypto.toUpperCase();
        const body = `
                        <div id='contribution-details' style='display:inline-block'>
                        <h5>View Transaction Details</h5>
                        <hr />
                        <div class='tranx-purchase-details active'>
                            <p>Transaction <strong>${trxNo}</strong> was place on <strong>${timestamp}</strong> and
                                <br> is currently <strong>${status}</strong>. 
                                ${status === 'Pending' ? ("<a id='cont-link' href='#' class='make-pay'>Please Make\n" +
            "            your payment <em class='ti ti-arrow-right'></em></a></p>") : ''}
                            <h6>Purchase Details</h6>
                            <ul class='tranx-purchase-info'>
                                <li>
                                    <div class='tranx-purchase-head'>Tranx Name</div>
                                    <div class='tranx-purchase-des'>${trxNo}</div>
                                </li><!-- li -->
                                <li>
                                    <div class='tranx-purchase-head'>Payment Method</div>
                                    <div class='tranx-purchase-des'>${crypto.toUpperCase()}</div>
                                </li><!-- li -->
                                <li>
                                    <div class='tranx-purchase-head'>${status === 'Complete' ? 'Contribution' : 'Expected'} Rate</div>
                                    <div class='tranx-purchase-des'>1 ${crypto}<em style='transform: rotate(90deg)' class='ti ti-exchange-vertical'></em>${rate} USD</div>
                                </li><!-- li -->
                                <li>
                                    <div class='tranx-purchase-head'>${status === 'Complete' ? 'Your Contribution' : 'Expected Contribution'}</div>
                                    <div class='tranx-purchase-des'>${contribution}</div>
                                </li><!-- li -->
                                <li>
                                    <div class='tranx-purchase-head'>${status === 'Complete' ? 'Tokens (T)' : 'Expected Tokens (T)'}</div>
                                    <div class='tranx-purchase-des'>
                                        <span>${tokens}</span>
                                    </div>
                                </li><!-- li -->
                                <li>
                                    <div class='tranx-purchase-head'>${status === 'Complete' ? 'Bonus (B)' : 'Expected Bonus (B)'}</div>
                                    <div class='tranx-purchase-des'>
                                        <span>${bonus}</span>
                                    </div>
                                </li><!-- li -->
                                <li>
                                    <div class='tranx-purchase-head'>${status === 'Complete' ? 'Total Tokens' : 'Expected Total'}</div>
                                    <div class='tranx-purchase-des'>
                                        <span>${parseFloat(tokens) + parseFloat(bonus)}</span>
                                        <span>(T+B)</span>
                                    </div>
                                </li><!-- li -->
                            </ul><!-- .tranx-purchase-info -->

                            <h6>Contribution To <em class='ti ti-arrow-right'></em> <span>${crypto === 'ETH' ? 'Ethereum' : 'Bitcoin'} Address</span></h6>
                            <div class='tranx-payment-info'>
                                <em class='${crypto !== "ETH" ? "fab fa-bitcoin" : "fab fa-ethereum"}'></em>
                                <input type='text' class='tranx-payment-address'
                                       value='${status === "Complete" ? receiver : status === "Pending" ? "Pending" : "Canceled"}' disabled>
                            </div><!-- .tranx-payment-info -->
                            
                            <div class='d-flex mx-auto justify-content-center'>
                                ${status === 'Pending' && cancelContribution ? `<button id='cancel-contribution-${trxNo}' class='btn btn-danger mt-3'>Cancel Contribution</button>` : ''}
                            </div>
                        </div><!-- .tranx-payment-details -->
                        </div>
                        
                        <div id='make-contribution' style='display:none' class='tranx-payment-details'>
                            <h5>Complete Contribution</h5>
                            <hr />
                            <p>Hi, Your transaction <strong>${trxNo}</strong> is still <strong>Pending Payment</strong><br>
                                You will receive <strong>${parseFloat(tokens) + parseFloat(bonus)} GAIN</strong> tokens (incl. bonus of ${bonus} GAIN) once paid.</p>
                            <h6>Please make your Payment to the Address below:</h6>
                            <div class='tranx-payment-info'>
                                <span class='tranx-copy-feedback copy-feedback'></span>
                                <em class='fab fa-ethereum'></em>
                                <input type='text' class='tranx-payment-address copy-address'
                                       value='${receiver}' disabled>
                                <a href='#' class='tranx-payment-copy copy-trigger'><em class='ti ti-files'></em></a>
                            </div><!-- .tranx-payment-info -->
                            <ul class='tranx-info-list'>
                                <li><span>SET GAS LIMIT:</span> 120 000</li>
                                <li><span>SET GAS PRICE:</span> 95 Gwei</li>
                            </ul><!-- .tranx-info-list -->
                            <div class='row'>
                                <div class='col-sm-5'>
                                    <div class='tranx-info-qr'>
                                        <span>OR Scan QR Code with your wallet app</span>
                                        <img class='tranx-info-qrimg' src='https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=${receiver}&choe=UTF-8' alt='QR Code'>
                                        <div class='gaps-4x'></div>
                                        <div class='gaps-2x d-sm-none'></div>
                                    </div>
                                </div><!-- .col -->
                                <div class="col-sm-7">
                                    <div class="note note-info">
                                        <em class="fas fa-info-circle"></em>
                                        <p>In case you send a different amount ETH, the number of GAIN tokens will
                                            update accordingly.</p>
                                    </div>
                                    <div class='gaps-1x'></div>
                                    <div class="note note-danger">
                                        <em class="fas fa-info-circle"></em>
                                        <p>Do not make payment through exchange (Kraken, Bitfinex). You can use
                                            MetaMask, MyEtherWallet, Mist wallets etc.</p>
                                    </div>
                                    <div class="gaps-1x"></div>
                                </div><!-- .col -->
                            </div><!-- .row -->
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="form-note">
                                        <a href='#' id='ok-btn' class='btn btn-light pay-done'>Done</a>
                                    </div>
                                     <!--<div class='gaps-1x'></div>-->
                                    <div class="form-note">
                                        <a href='#' id='cancel-btn' class='btn btn-xs btn-uline btn-uline-danger'>Back to Details</a>           
                                    </div>
                                </div>
                        </div><!-- .tranx-payment-details -->
        `;
        detailsModalBody.html(body);
        detailsModal.modal('show');
        $('#cont-link').click(() => {
            toggleModalView();
        });
        $('#ok-btn').click(() => {
            detailsModal.modal('hide');
        });
        $('#cancel-btn').click(() => {
            toggleModalView();
        });
        var $copy_trigger = $('.copy-trigger'),
            $copy_address = $('.copy-address'),
            $copy_feedback = $('.copy-feedback');
        if($copy_trigger.length > 0){
            copytoclipboard($copy_trigger,$copy_address,$copy_feedback);
        }
        if (status === 'Pending') {
            $(`#cancel-contribution-${trxNo}`).click(() => cancelContribution());
        }
    };

})(jQuery);