(($) => {

    'use strict';

    $(document).ready(() => {
        const updateAccBtn = $('#updateAccountInfoButton');
        const updateWalletBtn = $('#updateWalletInfoButton');
        const resendEmailBtn = $('#resendEmailBtn');
        const email = $('#email-address');
        const firstName = $('#first-name'), lastName = $('#last-name'), twitterHandle = $('#twitter-handle'),
            teleHandle = $('#telegram-handle'), dob = $('#date-of-birth'), country = $('#country'),
            savedChanges = $('#changesSaved'), notSaved = $('#notSaved');
        const walletTypeInp = $('#swallet'), walletAddressInp = $('#token-address'), saveWallet = $('#saveWallet'),
            notSaveWallet = $('#notSaveWallet'), passwordInp = $('#verify-password');

        firebaseUser.then((firebaseUser) => {
            let reFirstName, reLastName, reTwitterHandle, reTeleHandle, reDob, reCountry, reEmail;
            let reWalletType, reWalletAddress;
            //Pull current info from DB if any is present
            const ajax = $.ajax({
                                    url: '/backend/php/account.php',
                                    type: 'POST',
                                    data: {method: 'getUserInfo', uid: firebaseUser.uid},
                                    dataType: 'JSON',
                                    cache: false,
                                    async: true,
                                });
            ajax.always(() => {
                console.log("Fetch user data complete");
            });
            ajax.done((response) => {
                if (response.success) {
                    reFirstName = response.data['firstName'];
                    reLastName = response.data['lastName'];
                    reEmail = response.data['email'];
                    reTwitterHandle = response.data['twitter'];
                    reTeleHandle = response.data['telegram'];
                    reDob = response.data['dob'];
                    reCountry = response.data['country'];

                    firstName.val(response.data['firstName']);
                    lastName.val(response.data['lastName']);
                    email.val(response.data['email'] || firebaseUser.email);
                    twitterHandle.val(response.data['twitter']);
                    teleHandle.val(response.data['telegram']);
                    dob.val(response.data['dob']);
                    country.val(response.data['country']);
                    country.trigger('change');
                }
                else {
                    console.warn(response.errorMsg);
                }
            });
            ajax.fail((response) => {
                console.warn(`Fetch user data has failed --> ${JSON.stringify(response)}`);
                if (response.success) {
                    reFirstName = response.data['firstName'];
                    reLastName = response.data['lastName'];
                    reEmail = response.data['email'];
                    reTwitterHandle = response.data['twitter'];
                    reTeleHandle = response.data['telegram'];
                    reDob = response.data['dob'];
                    reCountry = response.data['country'];

                    firstName.val(response.data['firstName']);
                    lastName.val(response.data['lastName']);
                    email.val(response.data['email']);
                    twitterHandle.val(response.data['twitter']);
                    teleHandle.val(response.data['telegram']);
                    dob.val(response.data['dob']);
                    country.val(response.data['country']).prop('selected', true);
                    $(`#country option[value=${response.data['country']}]`).attr("selected", true);
                }
                else {
                    console.warn(response);
                }
            });

            const ajaxWallet = $.ajax({
                                          url: '/backend/php/account.php',
                                          type: 'POST',
                                          data: {method: 'getUserWallet', uid: firebaseUser.uid},
                                          dataType: 'JSON',
                                          cache: false,
                                          async: true,
                                      });
            ajaxWallet.always(() => {
                console.log("Fetch user wallet completed");
            });
            ajaxWallet.done((response) => {
                if (response.success) {
                    reWalletType = response.data['walletType'];
                    reWalletAddress = response.data['walletAddress'];
                    walletTypeInp.val(reWalletType);
                    walletTypeInp.trigger('change');
                    walletAddressInp.val(reWalletAddress);
                }
                else {
                    console.warn(response.errorMsg);
                }
            });
            ajaxWallet.fail((response) => {
                if (response.success) {
                    reWalletType = response.data['walletType'];
                    reWalletAddress = response.data['walletAddress'];
                    console.log(`Type: ${reWalletType} Address: ${reWalletAddress}`);
                    walletTypeInp.val(reWalletType);
                    walletTypeInp.trigger('change');
                    walletAddressInp.val(reWalletAddress);
                }
            });

            resendEmailBtn.click(() => {
                if (window.confirm("Would you like to resend your email verification message?")) {
                    firebaseUser.sendEmailVerification().then(() => {
                        console.log("New user verification email sent");
                    }).catch((err) => {
                        console.warn(`Unable to send new user verification email --> ${err.message || err}`);
                    });
                }
            });

            updateAccBtn.click(() => {
                const vFirstName = escapeHtml(firstName.val());
                const vLastName = escapeHtml(lastName.val());
                const vEmail = escapeHtml(email.val());
                const vTwitter = escapeHtml(twitterHandle.val());
                const vTele = escapeHtml(teleHandle.val());
                const vDob = dob.val().length > 11 ? dob.val().split(0, 11) : dob.val();
                const vCountry = escapeHtml(country.val());

                if (vFirstName && vLastName) {
                    if (!(vFirstName === reFirstName && vLastName === reLastName && vEmail === reEmail && vTwitter === reTwitterHandle && vTele === reTeleHandle && vDob === reDob && vCountry === reCountry)) {
                        const ajaxAccount = $.ajax({
                               url: '/backend/php/account.php',
                               type: 'POST',
                               data: {
                                   method: 'insertUserInfo',
                                   uid: firebaseUser.uid,
                                   firstName: vFirstName,
                                   lastName: vLastName,
                                   email: vEmail,
                                   twitter: vTwitter,
                                   telegram: vTele,
                                   dob: vDob,
                                   country: vCountry
                               },
                               dataType: 'JSON',
                               cache: false,
                               async: true,
                           });
                        ajaxAccount.done((response) => {
                            if (response.success) {
                                savedChanges.show('inline-block');
                                reFirstName = vFirstName;
                                reLastName = vLastName;
                                reEmail = vEmail;
                                reTwitterHandle = vTwitter;
                                reTeleHandle = vTele;
                                reDob = vDob;
                                reCountry = vCountry;
                            }
                            else {
                                savedChanges.hide('inline-block');
                                notSaved.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> User data failed to update');
                                notSaved.show('inline-block');
                            }
                        });
                        ajaxAccount.fail((response) => {
                            notSaved.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> User data failed to update');
                            notSaved.show('inline-block');
                        });

                    }
                    else {
                        savedChanges.hide('inline-block');
                        notSaved.show('inline-block');
                        notSaved.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> You must make changes in order to update data');
                    }
                }
                else {
                    savedChanges.hide('inline-block');
                    notSaved.show('inline-block');
                    notSaved.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> Required fields are empty');
                    if (!escapeHtml(firstName.val()))
                        flashDanger(firstName, 'alert-danger');

                    if (!escapeHtml(lastName.val()))
                        flashDanger(lastName, 'alert-danger');

                    if (!dob.val())
                        flashDanger(dob, 'alert-danger');

                    if (!escapeHtml(email.val()))
                        flashDanger(email, 'alert-danger');

                }
            });

            updateWalletBtn.click(() => {
                const walletType = walletTypeInp.val(), walletAddress = walletAddressInp.val(), password = passwordInp.val();
                const credential = firebase.auth.EmailAuthProvider.credential(firebaseUser.email, password);
                notSaveWallet.hide();
                saveWallet.hide();

                firebaseUser.reauthenticateAndRetrieveDataWithCredential(credential).then(() => {
                    if (walletAddress !== reWalletAddress || walletType !== reWalletType) {
                        const walletAjax = $.ajax({
                            url: '/backend/php/account.php',
                            type: 'POST',
                            data: {
                                method: 'insertUserWallet',
                                uid: firebaseUser.uid,
                                walletType: walletType,
                                walletAddress: walletAddress
                            },
                            dataType: 'JSON',
                            cache: false,
                            async: true,
                        });
                        walletAjax.done((response) => {
                            if (response.success) {
                                saveWallet.show('inline-block');
                                notSaveWallet.hide('inline-block');
                                reWalletType = walletType;
                                reWalletAddress = walletAddress;
                            }
                            else {
                                saveWallet.hide('inline-block');
                                notSaveWallet.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> Encountered an error saving wallet data');
                                notSaveWallet.show('inline-block');
                            }
                        });
                        walletAjax.fail((response) => {
                            saveWallet.hide('inline-block');
                            notSaveWallet.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> No Changes Have Been Made');
                            notSaveWallet.show('inline-block');
                        });
                    }
                    else {
                        //    Values haven't changed, show error message and user feedback
                        notSaveWallet.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> You must make changes in order to update wallet info');
                        saveWallet.hide('inline-block');
                        notSaveWallet.show('inline-block');
                    }
                }).catch((err) => {
                notSaveWallet.text(err.message || err);
                saveWallet.hide('inline-block');
                notSaveWallet.html('<em\n' + 'class="ti ti-alert d-inline-block"></em> Please verify your password to update your wallet address');
                notSaveWallet.show('inline-block');
            });
            });

            // UX - Clicks Update button on active tab when pressing enter
        });

        $(window).keyup((e) => {
            if (e.which === 13) {
                e.preventDefault();
                e.stopPropagation();
                const activePane = $('.active.tab-pane');
                // Figure out which tab is active
                if (activePane.hasClass('personal-data')) {
                    updateAccBtn.click();
                }
                else if (activePane.hasClass('wallet-address')) {
                    updateWalletBtn.click();
                }
                return false;
            }
            else {
                savedChanges.hide('inline-block');
                notSaved.hide('inline-block');
                saveWallet.hide('inline-block');
                notSaveWallet.hide('inline-block');
            }
        });

        const myModal = $('#myModal');
        country.on("change", function () {
            if ($(this).val() === 'United States') {
                myModal.modal({show: true});
            }
        });

        $('.selected').on('change', function () {
            var selected = $('#country').val();
            if (selected === 'US') {
                myModal.modal({show: true});
            }
        });
    });

})(jQuery);
