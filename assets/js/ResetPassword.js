(($) => {
    const actionCode = getParametersByName()['oobCode'];
    const apiKey = getParametersByName()['apiKey'];
    const continueUrl = getParametersByName()['continueUrl'];
    //endregion

    //region Init Firebase If Not Already Done
    if (!firebase.apps.length) {
        const config = {'apiKey': apiKey};
        firebase.initializeApp(config);
    }
    const auth = firebase.auth();

    $(document).ready(() => {
        //region Reset Password
        const resetPassBtn = $('#resetPasswordButton');
        let illegalChar = false;
        const resetHeader = $('#resetHeader'), note = $('#note'), noteSuccess = $('#noteSuccess'),
            noteBox = $('#noteBox'), noteBoxSuccess = $('#noteBoxSuccess'), newPassInp = $('#newPasswordInput'),
            repPassInp = $('#newPasswordRepeat'), userEmail = $('#userEmail');
        // If actionCode is not defined (manually routed to the page)
        if (!actionCode) {
            const loginHere = $('#loginHere');
            loginHere.attr('class', 'align-item-right');
            note.text("No action code has been given. Please request a password change, then check your inbox and follow the instructions.");
            noteBox.show('inline-block');
            return;
        }

        auth.verifyPasswordResetCode(actionCode).then((email) => {
            userEmail.val(email);
            resetPassBtn.click(() => {
                const newPass = escapeHtml(newPassInp.val()), repNewPass = escapeHtml(repPassInp.val());
                noteBox.hide('inline-block');
                noteBoxSuccess.hide('inline-block');
                resetHeader.hide('inline-block');
                const clearInp = (hide) => {
                    newPassInp.val('');
                    repPassInp.val('');
                    if (hide) {
                        newPassInp.hide();
                        repPassInp.hide();
                    }
                };
                if (newPass !== "" && repNewPass !== "" && newPass === repNewPass && !illegalChar) {
                    /* Complete password change process */
                    auth.confirmPasswordReset(actionCode, newPass).then((resp) => {
                        noteSuccess.text("Password has been updated!");
                        noteBoxSuccess.show('inline-block');
                        resetPassBtn.enable(false);
                        clearInp(true);
                    }).catch((err) => {
                        console.warn(`Error occurred during confirmation. Your action code may have expired, or your password is too weak. Please try again. ${err.message || err}`);
                        note.text(`Error occurred during confirmation. Your action code may have expired, or your password is too weak. Please try again. ${err.message || err}`);
                        noteBoxSuccess.hide('inline-block');
                        noteBox.show('inline-block');
                    });
                }
                else {
                    note.text(`Unable to set new password -> ${newPass === "" ? "New password is empty!" : newPass !== repNewPass ? "New passwords do not match!" : illegalChar ? "Password contains illegal characters. Please use only alphanumeric characters." : "Unknown Error!"}`);
                    noteBox.show('inline-block');
                    clearInp(false);
                }
            });

            newPassInp.keyup((e) => {
                if (e.which === 16 || e.which === 17) {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }

                // if (newPassInp.val().match(/[&<>"'`=\/]+$/g)) {
                illegalChar = !/^[a-zA-Z0-9]*$/.test(newPassInp.val());

                if (illegalChar) {
                    note.text("Password contains illegal characters. Please use only alphanumeric characters.");
                    noteBox.show('inline-block');
                    noteBoxSuccess.hide('inline-block');
                }
                else {
                    noteBox.hide('inline-block');
                }
            });
            repPassInp.keyup((e) => {
                if (e.which === 16 || e.which === 17) {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                // if (repPassInp.val().match(/[&<>"'`=\/]+$/g)) {
                illegalChar = !/^[a-zA-Z0-9]*$/.test(repPassInp.val());

                if (illegalChar) {
                    note.text("Password contains illegal characters. Please use only alphanumeric characters");
                    noteBox.show('inline-block');
                    noteBoxSuccess.hide('inline-block');
                }
                else {
                    noteBox.hide('inline-block');
                }
            });

            resetHeader.show();
            newPassInp.show();
            repPassInp.show();
            resetPassBtn.show();
            userEmail.show();

        }).catch((err) => {
            console.warn(`Error: Your action code may be invalid or expired. Try requesting another password reset. ${err.message || err}`);
            note.text(`Error: Your action code may be invalid or expired. Try requesting another password reset. ${err.message || err}`);
            noteBoxSuccess.hide('inline-block');
            noteBox.show('inline-block');
        });

        //endregion
    });

})(jQuery);