 (function($) {

     if (!firebase.apps.length) {
         const config = {
             apiKey: "AIzaSyDEOgJi9La3__DTHn9ZLeN9TTj_5P9eCh8",
             authDomain: "gainfytokenoffering.firebaseapp.com",
             databaseURL: "https://gainfytokenoffering.firebaseio.com",
             projectId: "gainfytokenoffering",
             storageBucket: "gainfytokenoffering.appspot.com",
             messagingSenderId: "777830695232"
         };
         // // Initialize Firebase
         firebase.initializeApp(config);
     }

     // // Initialize Firestore
     const db = firebase.firestore();
     // // Turn off deprecated settings
     db.settings({
         timestampsInSnapshots: true
     });

     const signOut = (dest = 'login.html', errorMsg = '') => {
         db.collection('accounts').doc(firebase.auth().currentUser.uid).set({
             two_factor_validated: false,
         }, {merge: true}).then(() => {
             firebase.auth().signOut().then(() => {
                 window.location.replace(dest);
             }).catch((err) => {console.warn(errorMsg ? `${errorMsg} :: Err ${err.code} ${err.message} ` : `Err ${err.code} ${err.message}`);});
         }).catch((err) => {
             // console.warn(`Error ${err.code} :: ${err.message}`);
             window.location.replace(dest);
         });
     };

})(jQuery);
