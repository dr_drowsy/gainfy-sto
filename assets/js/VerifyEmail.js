(($) => {

    const actionCode = getParametersByName()['oobCode'] || ' ';
    const apiKey = getParametersByName()['apiKey'];
    const continueUrl = getParametersByName()['continueUrl'];
    //endregion

    //region Init Firebase If Not Already Done
    if (!firebase.apps.length) {
        const config = {'apiKey': apiKey};
        firebase.initializeApp(config);
    }
    const auth = firebase.auth();

    $(document).ready(() => {
        //region Verify Email
        const verifyHeader = $('#verifyHeader');
        const noteBoxSuccess = $('#noteBoxSuccess'), noteBox = $('#noteBox'), note = $('#note'),
            noteSuccess = $('#noteSuccess');
        auth.applyActionCode(actionCode).then(() => {
            noteSuccess.text('Your email has been successfully verified!');
            noteBox.hide('inline-block');
            noteBoxSuccess.show('inline-block');
        }).catch((err) => {
            note.text(`Unable to verify email --> ${err.message || err}`);
            noteBoxSuccess.hide('inline-block');
            noteBox.show('inline-block');
        });
        //endregion
    });

})(jQuery);