(($) => {

    'use strict';

    //region Change Email
    if (!firebase.apps.length) {
        console.error("Firebase not initialized!");
    }

    const auth = firebase.auth();
    const changeEmailBtn = $('#changeEmailButton'), note = $('#note'), noteSuccess = $('#noteSuccess'), noteBox = $('#noteBox'), noteBoxSuccess = $('#noteBoxSuccess');
    changeEmailBtn.enable(false);

    auth.onAuthStateChanged((firebaseUser) => {
        if (firebaseUser) {
            const oldEmailInp = $('#oldEmailInput'), newEmailInp = $('#newEmailInput');
            oldEmailInp.val(firebaseUser.email);
            changeEmailBtn.enable(true);

            changeEmailBtn.click(() => {
                noteBox.hide('inline-block');
                noteBoxSuccess.hide('inline-block');
                const oldEmail = firebaseUser.email;
                const newEmail = escapeHtml(newEmailInp.val());
                if (newEmail !== "" && newEmail.includes('@') && oldEmail !== newEmail) {
                    firebaseUser.updateEmail(newEmail).then(() => {
                        noteSuccess.text(`Email address successfully changed to ${newEmail}`);
                        noteBoxSuccess.show('inline-block');
                        changeEmailBtn.enable(false);
                    }).catch((err) => {
                        console.warn(`Email change failed --> ${err.message || err}`);
                        note.text(`Email change failed -> ${err.message || err}`);
                        noteBoxSuccess.hide('inline-block');
                        noteBox.show('inline-block');
                        changeEmailBtn.enable(true);
                    });
                }
                else {
                    note.text(`Unable to change email to ${newEmail} -> ${newEmail === "" ? "No new email given!" : !newEmail.includes('@') ? "New email improperly formatted!" : oldEmail === newEmail ? "New email is the same as current email!" : "Unknown Error!"}`);
                    noteBox.show('inline-block');
                }
            });

            newEmailInp.keyup(() => {
                noteBox.hide('inline-block');
                noteBoxSuccess.hide('inline-block');
            })
        }
        else {
            console.warn("Firebase user not logged in");
        }
    });

    //endregion

})(jQuery);