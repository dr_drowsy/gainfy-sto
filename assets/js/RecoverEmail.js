(($) => {

    $(document).ready(() => {
        const actionCode = getParametersByName()['oobCode'] || ' ';
        const apiKey = getParametersByName()['apiKey'];
        const continueUrl = getParametersByName()['continueUrl'];
        //endregion

        //region Init Firebase If Not Already Done
        if (!firebase.apps.length) {
            const config = {'apiKey': apiKey};
            firebase.initializeApp(config);
        }
        const auth = firebase.auth();

        const sendPassResetBtn = $('#sendPasswordResetButton');
        const noteBox = $('#noteBox'), noteBoxSuccess = $('#noteBoxSuccess'), note = $('#note'),
            notesuccess = $('#noteSuccess');
        auth.checkActionCode(actionCode).then((info) => {
            restoredEmail = info['data']['email'];
            return auth.applyActionCode(actionCode);
        }).then(() => {
            notesuccess.text(`Your email address has been restored to ${restoredEmail}. If you think your account may have been compromised, you may send a password recovery email with the button below.`);
            noteBoxSuccess.show('inline-block');
            sendPassResetBtn.show('inline-block');
            sendPassResetBtn.click(() => {
                auth.sendPasswordResetEmail(restoredEmail).then(() => {
                    sendPassResetBtn.hide('inline-block');
                    notesuccess.text(`Password recovery email sent to ${restoredEmail}`);
                    noteBox.hide('inline-block');
                    noteBoxSuccess.show('inline-block')
                }).catch((err) => {
                    //    Problem sending password reset code
                    note.text(`There was a problem sending your password reset code --> ${err.message || err}`);
                    noteBoxSuccess.hide('inline-block');
                    noteBox.show('inline-block');
                })
            });
        }).catch((err) => {
            //    Invalid actionCode
            note.text(`Invalid action code --> ${err.message || err}`);
            noteBoxSuccess.hide('inline-block');
            noteBox.show('inline-block');
        });
    });

})(jQuery);