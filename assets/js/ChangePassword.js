(($) => {

    'use strict';

    //region Change Email
    if (!firebase.apps.length) {
        console.error("Firebase not initialized!");
    }

    const auth = firebase.auth();
    const changePassBtn = $('#change-password-btn'), oldPassInp = $('#old-password'), newPassInp = $('#new-password'),
        newPassConfInp = $('#confirm-password'), note = $('#pNote'), noteBox = $('#pNoteBox'),
        noteBoxSuccess = $('#pNoteBoxSuccess'), noteSuccess = $('#pNoteSuccess');
    let illegalChar = false;

    auth.onAuthStateChanged((firebaseUser) => {
        if (firebaseUser) {
            changePassBtn.click(() => {
                const currPass = oldPassInp.val(), newPass = newPassInp.val(), conPass = newPassConfInp.val();
                if (newPass !== '' && newPass.length >= 8 && newPass === conPass && newPass !== currPass) {
                    const credential = firebase.auth.EmailAuthProvider.credential(firebaseUser.email, currPass);
                    firebaseUser.reauthenticateAndRetrieveDataWithCredential(credential).then(() => {
                        firebaseUser.updatePassword(newPass).then(() => {
                            noteSuccess.text("Password successfully updated!");
                            noteBox.hide('inline-block');
                            noteBoxSuccess.show('inline-block');
                        }).catch((err) => {
                            note.text(err.message || err);
                            noteBoxSuccess.hide('inline-block');
                            noteBox.show('inline-block');
                        })
                    }).catch((err) => {
                        note.text(err.message || err);
                        noteBoxSuccess.hide('inline-block');
                        noteBox.show('inline-block');
                    });
                }
                else {
                    note.text(newPass === '' ? "New password cannot be empty." : newPass.length < 8 ? "New password must be 8 or more characters long." : newPass !== conPass ? "New password and confirm password must be the same." : newPass === currPass ? "New password must be different from current password." : "Unknown error!");
                    noteBoxSuccess.hide('inline-block');
                    noteBox.show('inline-block');
                }
            });

            oldPassInp.keyup((e) => {
                if (e.which === 16 || e.which === 17) {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }

                noteBox.hide('inline-block');
                noteBoxSuccess.hide('inline-block');
            });

            newPassInp.keyup((e) => {
                if (e.which === 16 || e.which === 17) {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }

                // if (newPassInp.val().match(/[&<>"'`=\/]+$/g)) {
                illegalChar = !/^[a-zA-Z0-9]*$/.test(newPassInp.val());

                if (illegalChar) {
                    note.text("Password contains illegal characters. Please use only alphanumeric characters.");
                    noteBox.show('inline-block');
                    noteBoxSuccess.hide('inline-block');
                }
                else {
                    noteBox.hide('inline-block');
                    noteBoxSuccess.hide('inline-block');
                }
            });
            newPassConfInp.keyup((e) => {
                if (e.which === 16 || e.which === 17) {
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                }
                // if (newPassConfInp.val().match(/[&<>"'`=\/]+$/g)) {
                illegalChar = !/^[a-zA-Z0-9]*$/.test(newPassConfInp.val());

                if (illegalChar) {
                    note.text("Password contains illegal characters. Please use only alphanumeric characters");
                    noteBox.show('inline-block');
                    noteBoxSuccess.hide('inline-block');
                }
                else {
                    noteBox.hide('inline-block');
                    noteBoxSuccess.hide('inline-block');
                }
            });
        }
        else {
            console.warn("Firebase user not logged in");
        }
    });

    //endregion

})(jQuery);