(($) => {
    'use strict';

    const reqEmail = false;

    if (!firebase.apps.length) {
        window.location.replace('login.html');
    }

    const db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });

    const auth = firebase.auth();

    auth.onAuthStateChanged(user => {
        if (!user) {
            window.location.replace('login.html');
        }
    });

})(jQuery);