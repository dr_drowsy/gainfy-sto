'use strict';

// // Initialize Firestore
const db = firebase.firestore();
// // Turn off deprecated settings
db.settings({
    timestampsInSnapshots: true
});

/* DECLARE ERROR CODES */
const ErrorCode = {
    CONTRIBUTION_TOO_LOW: '20',
    CONTRIBUTION_TOO_HIGH: '21',
    CANT_PREPARE_STATEMENT: '22',
    NO_BITCOIN_WALLETS: '23',
    NO_ETHEREUM_WALLETS: '24',
};
/* END ERROR CODES */

const Constants = {
    BONUS_RATE: 0.01,
    USD_TO_GAIN: 2,
    GAIN_TO_USD: 0.5,
    REFERRAL_BONUS: 25,
    MIN_CONTRIBUTION_USD: 100,
    MAX_CONTRIBUTION_USD: 100000,
    RATE_QUERY_TIMEOUT: 60000,
    DEFAULT_CONTRIBUTION: 200,
    CONTRIBUTION_TTL: 2, // In days
};

const auth = firebase.auth();

const $win = $(window), $body_m = $('body'), $navbar = $('.navbar');

const entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}

function signOut(dest = 'login.html', errorMsg = 'Encountered an error') {

    if (!firebase.apps.length) {
        console.warn("Cannot utilize all Functions.js because Firebase is not initialized!");
    }

    const db = firebase.firestore();
    const auth = firebase.auth();
    // Turn off deprecated settings
    db.settings({
        timestampsInSnapshots: true
    });

    db.collection('accounts').doc(auth.currentUser.uid).set({
        two_factor_validated: false,
    }, {merge: true}).then(() => {
        auth.signOut().then(() => {
            window.location.replace(dest);
        }).catch((err) => {
            console.warn(errorMsg ? `${errorMsg} :: Err ${err.code} ${err.message || err} ` : `Err ${err.code} ${err.message || err}`);
        });
    }).catch((err) => {
        console.warn(`Error ${err.code} :: ${err.message || err}`);
        window.location.replace(dest);
    });
}

//region Filter Parameters Out Of Url
function getParametersByName() {
    let params = {};
    let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, (m, key, value) => {
        params[key] = value;
    });
    return params;
}
//endregion

function flashDanger(component, className) {
    component.addClass(className);
    setTimeout(() => {
        component.removeClass(className);
        setTimeout(() => {
            component.addClass(className);
            setTimeout(() => {
                component.removeClass(className);
            }, 800);
        }, 500);
    }, 800);
}

// Toggle
function toggleTab(triger,action,connect){
    triger.on('click',function(){
        action.addClass('active');
        if(connect.hasClass('active')){
            connect.removeClass('active');
        }
        return false;
    });
}

//Copy Text to Clipboard
function copytoclipboard(triger,action,feedback){
    let $triger = triger, $action = action, $feedback = feedback;
    $triger.on('click',function(){
        let $this = $(this);
        $this.parent().find($action).removeAttr('disabled').select();
        document.execCommand("copy");
        $feedback.text('Copied to Clipboard').fadeIn().delay(1000).fadeOut();
        $this.parent().find($action).attr('disabled', 'disabled');
        return false;
    });
}

// Function For Toggle Class On click
function tglcls(tigger,action,connect,connect2){
    let $toglr = tigger, $tgld = action,$tgcon = connect,$tgcon2 = connect2;
    $toglr.on("click",function(){
        $tgld.toggleClass('active');
        $toglr.toggleClass('active');
        if($tgcon.hasClass('active')){
            $tgcon.removeClass('active');
        }
        if($tgcon2.hasClass('active')){
            $tgcon2.removeClass('active');
        }
        return false;
    });
}

// Get Window Width
function winwidth () {
    return $win.width();
}

/* Self Invoking */
(($) => {
    const auth = firebase.auth();
    const $userName = $('.user-name');
    const $userNameDropdown = $('.user-dropdown-name');
    const $userEmailDropdown = $('.user-dropdown-email');
    const $verifyEmailBtn = $('.confirm-email-btn');
    const emailNotConfirmed = $('#email-not-confirmed');
    const uidLabel = $('#uidLabel');
    const uidInput = $('#unique-id-input');
    const uidSpan = $('#uidSpan');
    $userName.html('');
    $userNameDropdown.html('');
    $userEmailDropdown.html('');

    auth.onAuthStateChanged((firebaseUser) => {
         if (firebaseUser) {
             $userName.html(firebaseUser.email);
             $userNameDropdown.html(firebaseUser.displayName || null);
             $userEmailDropdown.html(firebaseUser.email);
             uidSpan.text(firebaseUser.uid);
             if (uidLabel) {
                 uidLabel.text(firebaseUser.uid);
             }

             if (uidInput) {
                 uidInput.html(firebaseUser.uid);
             }

             if (!firebaseUser.emailVerified) {
                 if (emailNotConfirmed)
                     emailNotConfirmed.show('inline-block');
                 $verifyEmailBtn.show();
                 $verifyEmailBtn.click(() => {
                    if (window.confirm("Would you like to resend your email verification message?")) {
                        firebaseUser.sendEmailVerification().then(() => {
                            console.log("New user verification email sent");
                        }).catch((err) => {
                            console.warn(`Unable to send new user verification email --> ${err.message || err}`);
                        });
                    }
                 });
             }
             else {
                 $verifyEmailBtn.hide();
                 if (emailNotConfirmed)
                    emailNotConfirmed.hide();
             }

             const reserveTokens = $('#reserveTokensLink');
             if (reserveTokens) {
                 reserveTokens.click(function() {
                     $.ajax({
                         url:"/backend/php/accountCheck.php",
                         method:"POST",
                         dataType: 'JSON',
                         data: { UserID: firebaseUser.uid },
                         success: function (response){
                             const accountCheck = response.accountCheck;
                             console.log("account check box", accountCheck );
                             if (accountCheck) {
                                 window.location.replace('tokens.html');
                             }
                             else {
                                 alert("Please fill out your Basic Information on the Account's Page");
                             }
                         }
                     });
                 });
             }
         }
    });

    // Finds every logout button on the page and sets an event listener to log the user out correctly
    const logoutButton = $("[type*='logout-button']");
    logoutButton.click(() => {
        console.log("Clicked logout...");
        signOut('login.html', 'User is being logged out!');
    });

    

    // Touch Class
    if (!("ontouchstart" in document.documentElement)) {
        $body_m.addClass("no-touch");
    }

    let wwCurrent = winwidth();
    $win.on('resize', function () {
        wwCurrent = winwidth();
    });

    // Sticky
    let $is_sticky = $('.is-sticky');
    if ($is_sticky.length > 0 ) {
        let $navm = $('#mainnav').offset();
        $win.scroll(function(){
            let $scroll = $win.scrollTop();
            if ($win.width() > 991) {
                if($scroll > $navm.top ){
                    if(!$is_sticky.hasClass('has-fixed')) {$is_sticky.addClass('has-fixed');}
                } else {
                    if($is_sticky.hasClass('has-fixed')) {$is_sticky.removeClass('has-fixed');}
                }
            } else {
                if($is_sticky.hasClass('has-fixed')) {$is_sticky.removeClass('has-fixed');}
            }
        });
    }

    // Active page menu when click
    let CurURL = window.location.href, urlSplit = CurURL.split("#");
    let $nav_link = $("a");
    if ($nav_link.length > 0) {
        $nav_link.each(function() {
            if (CurURL === (this.href) && (urlSplit[1]!=="")) {
                $(this).closest("li").addClass("active").parent().closest("li").addClass("active");
            }
        });
    }

    // Select
    let $selectbox = $('.input-select, select');
    if ($selectbox.length > 0) {
        $selectbox.each(function() {
            let $this = $(this);
            $this.select2();
        });
    }

    let $toggle_action = $('.toggle-action'),
        $topbar_action = $('.topbar-action'),
        $toggle_nav = $('.toggle-nav'),
        $sidebar = $('.user-sidebar'),
        $sidebar_overlay = $('.user-sidebar-overlay');
    if ($toggle_action.length > 0 ) {
        tglcls($toggle_action,$topbar_action,$sidebar,$toggle_nav);
    }
    if ($toggle_nav.length > 0 ) {
        tglcls($toggle_nav,$sidebar,$topbar_action,$toggle_action);
    }
    if ($sidebar_overlay.length > 0 ) {
        $sidebar_overlay.on("click",function(){
            $sidebar.removeClass('active');
            $toggle_nav.removeClass('active');
        });
    }
    if(wwCurrent < 991){
        $sidebar.delay(500).addClass('user-sidebar-mobile');
    }else{
        $sidebar.delay(500).removeClass('user-sidebar-mobile');
    }
    $win.on('resize', function () {
        if(wwCurrent < 991){
            $sidebar.delay(500).addClass('user-sidebar-mobile');
        }else{
            $sidebar.delay(500).removeClass('user-sidebar-mobile');
        }
    });



    // Countdown Clock
    let $count_token_clock = $('.token-countdown-clock');
    if ($count_token_clock.length > 0 ) {
        $count_token_clock.each(function() {
            let $self = $(this), datetime = $self.attr("data-date");
            $self.countdown(datetime).on('update.countdown', function(event) {
                $(this).html(event.strftime('' + '<div class="col"><span class="countdown-time countdown-time-first">%D</span><span class="countdown-text">Days</span></div>' + '<div class="col"><span class="countdown-time">%H</span><span class="countdown-text">Hours</span></div>' + '<div class="col"><span class="countdown-time countdown-time-last">%M</span><span class="countdown-text">Minutes</span></div>'));
            });
        });
    }



/*    // Activity Data Table
    let $activity_table = $('.activity-table');
    if($activity_table.length > 0){
        $activity_table.DataTable({
            ordering: false,
            autoWidth: false,
            dom:'<"row"<"col-12"<"overflow-x-auto"t>>><"row align-items-center"<"col-sm-6 text-left"p><"col-sm-6 text-sm-right text-center"<"clear-table">>>',
            pageLength: 7,
            bPaginate : $('.data-table tbody tr').length>7,
            iDisplayLength: 7,
            language: {
                info: "_START_ -_END_ of _TOTAL_",
                infoEmpty: "No records",
                infoFiltered: "( Total _MAX_  )",
                paginate: {
                    first:      "First",
                    last:       "Last",
                    next:       "Next",
                    previous:   "Prev"
                },
            },
        });
        $(".clear-table").append('<a href="#" class="btn btn-primary btn-xs clear-activity">Clear Activity</a>');
    }*/

    // Payment Check (token page)
    let $payment_check = $('.payment-check'), $payment_btn = $('.payment-btn');
    if($payment_check.length > 0){
        $payment_check.on('change', function(){
            let _thisval = $(this).val(), _thisHash = '#'+_thisval;
            $payment_btn.attr('data-target', _thisHash)
        });
    }


    // Tooltip
    let $tooltip = $('[data-toggle="tooltip"]');
    if($tooltip.length > 0){
        $tooltip.tooltip();
    }

    // Date Picker
    let $date_picker = $('.date-picker');
    if($date_picker.length > 0){
        $date_picker.each(function(){
            $date_picker.datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                startView: "0",
                minViewMode: "0",
            }).datepicker('update', new Date());
        });
    }




    let $copy_trigger = $('.copy-trigger'),
        $copy_address = $('.copy-address'),
        $copy_feedback = $('.copy-feedback');
    if($copy_trigger.length > 0){
        copytoclipboard($copy_trigger,$copy_address,$copy_feedback);
    }

    // Make pay
    let $make_pay = $('.make-pay'),
        $pay_done = $('.pay-done'),
        $tranx_payment_details = $('.tranx-payment-details'),
        $tranx_purchase_details = $('.tranx-purchase-details');
    if($make_pay.length > 0){
        toggleTab($make_pay,$tranx_payment_details,$tranx_purchase_details)
    }
    if($pay_done.length > 0){
        toggleTab($pay_done,$tranx_purchase_details,$tranx_payment_details)
    }

    // Ath Open
    /*let $ath_trigger = $('.ath-trigger'), $ath_content = $('.ath-content');
    if($ath_trigger.length > 0){
        $ath_trigger.on('click', function(){
            $ath_content.slideDown();
            return false;
        });
    }*/



    // Dropzone
    let $upload_zone = $('.upload-zone');
    if ($upload_zone.length > 0 ) {
        $upload_zone.each(function(){
            let $self = $(this);
            $self.addClass('dropzone').dropzone({ url: "/file/post" });
        });
    }

})(jQuery);