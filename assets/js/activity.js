(function ($) {
    const tableBody = $('#tableBody');

    firebaseUser.then(firebaseUser => {
        $.ajax({
            url : "/backend/php/activityGetter.php",
            type : "POST",
            dataType : "JSON",
            cache : false,
            async : true,
            data : {
                method : 'getActivity',
                uid : firebaseUser.uid,
            },
        })
            .fail(response => console.log(JSON.stringify(response)))
            .done(response => {
                let data = response.data;

                data.map(row => {
                    let trow = `
                    <tr>
                      <td class='activity-time'>${row.Date}</td>
                      <td class='activity-device'>${row.Device}</td>
                      <td class='activity-browser'>${row.Browser}</td>
                      <td class='activity-ip'>${row.IP}</td>
                    </tr>
                    `;

                    tableBody.append($.parseHTML(trow));
                });

                // Activity Data Table
                let $activity_table = $('.activity-table');
                if ($activity_table.length > 0) {
                    $activity_table.DataTable({
                        ordering : false,
                        autoWidth : false,
                        dom : '<"row"<"col-12"<"overflow-x-auto"t>>><"row align-items-center"<"col-sm-6 text-left"p><"col-sm-6 text-sm-right text-center"<"clear-table">>>',
                        pageLength : 7,
                        bPaginate : $('.data-table tbody tr').length > 7,
                        iDisplayLength : 7,
                        language : {
                            info : "_START_ -_END_ of _TOTAL_",
                            infoEmpty : "No records",
                            infoFiltered : "( Total _MAX_  )",
                            paginate : {
                                first : "First",
                                last : "Last",
                                next : "Next",
                                previous : "Prev",
                            },
                        },
                    });
                    // $(".clear-table").append('<a href="#" class="btn btn-primary btn-xs clear-activity" id="clearActivity">Clear Activity</a>');
                }
            });
    });
})(jQuery);
