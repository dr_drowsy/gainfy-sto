<?php

$to = "mszczech01@gmail.com";
$firstLogin = false;
$purchaseComplete = false;
$invitation = false;


if($firstLogin == true)
{
    $subject = "Gainfy Token Offering Registration";
$firstLoginEmail = `
<p><strong>Welcome to the Gainfy Community!</strong></p>
<p>Thanks for registering for the GAIN Token Offering.</p>
<p>Be sure to <a href='https://sto.gainfy.com/kyc.html'><strong>complete your KYC/AML and /or Accredited Investor info</strong></a> and to designate where you would like your tokens to be sent to by <a href='https://sto.gainfy.com/account.html'><strong>updating your return address</strong></a> in settings!</p>
<p>In the meantime - check us out on <a href='http://www.twitter.com/gainfy'><strong>Twitter</strong></a>, in our <a href='https://t.me/gainfy_chat'><strong>Telegram</strong></a> group, and visit the official <a href='https://gainfy.com/'><strong>Gainfy website</strong></a>.</p>
<p>Best,</p>
<p>The Gainfy Team</p>
<p>Gainfy Foundation | <a href='http://www.gainfy.com/'>GAINFY</a></p>
<p><a href='mailto:team@gainfy.com'>team@gainfy.com</a></p>
<p>----------------------------------------</p>
<p><a href='http://www.twitter.com/gainfy'>Twitter</a>|<a href='https://www.linkedin.com/company/11247412/'>Linkedin</a>| <a href='https://t.me/gainfy_chat'>Telegram</a></p>
`;

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// More headers
$headers .= 'From: <support@gainfy.org>' . "\r\n";
mail($to,$subject,$firstLoginEmail,$headers);

}

if($purchaseComplete ==true)
{
    $subject = "Contribution Confirmation: GAIN Token";

    $purchaseCompleteEmail =`
<p><strong>Contribution Confirmation</strong></p>
<p>Your contribution has been confirmed. You may <a href='https://sto,gainfy.com/transactions.html'><strong>view your transaction info here</strong></a>.</p>
<p> Further payments to this address will not be accepted. To make another contribution, please visit the <a href='https://sto.gainfy.com/tokens.html'><strong> Reserve Tokens</strong></a> page.</p>
<p>If you have not completed your indetity verification, be sure to <a href='https://sto.gainfy.com/kyc.html'><strong>complete your KYC/AML and /or Accredited Investor info</strong></a> and to designate where you would like your tokens to be sent to by <a href='https://sto.gainfy.com/account.html'><strong>updating your return address</strong></a> in settings by December, 31, 2018!</p>
<p>Stop by the official <a href='https://gainfy.com/''><strong>Gainfy website</strong></a>to learn more about the Token offering.</p>
<p>Best,</p>
<p>The Gainfy Team</p>
<p>Gainfy Foundation | <a href='http://www.gainfy.com/'>GAINFY</a></p>
<p><a href='mailto:team@gainfy.com'>team@gainfy.com</a></p>
<p>----------------------------------------</p>
<p><a href='http://www.twitter.com/gainfy'>Twitter</a>|<a href='https://www.linkedin.com/company/11247412/'>Linkedin</a>| <a href='https://t.me/gainfy_chat'>Telegram</a></p>
`;
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <support@gainfy.org>' . "\r\n";


mail($to,$subject,$purchaseCompleteEmail,$headers);

}



if($invitation==true)
{
    $subject = "You have been invited to join the Gainfy Token Offering";

$invitationEmail = `
<p>Hello,</p>
<p>The Gainfy team has invited you to participate in the GAIN Token Sale with an exclusive offer prepared for you.</p>
<p><a href='https://sto.gainfy.com/tokens.html'>Get GAIN</a></p>
<p><strong>Important:</strong></p>
<p>We only accept BTC and ETH via our payment portal.</p>
<p>We accept FIAT via direct wire transfer only. The wire instruction will be emailed to you after you verify <a href='https://sto.gainfy.com/kyc.html'><strong>KYC/AML</strong></a>.</p>
<p>Stop by the official <a href='https://gainfy.com/''><strong>Gainfy website</strong></a> to learn more about the sale.</p>
<p>Best,</p>
<p>The Gainfy Team</p>
<p>Gainfy Foundation | <a href='http://www.gainfy.com/'>GAINFY</a></p>
<p><a href='mailto:team@gainfy.com'>team@gainfy.com</a></p>
<p>----------------------------------------</p>
<p><a href='http://www.twitter.com/gainfy'>Twitter</a>|<a href='https://www.linkedin.com/company/11247412/'>Linkedin</a>| <a href='https://t.me/gainfy_chat'>Telegram</a></p>
`;

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: <support@gainfy.org>' . "\r\n";

mail($to,$subject,$invitationEmail,$headers);
}
?>
