<?php
    require_once('connection.php');


    //    $email = filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_STRING);
    echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

    function getEthTransactions($conn) {
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);

        $return = new stdClass();

        fetchEthByStatus($conn, 'Pending', $uid, $return);
        //            TODO: May need to change Complete to Completed depending on server syntax
        fetchEthByStatus($conn, 'Complete', $uid, $return);
        fetchEthByStatus($conn, 'Canceled', $uid, $return);

        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }

    function getBtcTransactions($conn) {
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);

        $return = new stdClass();

        fetchBtcByStatus($conn, 'Pending', $uid, $return);
        //            TODO: May need to change Complete to Completed depending on server syntax
        fetchBtcByStatus($conn, 'Complete', $uid, $return);
        fetchBtcByStatus($conn, 'Canceled', $uid, $return);

        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }

    function &fetchEthByStatus($conn, $status, $uid, $return) {
        $sqlSelect = "SELECT * FROM EthereumTransactions WHERE UniqueID = ? AND TransactionStatus = ?;";
        $transactions = array();

        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
            $return->errorLog = $return->errorLog ?? '' . ' Unable to prepare statement, ' . mysqli_error($conn) . '. ';
        }


        mysqli_stmt_bind_param($stmt, "ss", $uid, $status);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);

        if (!empty($results)) {
            while ($row = mysqli_fetch_assoc($results)) {
                array_push($transactions, $row);
            }
            $return->transactions[$status] = $transactions;
        }
        else {
            $return->errorLog = $return->errorLog ?? '' . ' Error, should have returned a value. ';
        }

        return $return;
    }

    function &fetchBtcByStatus($conn, $status, $uid, $return) {
        $sqlSelect = "SELECT * FROM BitcoinTransactions WHERE UniqueID = ? AND TransactionStatus = ?;";
        $transactions = array();

        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
            $return->errorLog = $return->errorLog ?? '' . ' Unable to prepare statement, ' . mysqli_error($conn) . '. ';
        }


        mysqli_stmt_bind_param($stmt, "ss", $uid, $status);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);

        if (!empty($results)) {
            while ($row = mysqli_fetch_assoc($results)) {
                array_push($transactions, $row);
            }
            $return->transactions[$status] = $transactions;
        }
        else {
            $return->errorLog = $return->errorLog ?? '' . ' Error, should have returned a value. ';
        }

        return $return;
    }

    function cancelEthContribution($conn) {
        $trxId = filter_input(INPUT_POST, 'trxId', FILTER_SANITIZE_NUMBER_INT);
        $return = new stdClass();

        $stmt = mysqli_stmt_init($conn);
        $sqlUpdate = "UPDATE EthereumTransactions SET TransactionStatus = 'Canceled' WHERE TransactionID = ?;";
        if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
            mysqli_stmt_bind_param($stmt, "i", $trxId);
            mysqli_stmt_execute($stmt);
            $return->success = true;
        }
        else {
            $return->success = false;
            Utility::AppendToErrorLog($return, 'Unable to prepare update tranx status statement: ' . mysqli_error($conn) . '. ');
        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        $json = json_encode($return);
        echo $json;
    }

    function cancelBtcContribution($conn) {
        $trxId = filter_input(INPUT_POST, 'trxId', FILTER_SANITIZE_NUMBER_INT);
        $return = new stdClass();

        $stmt = mysqli_stmt_init($conn);
        $sqlUpdate = "UPDATE BitcoinTransactions SET TransactionStatus = 'Canceled' WHERE TransactionID = ?;";
        if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
            mysqli_stmt_bind_param($stmt, "i", $trxId);
            mysqli_stmt_execute($stmt);
            $return->success = true;
        }
        else {
            $return->success = false;
            Utility::AppendToErrorLog($return, 'Unable to prepare update tranx status statement: ' . mysqli_error($conn) . '. ');
        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        $json = json_encode($return);
        echo $json;
    }

    /*function getUserTransactions($conn) {
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $crypto = filter_input(INPUT_POST, 'crypto', FILTER_SANITIZE_STRING);

        $return = new stdClass();

        getAssignedWallets($conn, $crypto, $uid, $return);
        $wallets = $return->assignedWallets;
        $return->wallets = $wallets;

        if (!empty($wallets)) {
            foreach ($wallets as $wallet) {
                getWalletTransactions($conn, $wallet, $crypto, $return);
            }
        }
        else {
            //        User has no wallets assigned
            $return->errorLog = $return->errorLog ?? '' . 'No wallets have been assigned to this user.';
        }

        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }

    function &getAssignedWallets($conn, $uid, $crypto, $return) {
        $from = strtolower($crypto) == 'eth' ? 'EthereumWallets' : 'BitcoinWallets';

        $sqlSelect = "SELECT WalletAddress FROM EthereumWallets WHERE UniqueID = ?;";

        //        $results = mysqli_query($conn, $sqlSelect);
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
            $return->errorLog = $return->errorLog ?? '' . ' Unable to prepare statement ' . mysqli_error($conn) . ', ';
            return $return;
        }

        mysqli_stmt_bind_param($stmt, "s", $uid);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);
        $return->results = $results;

        if (!empty($results)) {
            $row = mysqli_fetch_all($results);
            $return->assignedWallets = $row;
        }
        else {
            $return->assignedWallets = null;
        }

        return $return;
    }

    function &getWalletTransactions($conn, $address, $crypto, $return) {
        $from = strtolower($crypto) == 'eth' ? 'EthereumTransactions' : 'BitcoinTransactions';

        $sqlSelect = "SELECT TransactionID, TransactionStatus FROM " . $from . " WHERE ReceivingAddress = " . $address . ";";

        $results = mysqli_query($conn, $sqlSelect);

        if (!empty($results)) {
            while ($row = mysqli_fetch_assoc($results)) {
                if ($row['TransactionStatus'] == "Pending") {
                    $sqlSelect = "SELECT TransactionID, ExpectedReservedTokens, ExpectedReservedBonus, ExpectedContributionAmount FROM EthereumTransactions WHERE TransactionID = " . $row['TransactionID'] . ";";
                }
                else {
                    if ($row['TransactionStatus'] == 'Complete') {
                        $sqlSelect = "SELECT TransactionID, ActualReservedTokens, ActualBonusTokens, ContributionAmountReceived FROM EthereumTransactions WHERE TransactionID = " . $row['TransactionID'] . ";";
                    }
                }

                if (!empty($result = mysqli_query($conn, $sqlSelect))) {
                    $transaction = mysqli_fetch_assoc($result);
                    $return->data['transactions'][$transaction['TransactionID']] = $transaction;
                    $return->log = $return->log ?? '' . 'Found result ' . $transaction['TransactionID'] . ', ';
                }
            }
            $return->log = $return->log ?? '' . 'Successfully fetched all Wallet Transactions. ';
        }
        else {
            $return->errorLog = $return->errorLog ?? '' . 'Query failed. No results from EthTranx at that address: ' . mysqli_error($conn) . ', ';
        }

        return $return;
    }*/

    //    OLD CODE
    /*echo "<p>User ID: " . $uid . "</p>";
    $URL = "https://api.etherscan.io/api?module=account&action=balance&address=";
    $EtherscanAPIkey = "TBJVU6FBAWW3CHYI7NKT4BBDEHHSAEA9KF";

    $assignedWalletGetter = "Select WalletAddress From EthereumWallets WHERE UniqueID = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $assignedWalletGetter)) {
        echo "sql statement failed";
    }
    else {
        mysqli_stmt_bind_param($stmt, 's', $uid);
        mysqli_stmt_execute($stmt);
        // mysqli_stmt_bind_result($stmt, $address);
        //
        //  mysqli_stmt_fetch($stmt);
        mysqli_stmt_bind_result($stmt, $result);
        $results = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_all($results)) {
            $arr = $row;
        }


        //         echo'  <tr>
        //                <td class="activity-time">'.$arr[$i][1].'</td>
        //                <td class="activity-device">'.$arr[$i][2].'</td>
        //                <td class="activity-browser">'.$arr[$i][3].'</td>
        //                <td class="activity-ip">'.$arr[$i][4].'</td>
        //            </tr>';
        //
        // }
        mysqli_stmt_close($stmt);
    }
    //var_export($arr);
    $arrSize = sizeof($arr);
    for ($i = 0; $i <= $arrSize; $i++) {
        $address = $arr[$i][0];

        //if($address != NULL){
        if ($arrSize > 0 && $address != NULL) {
            //$testAddr = "0x5b528833404224ebB89F4751A13b55072569186F";
            $etherURL = $URL . $address . "&tag=latest&apikey=" . $EtherscanAPIkey;

            //$etherURL = $URL.$address."&tag=latest&apikey=".$EtherscanAPIkey;

            $curl = curl_init("$etherURL");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $etherResult = curl_exec($curl);
            $etherJson = json_decode($etherResult, true);
            $etherBalance = (float)$etherJson["result"] / pow(10, 18);
            //$ethPrice = $ethJson->"amount";
            curl_close($curl);
            //$ethPrice = $ethJson['data']['amount'];
            echo "<p>Etherscan Result:" . $etherBalance . "</p>";
            //echo "<p>EthPrice:".$ethJson."</p>";

            $rateGetter = "Select TransactionStatus From EthereumTransactions WHERE ReceivingAddress ='" . $address . "';";
            if ($result = mysqli_query($conn, $rateGetter)) {
                $row = mysqli_fetch_array($result);
                $TransactionStatus = $row[0];
            }
            if ($address == '0xe200B7064f063AB830E03851AaA9272341a352B4') {
                $etherBalance = '.5';
            }
            if ($etherBalance != '0' && $TransactionStatus == 'Pending') {
                echo "<p>insideEtherBalanceif:" . $etherBalance . "</p>";
                $ContributionAmountReceived = $etherBalance;
                $rateGetter = "Select EthereumRate From EthereumTransactions WHERE ReceivingAddress ='" . $address . "';";
                if ($result = mysqli_query($conn, $rateGetter)) {
                    $row = mysqli_fetch_array($result);

                    $EthereumRate = (float)$row[0];
                    $ActualReservedTokens = ((float)$ContributionAmountReceived * (float)$EthereumRate) / .5;
                    $ActualReservedBonus = ((float)$ActualReservedTokens * .10);
                    $TotalTokens = ((float)$ActualReservedTokens + (float)$ActualReservedBonus);
                    $sqlUpdate = "Update EthereumTransactions SET ContributionAmountReceived = ? , ActualReservedTokens = ? ,
      ActualBonusTokens = ?, TransactionStatus = 'Complete' WHERE ReceivingAddress ='" . $address . "';";
                    // $sqlUpdateFull = $sqlUpdate.$sqlUpdate1.$sqlUpdate2;
                    echo "<p>sql Statement :" . $sqlUpdate . "</p>";
                    $stmt = mysqli_stmt_init($conn);
                    if (!mysqli_stmt_prepare($stmt, $sqlUpdate)) {
                        //echo '<p>'."sql prepare statement failed".'</p>';
                    }
                    else {
                        mysqli_stmt_bind_param($stmt, "sss", $ContributionAmountReceived, $ActualReservedTokens, $ActualReservedBonus);
                        mysqli_stmt_execute($stmt);
                        mysqli_stmt_close($stmt);
                    }
                    $gainfyImageLink = "https://cdn-images-1.medium.com/max/1600/1*RjCPMIKLWSjdR2yhhI4AFA.png";
                    $subject = "Gainfy Token Offering Contribution";
                    $Confirmation = '
     <html>
     <head>
     <p style='font - size:22px'><strong> Gainfy has Received your contribution!</strong></p>
     </head>
     <body>
     <img src=''.$gainfyImageLink.'' style='height: 160px; width: 400px;'/>
     <p style='font - size:18px'>Your contribution has been confirmed. You may <a href='https://sto,gainfy.com/transactions.html'><strong>view your transaction info here</strong></a>.</p>
     <p > We have confirmed processing your contribution of '.$ContributionAmountReceived.' ETH for a total reservation of '.$TotalTokens.' GAIN Tokens(including the bonus).</p >
     <p > Further contributions to this address will not be accepted . To make another contribution, please visit the < a href = 'https://sto.gainfy.com/tokens.html' ><strong > Reserve Tokens </strong ></a > page .</p >
     <p > Be sure to < a href = 'https://sto.gainfy.com/kyc.html' ><strong > complete your KYC / AML and /or Accredited Investor info </strong ></a >
     and to designate where you would like your tokens to be sent to by updating < a href = 'https://sto.gainfy.com/account.html' ><strong > your return address </strong ></a > on the account page!</p >
     <p > In the meantime - check us out on < a href = 'http://www.twitter.com/gainfy' ><strong > Twitter</strong ></a >, in our < a href = 'https://t.me/gainfy_chat' ><strong > Telegram</strong ></a > group, and visit the official < a href = 'https://gainfy.com/' ><strong > Gainfy website </strong ></a >.</p >
     <p > Best,</p >
     <p > The Gainfy Team </p >
     <p > Gainfy Foundation | <a href = 'http://www.gainfy.com/' > GAINFY</a ></p >
     <p ><a href = 'mailto:team@gainfy.com' > team@gainfy . com </a ></p >
     <p > ----------------------------------------</p >
     <p ><a href = 'http://www.twitter.com/gainfy' > Twitter</a >|<a href = 'https://www.linkedin.com/company/11247412/' > Linkedin</a >| <a href = 'https://t.me/gainfy_chat' > Telegram</a ></p >
     </body >
     </html >
                    ';
     $headers = "MIME-Version: 1.0" . "\r\n";
     $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
     $headers .= 'From: <Contribution@sto . gainfy . com > ' . "\r\n";

     mail($Email,$subject,$Confirmation,$headers);

}
$sqlUpdate = "Update EthereumWallets SET TransactionStatus = 'Complete' Where WalletAddress = '".$address."';";
if (mysqli_query($conn, $sqlUpdate)) {
//echo "Record updated successfully";
} else {
//echo "Error updating record: " . mysqli_error($conn);
}

}
else
{
    //echo "<p>Transaction Balance is zero!</p>";
}

$transactionGetter  = "Select * From EthereumTransactions WHERE ReceivingAddress ='".$address."';";
if($result = mysqli_query($conn,$transactionGetter)){
$row = mysqli_fetch_array($result);
//echo $row;
$TransactionID = $row[0];
$ReceivingAddress =$row[1];
$ExpectedReservedTokens=(float)$row[2];
$ExpectedReservedBonus=(float)$row[3];
$ExpectedContributionAmount=$row[4];
$DateAndTime=$row[5];
$EthereumRate = $row[6];
$ContributionAmountReceived=$row[7];
$ActualReservedTokens =(float)$row[8];
$ActualBonusTokens = (float)$row[9];
$User = $row[10];
$TransactionStatus =$row[11];
echo "<p>Transaction ID: ".$TransactionID."</p>";
echo "<p>Receiving Address: ".$ReceivingAddress."</p>";
echo "<p>Expected Token Reservation: ".$ExpectedReservedTokens."</p>";
echo "<p>Expected Token Reservation Bonus: ".$ExpectedReservedBonus."</p>";
echo "<p>Expected Contribution Amount: ".$ExpectedContributionAmount."</p>";
echo "<p>Date And Time of Transaction: ".$DateAndTime."</p>";
echo "<p>Ethereum Rate: ".$EthereumRate."</p>";
echo "<p>Contribution Amount Received: ".$ContributionAmountReceived."</p>";
echo "<p>Actual Reserved Tokens: ".$ActualReservedTokens."</p>";
echo "<p>Actual Bonus Tokens: ".$ActualBonusTokens."</p>";
//echo "<p>What we get from result-WalletID:".$WalletId."</p>";
$TotalExpectedTokens = $ExpectedReservedTokens + $ExpectedReservedBonus;
$ActualTotalReservedTokens = $ActualReservedTokens+$ActualBonusTokens;
echo "<p>Total Expected Tokens: ".$TotalExpectedTokens."</p>";
$FromAddress = $ContributionAmountReceived;
$TimeStamp = "_";


if($TransactionStatus != 'Pending')
{
//     $sqlUpdate = "Update EthereumWallets SET TransactionStatus = 'Complete' Where WalletAddress = '".$ReceivingAddress."';";
//     if (mysqli_query($conn, $sqlUpdate)) {
//     //echo "Record updated successfully";
// } else {
//     //echo "Error updating record: " . mysqli_error($conn);
// }
echo' < tr>';
echo' < input class="input-bordered" type = "hidden" id = "TransactionID" name = "TransactionID" value = "'.$TransactionID.'" >
            <td class="tranx-status tranx-status-approved" ><span class="d-none" > Approved</span ><em class="ti ti-check" ></em ></td >
           <td class="tranx-no" ><span > GAINTXETH'.$TransactionID.' </span > '.$DateAndTime.'</td >
           <td class="tranx-token" ><span > +'.$ActualTotalReservedTokens.'</span > GAIN</td >
           <td class="tranx-amount" ><span > '.$ContributionAmountReceived.'</span > ETH </td >
           <td class="tranx-from" ><span > Contribution Received </span > _</td >
           <td class="tranx-action" id = "'.$TransactionID.'" >
          <a href = "#" data - toggle = "modal" data - target = "#myModal" ><em class="ti ti-more-alt trxDetails" ></em > </a >
            </td >
       </tr > ';
}
if($TransactionStatus == 'Pending')
{
    echo ' < tr>
        <td class='tranx-status tranx-status-pending' ><span class='d-none' > Pending</span ><em class='ti ti-alert' ></em ></td >
        <td class='tranx-no' ><span > GAINTXETH'.$TransactionID.' </span > '.$DateAndTime.'</td >
        <td class='tranx-token' ><span > +'.$TotalExpectedTokens.'</span > GAIN</td >
        <td class='tranx-amount' ><span > '.$ExpectedContributionAmount.'</span > ETH</td >
        <td class='tranx-from' ><span > Contribution Pending </span > _</td >
        <td class='tranx-action' id = '' . $TransactionID . '' >
        <a href = '#' data - toggle = 'modal' data - target = '#myModal' ><em class='ti ti-more-alt trxDetails' ></em > </a >

        </td >
    </tr > ';
//<button type="button" class="btn btn-primary trxDetails" >More Info</button>
//    <input class="input-bordered" type="hidden" id="Tx' . $TransactionID . '" name="Tx' . $TransactionID . '">
}
}
}
else
{
        echo '<tr >
            <td class="tranx-status tranx-status-cancled" ><span class="d-none" > There are no transactions for this account </span ><em class="ti ti-close" ></em ></td >
            <td class="tranx-no" ><span > No transaction </span > _</td >
            <td class="tranx-token" ><span > +0</span > GAIN</td >
            <td class="tranx-amount" ><span > +0</span > ETH </td >
            <td class="tranx-from" ><span > No Transaction </span > _</td >

        </tr > ';

}
}
// if($TransactionID)
// <tr>
//        <td class="tranx-status tranx-status-approved"><span class="d-none">Approved</span><em class="ti ti-check"></em></td>
//        <td class="tranx-no"><span>ICIYOW0102</span>08 Jul, 18  10:20PM</td>
//        <td class="tranx-token"><span>+5,600</span>ICOX</td>
//        <td class="tranx-amount"><span>56.00</span>ETH <em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="1 ETH = 100 ICOX"></em></td>
//        <td class="tranx-from"><span>1F1t....4xqX</span>08 Jul, 18  10:20PM</td>
//        <td class="tranx-action">
//            <a href="#" data-toggle="modal" data-target="#tranxApproved"><em class="ti ti-more-alt"></em></a>
//        </td>
//    </tr>*/

?>
