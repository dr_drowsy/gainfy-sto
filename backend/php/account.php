<?php
require_once 'connection.php';

    echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

function getUserInfo($conn) {
    $uid = $_POST['uid'];

    $sqlSelect = "SELECT * FROM Accounts WHERE UniqueId = ?;";
    $stmt = mysqli_stmt_init($conn);
    $return = new stdClass;

    if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
        $return->success = false;
        $return->errorMsg = "Unable to intiialize the query --> " . mysqli_error($conn);
        $json = json_encode($return);
        return $json;
    }
    else {
        mysqli_stmt_bind_param($stmt, "s", $uid);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
    }

    if (!$result) {
        $return->success = true;
        $return->errorMsg = "Database contains no entries for this user";
        $return->data['firstName'] = '';
        $return->data['lastName'] = '';
        $return->data['email'] = '';
        $return->data['twitter'] = '';
        $return->data['telegram'] = '';
        $return->data['dob'] = '';
        $return->data['country'] = '';
    }
    else if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_assoc($result);
        $return->success = true;
        $return->errorMsg = '';
        $return->data['firstName'] = $row['FirstName'];
        $return->data['lastName'] = $row['LastName'];
        $return->data['email'] = $row['EmailAddress'];
        $return->data['twitter'] = $row['TwitterHandle'];
        $return->data['telegram'] = $row['TelegramHandle'];
        $return->data['dob'] = $row['DateOfBirth'];
        $return->data['country'] = $row['Country'];
    }
    elseif (mysqli_num_rows($result) > 1) {
        $return->success = false;
        $return->errorMsg = "Database contains a duplicate entry for this user. Contact a DB admin";
        $return->data = '';
    }
    else {
        $return->success = true;
        $return->errorMsg = "Database contains no entries for this user";
        $return->data['firstName'] = '';
        $return->data['lastName'] = '';
        $return->data['email'] = '';
        $return->data['twitter'] = '';
        $return->data['telegram'] = '';
        $return->data['dob'] = '';
        $return->data['country'] = '';
    }
    mysqli_close($conn);

    $json = json_encode($return);
    return $json;
}

function insertUserInfo($conn) {
    $UserID= filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
    $FirstName = filter_input(INPUT_POST, 'firstName', FILTER_SANITIZE_STRING);
    $LastName =filter_input(INPUT_POST, 'lastName', FILTER_SANITIZE_STRING);
    $Email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $Twitter = filter_input(INPUT_POST, 'twitter', FILTER_SANITIZE_STRING);
    $Telegram = filter_input(INPUT_POST, 'telegram', FILTER_SANITIZE_STRING);
    $DOB =filter_input(INPUT_POST, 'dob', FILTER_SANITIZE_STRING);
    $Country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);

    if ($Twitter == '')
        $Twitter = 'NA';
    if ($Telegram== '')
        $Telegram = 'NA';

    $FirstName = RemoveNonAlphanumeric($FirstName);
    $LastName = RemoveNonAlphanumeric($LastName);
    $Twitter = RemoveSpecialChar($Twitter);
    $Telegram = RemoveSpecialChar($Telegram);

    $sqlInsert = "INSERT INTO Accounts (UniqueID, FirstName, LastName, EmailAddress, TwitterHandle, TelegramHandle, DateOfBirth, Country) VALUES (?, ?, ?, ?, ?, ?, ? , ?) ON DUPLICATE KEY UPDATE UniqueID=VALUES(UniqueID), FirstName=VALUES(FirstName), LastName=VALUES(LastName), EmailAddress=VALUES(EmailAddress), TwitterHandle=VALUES(TwitterHandle), TelegramHandle=VALUES(TelegramHandle), DateOfBirth=VALUES(DateOfBirth), Country=VALUES(Country);";

    $stmt = mysqli_stmt_init($conn);
    $return = new stdClass;
    if (!mysqli_stmt_prepare($stmt, $sqlInsert)) {
        $return->success = false;
        $return->errorMsg = "Unable to prepare SQL statement --> " . mysqli_error($conn);
    }
    else {
        mysqli_stmt_bind_param($stmt, "ssssssss", $UserID, $FirstName, $LastName, $Email, $Twitter, $Telegram, $DOB, $Country);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);

        $return->success = true;
        $return->data['result'] = $results;
    }

    mysqli_close($conn);
//    header("Location: ../account.html");
    $json = json_encode($return);
    return $json;
}

function getUserWallet($conn) {
    $uid = $_POST['uid'];

    $sqlSelect = "SELECT * FROM UserWallets WHERE UniqueId = ?;";
    $stmt = mysqli_stmt_init($conn);
    $return = new stdClass;

    if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
        $return->success = false;
        $return->errorMsg = "Unable to intiialize the query --> " . mysqli_error($conn);
        $json = json_encode($return);
        return $json;
    }
    else {
        mysqli_stmt_bind_param($stmt, "s", $uid);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
    }

    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_assoc($result);
        $return->success = true;
        $return->errorMsg = '';
        $return->data['walletType'] = $row['WalletType'];
        $return->data['walletAddress'] = $row['WalletAddress'];
    }
    else if (mysqli_num_rows($result) < 1) {
        $return->success = false;
        $return->errorMsg = 'No user wallet entry exists';
        $return->data = '';
    }
    else {
        $return->success = false;
        $return->errorMsg = 'Duplicate entries found for this users wallet info. Contact DB admin.';
        $return->data = '';
    }

    mysqli_close($conn);
    $json = json_encode($return);
    return $json;
}

function insertUserWallet($conn) {
    $uid= filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
    $walletType = filter_input(INPUT_POST, 'walletType', FILTER_SANITIZE_STRING);
    $walletAddress = filter_input(INPUT_POST, 'walletAddress', FILTER_SANITIZE_STRING);

    $walletType = RemoveSpecialChar($walletType);
    $walletAddress = RemoveSpecialChar($walletAddress);

    $sqlInsert = "INSERT INTO UserWallets (UniqueID, WalletType, WalletAddress) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE UniqueID=VALUES(UniqueID), WalletType=VALUES(WalletType), WalletAddress=VALUES(WalletAddress);";

    $stmt = mysqli_stmt_init($conn);
    $return = new stdClass;
    if (!mysqli_stmt_prepare($stmt, $sqlInsert)) {
        $return->success = false;
        $return->errorMsg = "Unable to prepare SQL statement --> " . mysqli_error($conn);
    }
    else {
        mysqli_stmt_bind_param($stmt, "sss", $uid, $walletType, $walletAddress);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);

        $return->success = true;
        $return->data['result'] = $results;
    }

    mysqli_close($conn);
    $json = json_encode($return);
    return $json;
}

function RemoveSpecialChar($value) {
    $result  = preg_replace('/[^a-zA-Z0-9_ -@$#!%.?)(]/s','',$value);
    return $result;
}

function RemoveNonAlphanumeric($value) {
    $result  = preg_replace('/[^a-zA-Z0-9_ -)(]/s','',$value);
    return $result;
}
?>
