<?php

    require_once('connection.php');

    echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

    function getUserReferrals($conn) {
        $contributionMultiplier = 25;

        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $results = false;

        //    Get referrals
        $sqlSelect = "SELECT * FROM Referrals WHERE ReferrerID = ?;";
        $stmt = mysqli_stmt_init($conn);
        $return = new stdClass;
        $return->success = false;

        if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
            $return->referralSuccess = false;
            $return->errorMsg = "Unable to initialize the query --> " . mysqli_error($conn) . ".";
        }
        else {
            mysqli_stmt_bind_param($stmt, "s", $uid);
            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);
        }

        if (isset($results)) {
            //        May generate an empty first element
            $referrals = array();
            $contributions = 0;
            $signups = 0;
            while ($row = mysqli_fetch_assoc($results)) {
                //            Will fetch each row as associative array until none are left.
                //            Loop through adding the total number of contributions
                $contributions += $row['ContributionCount'];
                $signups++;
                //            $thisRow = array($row['UniqueID']=>$row);
                //            array_merge($referrals, $thisRow);
                $referrals[$row['UniqueID']] = $row;
            }
            //        Bonus will be the number of contributions times [n] dollars for each contribution
            $bonus = $contributions * $contributionMultiplier;
            $return->success = true;
            $return->referralSuccess = true;
            $return->data['referrals'] = $referrals;
            $return->data['signups'] = $signups;
            $return->data['contributions'] = $contributions;
            $return->data['bonus'] = $bonus;
            $return->errorMsg = '';
        }
        else {
            $return->referralSuccess = false;
            $return->errorMsg = "No Referrals";
        }


        //    Get referredBy
        $sqlSelect = "SELECT ReferredByID FROM Accounts WHERE UniqueID = ?;";
        $stmt = mysqli_stmt_init($conn);
        $result = false;
        $results = false;

        if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
            $return->referredBySuccess = false;
            $return->errorMsg = "Unable to initialize the query --> " . mysqli_error($conn) . ".";
        }
        else {
            mysqli_stmt_bind_param($stmt, "s", $uid);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            $result = mysqli_fetch_assoc($result);
            $queryId = $result['ReferredByID'];

            if (isset($queryId)) {
                $sqlSelect = "SELECT * FROM Accounts WHERE UniqueID = ?;";
                $stmt = mysqli_stmt_init($conn);

                if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
                    $return->referredBySuccess = false;
                    $return->errorMsg = $return->errorMsg . " Unable to prepare Referrer selection statement -->" . mysqli_error($conn) . ".";
                }
                else {
                    $queryId = $result['ReferredByID'];
                    mysqli_stmt_bind_param($stmt, "s", $queryId);
                    mysqli_stmt_execute($stmt);
                    $results = mysqli_stmt_get_result($stmt);
                    $returnArr = mysqli_fetch_assoc($results);

                    if (isset($returnArr)) {
                        $return->referredBySuccess = true;
                        $return->sucess = true;
                        $return->data['referredBy'] = $returnArr;
                    }
                    else {
                        $return->referredBySuccess = false;
                        $return->errorMsg = $return->errorMsg . " No account entries found for referredById";
                    }
                }
            }
            else {
                //        The user was not referred by anyone
                $return->referredBySuccess = false;
                $return->errorMsg = $return->errorMsg . " User was not referred by another person.";
            }
        }


        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }