<?php

    require_once "connection.php";

$UserID = filter_input(INPUT_POST, 'UserID', FILTER_SANITIZE_STRING);

$kycStatusChecker = "Select ApplicationStatus From KycApplications WHERE UniqueID = ? ;";
$stmt = mysqli_stmt_init($conn);
if (!mysqli_stmt_prepare($stmt, $kycStatusChecker)) {
    echo "";
}
else {
    mysqli_stmt_bind_param($stmt, 's', $UserID);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $status);

    mysqli_stmt_fetch($stmt);

    mysqli_stmt_close($stmt);
}


// echo'
// <h2 class="user-panel-title">Identity Verification - KYC</h2>
// <p>To comply with regulation each participant will have to go through indentity verification (KYC). So please complete our fast and secure verification process to participate in our token sale.
// You can proceed from here to verify your indentity and also you can check your application status if you submit already. </p>
// <div class="gaps-2x"></div>';
echo '
<h2 class="user-panel-title">Identity Verification - KYC</h2>';
// <p>The KYC Process will be available in the next couple of days!. </p>
// <p>-Gainfy Team</p>
// <div class="gaps-2x"></div>';

$status = 'wait';

if ($status == 'wait') {
    echo '<div class="status status-process">
        <div class="status-icon">
            <em class="ti ti-files"></em>
            <div class="status-icon-sm">
                <em class="ti ti-alarm-clock"></em>
            </div>
        </div>
        <span class="status-text"></span>
        <p>The KYC Process will be available in the next couple of days!. </p>
        <p>-Gainfy Team</p>
    </div><!-- .status -->';

}

if ($status == '') {
    echo `
    <div class='status status - empty'>
        <div class='status - icon'>
            <em class='ti ti - files'></em>
            <div class='status - icon - sm'>
                <em class='ti ti - close'></em>
            </div>
        </div>
        <span class='status - text'>You have not submitted your KYC Application</span>
        <a href=' ../../kycApplication . html' class='btn btn - primary'>Click to proceed</a>
    </div>
    <div class='note note - md note - info note - plane'>
        <em class='fas fa - info - circle'></em>
        <p>Some of contries and regions will not able to pass KYC process and therefore are restricted from token sale.</p>
    </div>`;
} elseif ($status == 'Pending') {
    echo '<div class="status status-process">
        <div class="status-icon">
            <em class="ti ti-files"></em>
            <div class="status-icon-sm">
                <em class="ti ti-alarm-clock"></em>
            </div>
        </div>
        <span class="status-text">Your Application under Process for Varification.</span>
        <p>We are still working on your identity verification. Once our team verified your indentity, you will be whitelisted and notified by email.</p>
    </div><!-- .status -->';

} elseif ($status == 'Rejected') {
    echo `<div class='status status - canceled'>
        <div class='status - icon'>
            <em class='ti ti - files'></em>
            <div class='status - icon - sm'>
                <em class='ti ti - close'></em>
            </div>
        </div>
        <span class='status - text'>Your application rejected by admin.</span>
        <p>In our verification process, we found information incurrect. It would great if you resubmit the form. If face problem in submission please contact us with support team.</p>
        <a href=' ../../kyc - application . html' class='btn btn - primary'>Resubmit</a>
    </div><!-- .status -->`;
} elseif ($status == 'Missing') {
    echo `<div class='status status - warnning'>
        <div class='status - icon'>
            <em class='ti ti - files'></em>
            <div class='status - icon - sm'>
                <em class='ti ti - alert'></em>
            </div>
        </div>
        <span class='status - text'>We found some information missing in application.</span>
        <p>In our verification process, we found information are missing. It would great if you resubmit the form. If face problem in submission please contact us with support team.</p>
        <a href=' ../../kyc - application . html' class='btn btn - primary'>Submit Again</a>
    </div><!-- .status -->`;
} elseif ($status == 'Verified') {
    echo `
    <div class='status status - verified'>
        <div class='status - icon'>
            <em class='ti ti - files'></em>
            <div class='status - icon - sm'>
                <em class='ti ti - check'></em>
            </div>
        </div>
        <span class='status - text'>Your Identity Verified.</span>
        <p>One fo our team verified your indentity. <br class='d - none d - md - block'>You are now in whitelisted for token sale.</p>
        <div class='gaps - 2x'></div>
        <a href=' ../../kyc - application . html' class='ucap'>Resubmit the application</a>
    </div><!-- .status -->`;

}


mysqli_close($conn);

?>
