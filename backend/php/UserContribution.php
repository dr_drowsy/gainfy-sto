<?php

require_once "connection.php";
require_once "utility/Utility.php";


filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

function getContributions($conn) {
    $return = new stdClass();

    getEthContributions($conn, $return);

    getBtcContributions($conn, $return);

    mysqli_close($conn);
    $json = json_encode($return);
    echo $json;
}

function &getEthContributions($conn, $return) {
    $UserID = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
    $ContributionsReceived = 0;

    $sqlSelect = "SELECT ContributionAmountReceived FROM EthereumTransactions WHERE TransactionStatus = 'Complete' AND UniqueID = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (mysqli_stmt_prepare($stmt, $sqlSelect)) {
        mysqli_stmt_bind_param($stmt, "s", $UserID);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($results)) {
            $ContributionsReceived += (float)$row['ContributionAmountReceived'];
        }

        Utility::AppendToLog($return, 'Contributions received: ' . $ContributionsReceived);
        $return->EthContributions = $ContributionsReceived;
    }
    else {
        Utility::AppendToErrorLog($return, "Unable to prepare select eth contributions. ");
    }

    mysqli_stmt_close($stmt);
    return $return;
}

    function &getBtcContributions($conn, $return) {
        $UserID = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $ContributionsReceived = 0;

        $sqlSelect = "SELECT ContributionAmountReceived FROM BitcoinTransactions WHERE TransactionStatus = 'Complete' AND UniqueID = ?;";
        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlSelect)) {
            mysqli_stmt_bind_param($stmt, "s", $UserID);
            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);

            while ($row = mysqli_fetch_assoc($results)) {
                $ContributionsReceived += (float)$row['ContributionAmountReceived'];
            }

            Utility::AppendToLog($return, 'Contributions received: ' . $ContributionsReceived);
            $return->BtcContributions = $ContributionsReceived;
        }
        else {
            Utility::AppendToErrorLog($return, "Unable to prepare select eth contributions. ");
        }

        mysqli_stmt_close($stmt);
        return $return;
    }
?>
