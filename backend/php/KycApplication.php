<?php

    require_once "connection.php";

    $passportDoc = false;
    $natDocFront = false;
    $natDocBack = false;
    $dlDocFront = false;
    $dlDocBack = false;
    $location = "KycApplications/";

    if (isset($_POST["submit"])) {
        $UserID = filter_input(INPUT_POST, 'unique-id-input', FILTER_SANITIZE_STRING);
        $FirstName = filter_input(INPUT_POST, 'first-name', FILTER_SANITIZE_STRING);
        $LastName = filter_input(INPUT_POST, 'last-name', FILTER_SANITIZE_STRING);
        $DOB = filter_input(INPUT_POST, 'date-of-birth', FILTER_SANITIZE_STRING);
        $Country = filter_input(INPUT_POST, 'Country', FILTER_SANITIZE_STRING);
        $AddressLine1 = filter_input(INPUT_POST, 'address-line-1', FILTER_SANITIZE_STRING);
        $AddressLine2 = filter_input(INPUT_POST, 'address-line-2', FILTER_SANITIZE_STRING);
        $CityOfResidence = filter_input(INPUT_POST, 'city-of-residence', FILTER_SANITIZE_STRING);
        $zipcode = filter_input(INPUT_POST, 'zip-code', FILTER_SANITIZE_STRING);
        $UserAddress = filter_input(INPUT_POST, 'token-address', FILTER_SANITIZE_STRING);
        $PassportNumber = filter_input(INPUT_POST, 'passport-number', FILTER_SANITIZE_STRING);
        $PassportExpiration = filter_input(INPUT_POST, 'passport-expiration', FILTER_SANITIZE_STRING);
        $NationalNumber = filter_input(INPUT_POST, 'national-number', FILTER_SANITIZE_STRING);
        $NationalExpiration = filter_input(INPUT_POST, 'national-expiration', FILTER_SANITIZE_STRING);
        $DriversNumber = filter_input(INPUT_POST, 'dl-number', FILTER_SANITIZE_STRING);
        $DriversExpiration = filter_input(INPUT_POST, 'dl-expiration', FILTER_SANITIZE_STRING);

        // echo '<p>USER ID:'.$UserID.' </p>';
        // echo '<p>$FirstName ID:'.$FirstName.' </p>';
        // echo '<p>$LastName ID:'.$LastName.' </p>';
        // echo '<p>$DOB ID:'.$DOB.' </p>';
        // echo '<p>$Country ID:'.$Country.' </p>';
        // echo '<p>$AddressLine1 ID:'.$AddressLine1.' </p>';
        // echo '<p>$AddressLine2 ID:'.$AddressLine2.' </p>';
        // echo '<p>$CityOfResidence ID:'.$CityOfResidence.' </p>';
        // echo '<p>$zipcode ID:'.$zipcode.' </p>';
        // echo '<p>$UserAddress ID:'.$UserAddress.' </p>';
        // echo '<p>$PassportNumber ID:'.$PassportNumber.' </p>';
        // echo '<p>$PassportExpiration ID:'.$PassportExpiration.' </p>';
        // echo '<p>$NationalNumber ID:'.$NationalNumber.' </p>';
        // echo '<p>$NationalExpiration ID:'.$NationalExpiration.' </p>';
        // echo '<p>$DriversNumber ID:'.$DriversNumber.' </p>';
        // echo '<p>$DriversExpiration ID:'.$DriversExpiration.' </p>';

        if ($AddressLine2 == '') {
            $AddressLine2 = "NA";
        }

        if (isset($_FILES["passportImage"]["tmp_name"])) {
            $checkPassport = getimagesize($_FILES["passportImage"]["tmp_name"]);
            if ($checkPassport !== false) {
                $passportImage = $_FILES['passportImage']['tmp_name'];
                $passportContent = addslashes(file_get_contents($passportImage));
                $passportDoc = true;

                $passportImageReal = $_FILES['passportImage']['name'];
                if (move_uploaded_file($passportImage, $location . $passportImageReal)) {
                    //echo 'File Uploaded';
                }

                $passportContent2 = addslashes(file_get_contents($_FILES['passportImage']['name']));
                // echo '<p>passport FRONT POSTED </p>';
                // echo '<p>passport tmp name'. $passportImage .' </p>';
                // echo '<p>passport real name'. $_FILES['passportImage']['name'].' </p>';
                // echo '<p>passport image type'. $_FILES['passportImage']['type'].' </p>';
                // echo 'filename="' .$_FILES['passportImage']['name'].'" Content-Type:'.$_FILES['passportImage']['type'];
                //echo'<img src=@' . $_FILES['passport']['tmp_name'] . ';filename=' . $_FILES['file']['name'] '>';
            }
        }
        if (isset($_FILES["NationalIDFront"]["tmp_name"])) {
            $checkNatFront = getimagesize($_FILES["NationalIDFront"]["tmp_name"]);
            if ($checkNatFront !== false) {
                $NatFrontImage = $_FILES['NationalIDFront']['tmp_name'];
                $NatFrontContent = addslashes(file_get_contents($NatFrontImage));
                $natDocFront = true;
                echo '<p>National ID FRONT POSTED </p>';

            }
        }
        if (isset($_FILES["NationalIDBack"]["tmp_name"])) {
            $checkNatBack = getimagesize($_FILES["NationalIDBack"]["tmp_name"]);
            if ($checkNatBack !== false) {
                $NatBackImage = $_FILES['NationalIDBack']['tmp_name'];
                $NatBackContent = addslashes(file_get_contents($NatBackImage));
                $natDocBack = true;
                echo '<p>National ID BACK POSTED </p>';

            }
        }
        if (isset($_FILES["DriversLicenseFront"]["tmp_name"])) {
            $checkDlFront = getimagesize($_FILES["DriversLicenseFront"]["tmp_name"]);
            if ($checkDlFront !== false) {
                $DlFrontImage = $_FILES['DriversLicenseFront']['tmp_name'];
                $DlFrontContent = addslashes(file_get_contents($DlFrontImage));
                $dlDocFront = true;
                echo '<p>DL FRONT POSTED </p>';

            }
        }
        if (isset($_FILES["DriversLicenseBack"]["tmp_name"])) {
            $checkDlBack = getimagesize($_FILES["DriversLicenseBack"]["tmp_name"]);
            if ($checkDlBack !== false) {
                $DlBackImage = $_FILES['DriversLicenseBack']['tmp_name'];
                $DlBackContent = addslashes(file_get_contents($DlBackImage));
                $dlDocBack = true;
                echo '<p>DL BACK POSTED </p>';

            }
        }

        if ($passportDoc == true) {
            $sqlInsertpart1 = "INSERT INTO KycApplications ";
            $sqlInsertpart2 = "(`UniqueID`, `FirstName`,`LastName`,`DateOfBirth`,
                    `Country`, `AddressLine1`, `AddressLine2`, `City`, `ZipCode`, `Document1`, `IdNumber`,`IdExpiration`,`DeclaredWallet`, `ApplicationStatus` )
                    Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'Pending');";
            $sqlInsertFull = $sqlInsertpart1 . $sqlInsertpart2;

            $stmt = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($stmt, $sqlInsertFull)) {
                echo "sql statement failed";
            }
            else {
                mysqli_stmt_bind_param($stmt, "sssssssssbsss", $UserID, $FirstName, $LastName, $DOB,
                    $Country, $AddressLine1, $AddressLine2, $CityOfResidence, $zipcode, $passportContent, $PassportNumber, $PassportExpiration, $UserAddress);

                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }
        }


        if ($natDocFront == true && $natDocBack == true) {
            $sqlInsertpart1 = "INSERT INTO KycApplications ";
            $sqlInsertpart2 = "(`UniqueID`, `FirstName`,`LastName`,`DateOfBirth`,
                                `Country`, `AddressLine1`, `AddressLine2`, `City`, `ZipCode`, `Document1`,
                                `Document2`,`IdNumber`,`IdExpiration`,`DeclaredWallet`,`ApplicationStatus`) Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,'Pending');";
            $sqlInsertFull = $sqlInsertpart1 . $sqlInsertpart2;

            $stmt = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($stmt, $sqlInsertFull)) {
                echo "sql statement failed";
            }
            else {
                mysqli_stmt_bind_param($stmt, "sssssssssbbsss", $UserID, $FirstName, $LastName, $DOB,
                    $Country, $AddressLine1, $AddressLine2, $CityOfResidence, $zipcode, $NatFrontContent, $NatBackContent, $NationalNumber, $NationalExpiration, $UserAddress);

                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }

        }

        if ($dlDocFront == true && $dlDocBack == true) {
            $sqlInsertpart1 = "INSERT INTO KycApplications ";
            $sqlInsertpart2 = "(`UniqueID`, `FirstName`,`LastName`,`DateOfBirth`,
                                        `Country`, `AddressLine1`, `AddressLine2`, `City`, `ZipCode`, `Document1`,
                                        `Document2`,`IdNumber`,`IdExpiration`, `DeclaredWallet`, `ApplicationStatus`) Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'Pending');";
            $sqlInsertFull = $sqlInsertpart1 . $sqlInsertpart2;

            $stmt = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($stmt, $sqlInsertFull)) {
                echo "sql statement failed";
            }
            else {
                mysqli_stmt_bind_param($stmt, "sssssssssbbsss", $UserID, $FirstName, $LastName, $DOB,
                    $Country, $AddressLine1, $AddressLine2, $CityOfResidence, $zipcode, $DlFrontContent, $DlBackContent, $DriversNumber, $DriversExpiration, $UserAddress);

                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }
        }

        $publicKey = "1ELwJWjXLMjyUNwb1G6gAGaADijhzUY6yW";
        $privateKey = "L2g51Hd54oycg1yBfuFZrRvHik21FvC3N6ZgJtCdjuZ3ApcqmYhV";
        //.$publicKey."/".$privateKey."/"."Passport/".$FirstName ."/".$LastName."/".$CityOfResidence."/".$DOB."/".$PassportExpiration."/".$PassportNumber."/".$passportImage;
        //
        // $FullURL= "https//arcadiakyc.com/api/submitOrder?public_key=".$publicKey."&private_key=".$privateKey.
        // "&order_type=National&original_id=11111111&name=".$FirstName."&surname=".$LastName."&placebirth=".$CityOfResidence.
        // "&birthday=26 DEC 1985&expire=".$PassportExpiration."&serial=".$PassportNumber."&document_photo="$location.$PassportImageReal;


        //    $baseURL ="https://arcadiakyc.com/api/submitOrder";
        // echo'    <img src ='.$location.$passportImageReal.' width="200" height="200" /> ';
        // $ch = curl_init();
        // $args = new CURLFile($_FILES['passportImage']['tmp_name'],$_FILES['passportImage']['type'],$_FILES['passportImage']['name']);
        // $data =  array(
        //     "public_key" => $publicKey,
        //     'private_key' => $privateKey,
        //     'order_type' => "National",
        //     'country_id' => '840',
        //     'origin_id' => '1',
        //     'name' => $FirstName,
        //     'surname' => $LastName,
        //     'placebirth' => $CityOfResidence,
        //     'birthday' => '26 DEC 1985',
        //     'expire' => $PassportExpiration,
        //     'serial' => $PassportNumber,
        //     'document_photo' => $args
        //     //new CURLFile($FILES['passportImage']['tmp_name'], $FILES['passportImage']['type'],$FILES['passportImage']['name'])
        // );
        //
        //file_get_contents($location.$passportImageReal, FILE_USE_INCLUDE_PATH));
        // '@'.(realpath($location.$passportImageReal)));
        //file_get_contents($location.$passportImageReal)
        //$request_headers = array('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS','Access-Control-Allow-Origin:*',
        //'Cache-Control:no-cache,private','Connection:keep-alive', 'Content-Type:multipart/form-data');
        //    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        // curl_setopt($ch, CURLOPT_URL, $baseURL);
        // //curl_setopt($curl, CURLOPT_ENCODING, "gzip");
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_INFILESIZE, filesize($args));
        //                        curl_setopt_array($curl, array(
        //                          CURLOPT_URL => "https://arcadiakyc.com/api/submitOrder",
        //                          CURLOPT_RETURNTRANSFER => true,
        //                          CURLOPT_ENCODING => "",
        //                          CURLOPT_MAXREDIRS => 10,
        //                          CURLOPT_TIMEOUT => 60,
        //                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //                          CURLOPT_CUSTOMREQUEST => "POST",
        //                          CURLOPT_POSTFIELDS =>  $data,
        //                          //   'document_photo' =>$args);
        //                          // "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"public_key\"\r\n\r\n11\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"private_key\"\r\n\r\n11\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"order_type\"\r\n\r\nNational\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"country_id\"\r\n\r\n840\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"origin_id\"\r\n\r\n11111111\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"name\"\r\n\r\nKathleen \r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"surname\"\r\n\r\nOnufer\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"placebirth\"\r\n\r\nMissouri\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name
        //                          //birthday\"\r\n\r\n26 DEC 1985\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"expire\"\r\n\r\n07 Apr 2013\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"serial\"\r\n\r\n160404609\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"document_photo\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
        //                          CURLOPT_HTTPHEADER => array(
        //                              "Cache-Control: no-cache",
        // "Content-Type: multipart/form-data",
        //  "Postman-Token: 806c85af-9d71-4478-bf5d-c1ce496d5629",
        //  "content-type: multipart/form-data"
        //                          ),
        //                        ));

        //                             $response = curl_exec($ch);
        //                             $err = curl_error($ch);
        //
        //                             curl_close($ch);
        //
        //                             if ($err) {
        //                               echo "cURL Error #:" . $err;
        //                             } else {
        //                               echo $response;
        //                             }
        //
        //                             if($response ===false)
        //                             {
        //                                 $info = curl_getinfo($ch);
        //                                             curl_close($ch);
        //                                             die('error occured during curl exec. Additioanl info: ' . var_export($info));
        //                             }
        //                             $kycJson = json_decode($response, true);
        //                             //$ethPrice = $ethJson->"amount";
        //                             curl_close($ch);
        //                             $kycOne = $kycJson[1];
        //                             $kycSuccess = $kycJson["success"];
        //                             $kycMessage = $kycJson["message"];
        //                             $kycSuccess = $kycJson["result"];
        //
        //                             echo '<p>$kycJson ID:'.print_r($kycJson).' </p>';
        //                             echo '<p>$kycOne ID:'.$kycOne.' </p>';
        //
        //                             echo '<p>$kycSuccess ID:'.$kycSuccess.' </p>';
        //                             echo '<p>$kycMessage ID:'.$kycMessage.' </p>';
        //                             echo '<p>$kycSuccess ID:'.$kycSuccess.' </p>';
        //
        // }
        // else
        // {
        //     echo ' <p>NO SUbmit </p>';
        // }

        // $curl =  curl_init($baseURL);
        // $args = new CURLFile($_FILES['passportImage']['name'],$_FILES['passportImage']['type']);
        //
        // echo '<p>file path ID:'.$location.$passportImageReal.' </p>';
        //
        // if (function_exists('curl_file_create')) { // php 5.5+
        //         $cFile = curl_file_create($passportImageReal,$_FILES['passportImage']['type']);
        //         } else { //
        //             $cFile = '@' . realpath($passportImageReal,$_FILES['passportImage']['type']);
        //         }
        //         echo '<p>CFILE!!! ID:'.print_r($cFile).' </p>';
        //
        //         //$cfile = curl_file_create($location.$passportImageReal);
        // //echo '<p>$cfile ID:'.$cfile.' </p>';
        // echo '<p>$args ID:'.print_r(file_get_contents($args)).' </p>';
        // echo'    <img src ='.$location.$passportImageReal.' /> ';
        // //echo '<p>$contents of passportImage real ID:'.print_r(addslashes(file_get_contents($location.$passportImageReal))).' </p>';
        //
        // $curl_post_data = array(
        //     'public_key' => $publicKey,
        //     'private_key' => $privateKey,
        //     'order_type' => 'National',
        //     'country_id' => '840',
        //     'origin_id' => '1',
        //     'name' => $FirstName,
        //     'surname' => $LastName,
        //     'placebirth' => $CityOfResidence,
        //     'birthday' => '26 DEC 1985',
        //     'expire' => $PassportExpiration,
        //     'serial' => $PassportNumber,
        //     'document_photo' => $args);
        // //    'document_photo' =>$args);
        //     // array(
        //     //     'filename' => file_get_contents($_FILES['passportImage']['name']),
        //     //     'Content-Type' =>$_FILES['passportImage']['type'])
        //
        //     //'@/'.addslashes(file_get_contents($location.$passportImageReal)));
        //     // file_get_contents($_FILES['passportImage']['name']));
        //     //'filename="' .$_FILES['passportImage']['name'].'" Content-Type:'.$_FILES['passportImage']['type']);
        //     // '@tmp/filename=" ' .$_FILES['passportImage']['name']. " ' Contrent-Type:".$_FILES['passportImage']['type']);
        //         $request_headers = array('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS','Access-Control-Allow-Origin: *'
        //         ,'Cache-Control: no-cache, private','Connection : keep-alive', 'Content-Type: text/html; charset=UTF-8');
        //     // '@' . $_FILES['passport']['tmp_name']
        //                 //           . ';filename=' . $_FILES['file']['name']);
        //                     curl_setopt($curl, CURLOPT_POST, true);
        //                     curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
        //                     //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //
        //                     //curl_setopt($curl, CURLOPT_UPLOAD, TRUE);
        //                     //curl_setopt($s,CURLOPT_HTTPHEADER,array('Expect:'));
        //                     curl_setopt($curl, CURLOPT_HTTPHEADER,array(
        //                                                                'Content-Type: multipart/form-data'));
        //                     curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
        //
        //                     //curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
        //                     curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        //
        //
        //
        // $kycResult= curl_exec($curl);
    }
    mysqli_close($conn);
    header("Location: ../kyc.html");
?>
