<?php

    require_once('connection.php');

    echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

    function isReferrerValid($conn) {
        $referredBy = filter_input(INPUT_POST, 'referredBy', FILTER_SANITIZE_STRING);

        //    Make sure the referredById is a valid account first
        $sqlSelect = "SELECT UniqueID FROM Accounts WHERE UniqueID = ?;";
        $stmt = mysqli_stmt_init($conn);
        $return = new stdClass;
        $result = false;

        if (!mysqli_stmt_prepare($stmt, $sqlSelect)) {
            $return->success = false;
            $return->errorMsg = "Unable to initialize the query --> " . mysqli_error($conn);
        }
        else {
            mysqli_stmt_bind_param($stmt, "s", $referredBy);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
        }

        if ($result != null && $result != '' && !$result) {
            $return->success = false;
            $return->errorMsg = "The referral code entered is not valid. Please try the link again.";
        }
        else {
            $return->success = true;
            $return->errorMsg = '';
        }
        mysqli_close($conn);

        $json = json_encode($return);
        return $json;
    }

    function registerNewUser($conn) {
        //    Inserts user info upon registering
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $referredBy = filter_input(INPUT_POST, 'referredBy', FILTER_SANITIZE_STRING);
        $firstName = filter_input(INPUT_POST, 'firstName', FILTER_SANITIZE_STRING);
        $lastName = filter_input(INPUT_POST, 'lastName', FILTER_SANITIZE_STRING);

        $firstName = RemoveNonAlphanumeric($firstName);
        $lastName = RemoveNonAlphanumeric($lastName);

        if ($firstName == '') {
            $firstName = null;
        }
        if ($lastName == '') {
            $lastName = null;
        }
        if ($referredBy == '') {
            $referredBy = null;
        }


        $sqlInsert = "INSERT INTO Accounts (UniqueID, FirstName, LastName, EmailAddress, Timestamp, ReferredByID) VALUES (?, ?, ?, ?, ?, ?);";
        $stmt = mysqli_stmt_init($conn);
        $return = new stdClass;
        $return->success = false;


        if (!mysqli_stmt_prepare($stmt, $sqlInsert)) {
            $return->accountSuccess = false;
            $return->accountErrorMsg = "Unable to prepare Account SQL statement --> " . mysqli_error($conn);
            $return->errorMsg = $return->errorMsg . " Unable to prepare Referral SQL statement --> " . mysqli_error($conn) . ".";
        }
        else {
            $timestamp = date('Y-m-d G:i:s');
            mysqli_stmt_bind_param($stmt, "ssssss", $uid, $firstName, $lastName, $email, $timestamp, $referredBy);
            mysqli_stmt_execute($stmt);
            $accountResults = mysqli_stmt_get_result($stmt);
            if (!$accountResults) {
                $return->accountErrorMsg = mysqli_error($conn);
            }
            $return->data['accountResults'] = $accountResults;
            $return->success = true;
            $return->accountSuccess = true;
        }

        //   User registered using a referral link
        if ($referredBy != null) {
            $sqlInsert = "INSERT INTO Referrals (UniqueID, ReferrerID, Timestamp, ContributionCount, ContributionValue) VALUES (?, ?, ?, ?, ?);";

            $stmt = mysqli_stmt_init($conn);

            if (!mysqli_stmt_prepare($stmt, $sqlInsert)) {
                $return->referralSuccess = false;
                $return->referralErrorMsg = "Unable to prepare Referral SQL statement --> " . mysqli_error($conn);
                $return->errorMsg = $return->errorMsg . " Unable to prepare Referral SQL statement --> " . mysqli_error($conn) . ".";
            }
            else {
                $timestamp = date('Y-m-d G:i:s');
                $contributionCount = 0;
                $contributionValue = 0.0;
                mysqli_stmt_bind_param($stmt, "sssid", $uid, $referredBy, $timestamp, $contributionCount, $contributionValue);
                mysqli_stmt_execute($stmt);
                $referralResults = mysqli_stmt_get_result($stmt);
                if (!$referralResults) {
                    $return->referralErrorMsg = mysqli_error($conn);
                }
                $return->referralSuccess = true;
                $return->data['referralResults'] = $referralResults;
                $return->success = true;
            }
        }

        mysqli_close($conn);

        $json = json_encode($return);
        return $json;

    }

    function RemoveSpecialChar($value) {
        $result = preg_replace('/[^a-zA-Z0-9_ -@$#!%.?)(]/s', '', $value);
        return $result;
    }

    function RemoveNonAlphanumeric($value) {
        $result = preg_replace('/[^a-zA-Z0-9_ -)(]/s', '', $value);
        return $result;
    }

?>