<?php
/**
 * Created by PhpStorm.
 * User: Dalton
 * Date: 9/20/18
 * Time: 3:34 PM
 */

require_once('rfc6238.php');
require_once('base32static.php');

echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)();


/**
 * Generate QR code from secret clue.
 *
 * Generates and encodes (base32) a random secret clue, then uses that code to generate a QR code to link Authentication App to secret clue.
 *
 * @return string
 * @author Dalton Pierce
 */
function getQrCode() {
//    Fetch and decode user input
    $domain = filter_input(INPUT_POST, 'domain', FILTER_SANITIZE_STRING);
    $issuer = filter_input(INPUT_POST, 'issuer', FILTER_SANITIZE_STRING);
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $cacheClue = false;

    if ($cacheClue === true) {
        //    Start SESSION
        session_start();
//    If Secret Clue has been previously generated, fetch from SESSION
        if (!empty($_SESSION['encClue']) && $_SESSION['encClue'] !== "reset" && !empty($_SESSION['lastUser']) && $_SESSION['lastUser'] === $username) {
            $encClue = (string)$_SESSION['encClue'];
        }
//    Otherwise, generate a new Secret Clue and encode, then write to SESSION
        else {
            $randClue = (string)TokenAuth6238::generateRandomClue(16);
            $rawClue = (string)Base32Static::encode($randClue);
            $encClue = (string)trim($rawClue, "=");
            $_SESSION['encClue'] = $encClue;
            $_SESSION['lastUser'] = $username;
        }
//    Close SESSION
        session_write_close();
    }
    else {
        $randClue = (string)TokenAuth6238::generateRandomClue(16);
        $rawClue = (string)Base32Static::encode($randClue);
        $encClue = (string)trim($rawClue, "=");
        $_SESSION['encClue'] = $encClue;
        $_SESSION['lastUser'] = $username;
    }

//    Generate QR Code
    $barcode = (string)TokenAuth6238::getBarCodeUrl($username, $domain, $encClue, $issuer);

//    Prepare response
    $return = new stdClass;
    if (isset($barcode)) {
        $return->success = true;
        $return->errorMsg = '';
        $return->data['barcode'] = $barcode;
        $return->data['clue'] = $encClue;
    }
    else {
        $return->success = false;
        $return->errorMsg = "Unable to generate barcode URL with given parameters...";
        $return->data = '';
    }

//    Encode response, and return
    $json = json_encode($return);
    return $json;
}

/**
 * Validate user input against secret clue.
 *
 * Compares key entered by user against the secret clue generated for them. Accepts parameters through POST request in JSON format. Sends response in JSON format.
 *
 * @return boolean|string
 * @author Dalton Pierce
 */
function validateKey() {
//    Fetch and decode user input
    $userInput = filter_input(INPUT_POST, 'key', FILTER_SANITIZE_STRING);
    $encClue = filter_input(INPUT_POST, 'clue', FILTER_SANITIZE_STRING);

//    Validate the user input
    $valid = TokenAuth6238::verify($encClue, $userInput);

//    Prepare response
    $return = new stdClass;
    if (is_bool($valid)) {
        $return->success = true;
        $return->errorMsg = '';
        $return->data['valid'] = $valid;
    }
    else {
        $return->success = false;
        $return->errorMsg = 'Unable to validate given key code against secret clue...';
        $return->data['valid'] = false;
    }

//    Encode response, and return
    $json = json_encode($return);
    return $json;
}

function getScratchCodes() {
    $return = new stdClass();

    $scratchCodes = array();
    for ($i = 0; $i < 6; $i++) {
        $scratchCode = strtoupper(substr(sha1(mt_rand()),17,6));
        array_push($scratchCodes, $scratchCode);
    }

    $return->scratchCodes = $scratchCodes;

    $json = json_encode($return);
    return $json;
}