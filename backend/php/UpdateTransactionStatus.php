#!/usr/bin/php

<?php

    require_once(__DIR__ . '/connection.php');
    require_once(__DIR__ . '/utility/Utility.php');
    require_once (__DIR__ . '/tokenSaleSupply.php');

    if (!empty($argv[1])) {
        switch ($argv[1]) {
            case "scanAllTranxChanges":
                echo "\nRan 'scanAllTranxChanges' at ". date("Y-m-d H:i:s") ."\n";
                echo scanAllTranxChanges($conn);
                break;
            case "scanCryptoTranxChanges":
                if (!empty($argv[2])) {
                    echo "\nRan 'scanCryptoTranxChanges' at ". date("Y-m-d H:i:s") ."\n";
                    echo scanCryptoTranxChanges($conn, $argv[2]);
                }
                else {
                    echo "\nThis function requires a crypto type to  be specified. Either ETH or BTC.\n";
                }
                break;
            case "sendTestEmail":
                echo "\nRan 'sendTestEmail' at ". date("Y-m-d H:i:s") ."\n";
                echo sendTestEmail();
                break;
            default:
                echo $argv[1]($conn, ...$argv);
                break;
        }
    }

    function sendTestEmail() {
        $gainfyImageLink = Utility::GetEmailLogoLink();
        $backgroundLink = Utility::GetEmailBgLink();
        $headers = Utility::GetEmailHeaders();
        $subject = "Gainfy STO Contribution Expired";

        $date = (new DateTime('now'));
        $timestamp = $date->format('Y-m-d H:i:s');
        $email = 'dnnp2011@gmail.com';
        $walletAddress = 'test address';
        $expectedTokens = '124.4574';
        $expectedContribution = '0.032525';
        $tranxId = 'GAINXETH290';
        $ttl = 2;
        $crypto = 'ETH';

        $emailBody = prepareTranxExpireEmail($gainfyImageLink, $backgroundLink, $timestamp, $walletAddress, $tranxId, $expectedTokens, $expectedContribution, $ttl, $crypto);

        mail($email, $subject, $emailBody, $headers);

        return 'Email sent to ' . $email . ' regarding ' . $subject . ". \r\n";
    }

    function scanAllTranxChanges($conn) {
        $return = new stdClass();

        scanBtcTranxChanges($conn, $return);
        scanEthTranxChanges($conn, $return);
        updateTokenSaleSupply($conn); // Calls update from tokenSaleSupply.php

        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }

    function scanCryptoTranxChanges($conn, $crypto) {
        $return = new stdClass();

        switch (strtoupper($crypto)) {
            case 'BTC':
                scanBtcTranxChanges($conn, $return);
                break;
            case 'ETH':
                scanEthTranxChanges($conn, $return);
                break;
            default:
                Utility::AppendToErrorLog($return, '\n\rIncorrect crypto type format in scanCryptoTranxChanges. Requires ETH or BTC.\n\r');
                break;
        }

        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }

    function &scanEthTranxChanges($conn, $return) {
        //       $allTranxApiUrl = "http://api.etherscan.io/api?module=account&action=txlist&address=<ADDRESS>&startblock=0&endblock=99999999999&sort=asc&apikey=YourApiKeyToken";
        //0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&tag=latest&apikey=YourApiKeyToken
        // Single Address
        //        $apiUrl = "https://api.etherscan.io/api?module=account&action=balance&address=";

        // Multi-Address
        //        $multiscanUrl = "https://api.etherscan.io/api?module=account&action=balancemulti&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a,0x63a9975ba31b0b9626b34300f7f627147df1f526,0x198ef1ec325a96cc354c7266a038be8b5c558f67&tag=latest&apikey=YourApiKeyToken";
        $etherscanApiKey = "TBJVU6FBAWW3CHYI7NKT4BBDEHHSAEA9KF";
        $etherscanApiKeyFooter = "&tag=latest&apikey=" . $etherscanApiKey;
        $apiUrl = "https://api.etherscan.io/api?module=account&action=balancemulti&address=";

        $ethOmegaUsd = Utility::GetOmegaUsd(); //5 cents
        $currentEthExchangeRate = Utility::GetEthExchangeRate(); // Difference of greater than 5 cents to avoid floating point math errors and inaccuracy
        $ethOmega = $currentEthExchangeRate ? $ethOmegaUsd / $currentEthExchangeRate : 0.00025; // Difference of greater than 5 cents to avoid floating point math errors and inaccuracy

        UpdateWalletTranxStatus($conn, $return, 'ETH');

        $sqlJoinSelect = "
            SELECT Accounts.EmailAddress, Accounts.ReferredByID, EthereumWallets.LastBalance, EthereumWallets.WalletAddress, EthereumWallets.WalletID, 
            EthereumTransactions.ExpectedReservedTokens, EthereumTransactions.ExpectedReservedBonus, EthereumTransactions.ExpectedContributionAmount, 
            EthereumTransactions.EthereumRate, EthereumTransactions.BonusRate, EthereumTransactions.TransactionID, Accounts.UniqueID
            FROM EthereumTransactions
            INNER JOIN EthereumWallets ON  EthereumWallets.WalletAddress = EthereumTransactions.ReceivingAddress
            INNER JOIN Accounts ON Accounts.UniqueID = EthereumTransactions.UniqueID
            WHERE EthereumTransactions.TransactionStatus = 'Pending'
            ORDER BY EthereumTransactions.DateAndTime ASC;";

        $stmt = mysqli_stmt_init($conn);

        if (mysqli_stmt_prepare($stmt, $sqlJoinSelect)) {

            mysqli_stmt_execute($stmt);
            //Method 1: Get result, loop through rows
            $results = mysqli_stmt_get_result($stmt);
            if (empty($results)) {
                Utility::AppendToLog($return, 'No results for sqlJoinSelect. ');
                mysqli_close($conn);
                $json = json_encode($return);
                return $json;
            }
            mysqli_stmt_close($stmt);

            $addressData = array();
            $allAddresses = array();

            //            Build and fetch ethereum wallet balances from api
            if ($allResults = mysqli_fetch_all($results)) {

                foreach ($allResults as $result) {
                    $email = $result[0];
                    $referredBy = $result[1];
                    $lastBalance = $result[2];
                    $walletAddress = $result[3];
                    $walletId = $result[4];
                    $expectedTokens = $result[5];
                    $expectedBonus = $result[6];
                    $expectedContribution = $result[7];
                    $ethRate = $result[8];
                    $bonusRate = $result[9];
                    $tranxId = $result[10];
                    $uid = $result[11];

                    $tempArr = array('EmailAddress' => $email, 'ReferredByID' => $referredBy, 'LastBalance' => $lastBalance, 'WalletAddress' => $walletAddress, 'WalletID' => $walletId, 'ExpectedReservedTokens' => $expectedTokens,
                        'ExpectedReservedBonus' => $expectedBonus, 'ExpectedContributionAmount' => $expectedContribution, 'EthereumRate' => $ethRate, 'BonusRate' => $bonusRate, 'TransactionID' => $tranxId, 'UniqueID' => $uid);
                    $allAddresses[$walletAddress] = $tempArr;
                }


                $addressCount = count($allAddresses); // 4
                $charLimit = Utility::GetCharLimit(); //1900
                $addressLength = (strlen($allAddresses[array_keys($allAddresses)[0]]['WalletAddress']));
                $loops = (int)ceil(($addressCount * $addressLength) / $charLimit); // The number of loops to batch query all addresses under the character limit 1
                $maxAddressesInQuery = (int)floor($charLimit / $addressLength); // The max number of addresses in one query 490
                $count = 0; // 0

                while ($count < $loops) {
                    if ($loops - $count === 1) {
                        $remainder = ($addressCount - ($count * $maxAddressesInQuery)); // 20
                        $urlQuery = $apiUrl . $allAddresses[array_keys($allAddresses)[$count * $maxAddressesInQuery]]['WalletAddress']; // 80
                        for ($i = 1; $i < $remainder; $i++) {
                            $urlQuery .= ',' . $allAddresses[array_keys($allAddresses)[($count * $maxAddressesInQuery) + $i]]['WalletAddress'];
                        }

                        $urlQuery .= $etherscanApiKeyFooter;
                        $jsonResult = Utility::ExecuteBalanceCurl($urlQuery, true);
                        $resultArray = $jsonResult['result'];
                        $resultAssoc = array();
                        foreach ($resultArray as $item) {
                            $resultAssoc[$item['account']] = $item['balance'];
                        }
                        array_push($addressData, $resultAssoc);
                    }
                    else {
                        $urlQuery = $apiUrl . $allAddresses[array_keys($allAddresses)[$count * $maxAddressesInQuery]]['WalletAddress'];
                        for ($i = 1; $i < $maxAddressesInQuery; $i++) {
                            $urlQuery = $urlQuery . '|' . $allAddresses[array_keys($allAddresses)[($count * $maxAddressesInQuery) + $i]]['WalletAddress'];
                        }

                        $urlQuery .= $etherscanApiKeyFooter;
                        $jsonResult = Utility::ExecuteBalanceCurl($urlQuery);
                        $resultArray = $jsonResult['result'];
                        $resultAssoc = array();
                        foreach ($resultArray as $item) {
                            $resultAssoc[$item['account']] = $item['balance'];
                        }
                        array_push($addressData, $resultAssoc);
                    }

                    $count++;
                }
            }
            else {
                Utility::AppendToLog($return, 'No results from tranx search. ');
            }


            if (empty($addressData)) {
                Utility::AppendToErrorLog($return, 'The required value "addressData" from scanEthTranxChanges has not been correctly set, but is required for the next sections. ');
                return $return;
            }

            //            Check if wallet balances have changes, if so, update them
            mysqli_data_seek($results, 0);
            foreach ($allAddresses as $address) {
                $email = $address['EmailAddress'];
                $referredBy = $address['ReferredByID'];
                $lastBalance = $address['LastBalance'];
                $walletAddress = $address['WalletAddress'];
                $walletId = $address['WalletID'];
                $bonusRate = $address['BonusRate'];
                $tranxId = $address['TransactionID'];
                $uid = $address['UniqueID'];


                //                    If the wallet has more than a 5 cent difference since it was last checked
                $currentBalance = Utility::WeiToEth($addressData[0][$walletAddress]);

                //Debug
//                if ($walletAddress == '0xf98e7FDdB47cbf45a96A3A794f412E6958Cd3EEC') {
//                    $currentBalance = 2.8;
//                }

                $ethWalletDelta = $currentBalance - $lastBalance; // This is the size of the contribution (how much wallet has changed since last check);
                Utility::AppendToLog($return, 'Current ETH Balance: ' . $currentBalance . ' Delta: ' . $ethWalletDelta);

                if ($ethWalletDelta > $ethOmega) {
                    Utility::AppendToLog($return, 'ETH Wallet Balance changed!');

                    $actualTokens = Utility::EthToGain($currentEthExchangeRate, $ethWalletDelta);
                    $actualBonus = $actualTokens * $bonusRate;

                    HandleConfirmTransaction($conn, 'ETH', $tranxId, $currentBalance, $currentEthExchangeRate, $walletId, $actualTokens, $actualBonus, $ethWalletDelta, $referredBy, $uid, $email, $return);
                }
                elseif ($ethWalletDelta < 0) {
                    Utility::AppendToErrorLog($return, 'ERROR! ETH wallet balance has dropped!');
                }
                else {
                    Utility::AppendToLog($return, 'Wallet balance NOT changed');
                }
            }

            mysqli_free_result($results);
        }
        else {
            mysqli_stmt_close($stmt);
            $return->errorLog = $return->errorLog ?? '' . 'Unable to prepare joinSelect stmt ' . mysqli_error($conn);
        }

        //        Partition: Cancel out-dated transactions
        killOldTranx($conn, $return, 'ETH');

        return $return;
    }

    function &scanBtcTranxChanges($conn, $return) {
        $apiUrl = "https://blockchain.info/balance?active=";
        $btcOmegaUsd = Utility::GetOmegaUsd();; //5 cents
        $currentBtcExchangeRate = Utility::GetBtcExchangeRate();
        $btcOmega = $currentBtcExchangeRate ? $btcOmegaUsd / $currentBtcExchangeRate : 0.000008; // Difference of greater than 5 cents to avoid floating point math errors and inaccuracy

        UpdateWalletTranxStatus($conn, $return, 'BTC');


        $sqlJoinSelect = "
            SELECT Accounts.EmailAddress, Accounts.ReferredByID, BitcoinWallets.LastBalance, BitcoinWallets.WalletAddress, BitcoinWallets.WalletID, 
            BitcoinTransactions.ExpectedReservedTokens, BitcoinTransactions.ExpectedReservedBonus, BitcoinTransactions.ExpectedContributionAmount, 
            BitcoinTransactions.BitcoinRate, BitcoinTransactions.BonusRate, BitcoinTransactions.TransactionID, Accounts.UniqueID
            FROM BitcoinTransactions
            INNER JOIN BitcoinWallets ON  BitcoinWallets.WalletAddress = BitcoinTransactions.ReceivingAddress
            INNER JOIN Accounts ON Accounts.UniqueID = BitcoinTransactions.UniqueID
            WHERE BitcoinTransactions.TransactionStatus = 'Pending'
            ORDER BY BitcoinTransactions.DateAndTime ASC;";

        $stmt = mysqli_stmt_init($conn);

        if (mysqli_stmt_prepare($stmt, $sqlJoinSelect)) {

            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);
            mysqli_stmt_close($stmt);

            $addressData = array();
            $allAddresses = array();

            if ($allResults = mysqli_fetch_all($results)) {

                foreach ($allResults as $result) {
                    $email = $result[0];
                    $referredBy = $result[1];
                    $lastBalance = $result[2];
                    $walletAddress = $result[3];
                    $walletId = $result[4];
                    $expectedTokens = $result[5];
                    $expectedBonus = $result[6];
                    $expectedContribution = $result[7];
                    $btcRate = $result[8];
                    $bonusRate = $result[9];
                    $tranxId = $result[10];
                    $uid = $result[11];

                    $tempArr = array('EmailAddress' => $email, 'ReferredByID' => $referredBy, 'LastBalance' => $lastBalance, 'WalletAddress' => $walletAddress, 'WalletID' => $walletId, 'ExpectedReservedTokens' => $expectedTokens,
                        'ExpectedReservedBonus' => $expectedBonus, 'ExpectedContributionAmount' => $expectedContribution, 'BitcoinRate' => $btcRate, 'BonusRate' => $bonusRate, 'TransactionID' => $tranxId, 'UniqueID' => $uid);
                    $allAddresses[$walletAddress] = $tempArr;
                }

                $addressCount = count($allAddresses); // 13
                $charLimit = Utility::GetCharLimit(); //1960
                $loops = (int)ceil((($addressCount * (strlen($allAddresses[array_keys($allAddresses)[0]]['WalletAddress'])))) / $charLimit); // The number of loops to batch query all addresses under the character limit 1
                $maxAddressesInQuery = (int)floor($charLimit / strlen($allAddresses[array_keys($allAddresses)[0]]['WalletAddress'])); // The max number of addresses in one query 490
                $count = 0; // 0


                // due to the manor in which the below code does not generate duplicate entries
                while ($count < $loops) {
                    if ($loops - $count == 1) {
                        //                    Last loop, don't do full iteration
                        $remainder = ($addressCount - ($count * $maxAddressesInQuery)); // 20
                        $urlQuery = $apiUrl . $allAddresses[array_keys($allAddresses)[$count * $maxAddressesInQuery]]['WalletAddress']; // 80
                        for ($i = 1; $i < $remainder; $i++) {
                            $urlQuery .= '|' . $allAddresses[array_keys($allAddresses)[($count * $maxAddressesInQuery) + $i]]['WalletAddress'];
                        }
                        $curl = curl_init($urlQuery);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                        $curlResult = curl_exec($curl);
                        $jsonResult = json_decode($curlResult);
                        array_push($addressData, $jsonResult);
                    }
                    else {
                        $urlQuery = $apiUrl . $allAddresses[array_keys($allAddresses)[$count * $maxAddressesInQuery]]['WalletAddress'];
                        for ($i = 1; $i < $maxAddressesInQuery; $i++) {
                            $urlQuery = $urlQuery . '|' . $allAddresses[array_keys($allAddresses)[($count * $maxAddressesInQuery) + $i]]['WalletAddress'];
                        }
                        $curl = curl_init($urlQuery);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                        $curlResult = curl_exec($curl);
                        $jsonResult = json_decode($curlResult);
                        array_push($addressData, $jsonResult);
                    }
                    // Make query
                    $count++;
                }
            }
            else {
                Utility::AppendToLog($return, 'No results from btc tranx search. ');
            }


            if (empty($addressData)) {
                Utility::AppendToErrorLog($return, 'The required value "addressData" from scanBtcTranxChanges has not been correctly set, but is required for the next sections. ');
                return $return;
            }

            //            Check if wallet balances have changes, if so, update them
            mysqli_data_seek($results, 0);
            foreach ($allAddresses as $address) {
                $email = $address['EmailAddress'];
                $referredBy = $address['ReferredByID'];
                $lastBalance = $address['LastBalance'];
                $walletAddress = $address['WalletAddress'];
                $walletId = $address['WalletID'];
                $bonusRate = $address['BonusRate'];
                $tranxId = $address['TransactionID'];
                $uid = $address['UniqueID'];


                //                    If the wallet has more than a 5 cent difference since it was last checked
                $currentBalance = Utility::SatoshiToBtc(($addressData[0]->{$walletAddress}->{'total_received'}));


                $btcWalletDelta = $currentBalance - $lastBalance; // This is the size of the contribution (how much wallet has changed since last check);
                Utility::AppendToLog($return, 'Current Balance: ' . $currentBalance . ' Delta: ' . $btcWalletDelta);

                if ($btcWalletDelta > $btcOmega) {
                    Utility::AppendToLog($return, 'Wallet Balance changed!');

                    $actualTokens = Utility::BtcToGain($currentBtcExchangeRate, $btcWalletDelta);
                    $actualBonus = $actualTokens * $bonusRate;

                    HandleConfirmTransaction($conn, 'BTC', $tranxId, $currentBalance, $currentBtcExchangeRate, $walletId, $actualTokens, $actualBonus, $btcWalletDelta, $referredBy, $uid, $email, $return);
                }
                elseif ($btcWalletDelta < 0) {
                    //                    Bitcoin checks the total_received for a wallet, not the current balance. So this if shouldn't get hit
                    Utility::AppendToErrorLog($return, 'ERROR! BTC wallet balance has dropped!');
                    //TODO: Consider updating LastBalance of wallet if balance drops?
                }
                else {
                    Utility::AppendToLog($return, 'Wallet balance NOT changed');
                }
            }

            mysqli_free_result($results);
        }
        else {
            mysqli_stmt_close($stmt);
            $return->errorLog = $return->errorLog ?? '' . 'Unable to prepare joinSelect stmt ' . mysqli_error($conn);
        }

        //        Partition: Cancel out-dated transactions
        killOldTranx($conn, $return, 'BTC');

        return $return;
    }

    function &UpdateWalletTranxStatus($conn, $return, $crypto) {
        switch ($crypto) {
            case 'ETH':
                $sqlSelect = "SELECT * FROM EthereumWallets WHERE TransactionStatus = 'Pending' OR TransactionStatus = 'Complete';";
                $stmtcheck = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmtcheck, $sqlSelect)) {
                    mysqli_stmt_execute($stmtcheck);
                    $results = mysqli_stmt_get_result($stmtcheck);

                    $numFirstWalletPass = 0;
                    $numFirstWalletFail = 0;
                    while ($row = mysqli_fetch_assoc($results)) {
                        $walletId = (int)$row['WalletID'];
                        if (!isset($row['LastBalance'])) {
                            $currentBalance = Utility::GetEthWalletBalance($row['WalletAddress']);

                            $sqlInsert = "UPDATE EthereumWallets SET LastBalance = ? WHERE WalletID = ?;";
                            $updatestmt = mysqli_stmt_init($conn);
                            if (mysqli_stmt_prepare($updatestmt, $sqlInsert)) {
                                mysqli_stmt_bind_param($updatestmt, "di", $currentBalance, $walletId);
                                mysqli_stmt_execute($updatestmt);
                                Utility::AppendToLog($return, 'Eth Wallet balances have been updated');
                                $numFirstWalletPass++;
                            }
                            else {
                                $return->errorLog = $return->errorLog ?? '' . 'Unable to prepare Set initial Eth LastBalance stmt ' . mysqli_error($conn);
                                $numFirstWalletFail++;
                            }

                            mysqli_stmt_free_result($updatestmt);
                            mysqli_stmt_close($updatestmt);
                        }

                        //                Find any transactions set to go to this wallet, if none of them are pending, change wallet status to complete (to account for legacy wallet statuses that haven't been correctly updated)
                        $sqlSelect = "SELECT TransactionStatus, ReceivingAddress FROM EthereumTransactions WHERE ReceivingAddress = ? AND TransactionStatus = 'Pending';";
                        $stmtstatus = mysqli_stmt_init($conn);
                        if (mysqli_stmt_prepare($stmtstatus, $sqlSelect)) {
                            $receivingAddress = $row['WalletAddress'];
                            mysqli_stmt_bind_param($stmtstatus, "s", $receivingAddress);
                            mysqli_stmt_execute($stmtstatus);
                            $statusResults = mysqli_stmt_get_result($stmtstatus);

                            //                    If no pending transactions were found, change tranx status of wallet to complete
                            if (mysqli_num_rows($statusResults) > 0) {
                                Utility::AppendToLog($return, 'Eth Wallet ' . $walletId . ' still has pending transactions. ');
                                $sqlUpdate = "UPDATE EthereumWallets SET TransactionStatus = 'Pending' WHERE WalletAddress = ? AND TransactionStatus != 'Pending';";

                                $legacystmt = mysqli_stmt_init($conn);
                                if (mysqli_stmt_prepare($legacystmt, $sqlUpdate)) {
                                    mysqli_stmt_bind_param($legacystmt, "s", $receivingAddress);
                                    mysqli_stmt_execute($legacystmt);
                                    Utility::AppendToLog($return, 'Updated Eth Wallet' . $walletId . 'to Pending tranxStatus. ');
                                }
                                else {
                                    Utility::AppendToErrorLog($return, 'Problem preparing stmt to update Eth Wallets to Completed tranxStatus: ' . mysqli_error($conn));
                                }
                            }
                            else {
                                $sqlUpdate = "UPDATE EthereumWallets SET TransactionStatus = 'Complete' WHERE WalletAddress = ?;";

                                $legacystmt = mysqli_stmt_init($conn);
                                if (mysqli_stmt_prepare($legacystmt, $sqlUpdate)) {
                                    mysqli_stmt_bind_param($legacystmt, "s", $receivingAddress);
                                    mysqli_stmt_execute($legacystmt);
                                    Utility::AppendToLog($return, 'Updated Eth Wallet' . $walletId . 'to Completed tranxStatus. ');
                                }
                                else {
                                    Utility::AppendToErrorLog($return, 'Problem preparing stmt to update Wallets to Completed tranxStatus: ' . mysqli_error($conn));
                                }
                            }

                            mysqli_free_result($statusResults);
                            mysqli_stmt_close($stmtstatus);
                        }
                        else {
                            Utility::AppendToErrorLog($return, 'Unable to prepare Check wallet transaction status statement: ' . mysqli_error($conn));
                        }
                    }

                    mysqli_stmt_free_result($stmtcheck);
                    mysqli_stmt_close($stmtcheck);
                    //            Debug:
                    Utility::AppendToLog($return, 'Number of new eth wallets initialized: ' . $numFirstWalletPass);
                    if ($numFirstWalletFail > 0) {
                        Utility::AppendToErrorLog($return, 'Number of new eth wallets failed to initialize:  ' . $numFirstWalletFail);
                    }
                }
                else {
                    mysqli_stmt_close($stmtcheck);
                    Utility::AppendToErrorLog($return, 'Problem preparing LastBalance check statement: ' . mysqli_error($conn));
                }
                break;
            case 'BTC':
                $sqlSelect = "SELECT * FROM BitcoinWallets WHERE TransactionStatus = 'Pending' OR TransactionStatus = 'Complete';";
                $stmtcheck = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmtcheck, $sqlSelect)) {
                    mysqli_stmt_execute($stmtcheck);
                    $results = mysqli_stmt_get_result($stmtcheck);

                    $numFirstWalletPass = 0;
                    $numFirstWalletFail = 0;
                    while ($row = mysqli_fetch_assoc($results)) {
                        $walletId = (int)$row['WalletID'];
                        //                Utility::DebugDump($row);
                        if (!isset($row['LastBalance'])) {
                            $currentBalance = Utility::GetBtcWalletBalance($row['WalletAddress']);

                            $sqlInsert = "UPDATE BitcoinWallets SET LastBalance = ? WHERE WalletID = ?;";
                            $updatestmt = mysqli_stmt_init($conn);
                            if (mysqli_stmt_prepare($updatestmt, $sqlInsert)) {
                                mysqli_stmt_bind_param($updatestmt, "di", $currentBalance, $walletId);
                                mysqli_stmt_execute($updatestmt);
                                Utility::AppendToLog($return, 'Btc Wallet balances have been updated');
                                $numFirstWalletPass++;
                            }
                            else {
                                $return->errorLog = $return->errorLog ?? '' . 'Unable to prepare Set initial Btc LastBalance stmt ' . mysqli_error($conn);
                                $numFirstWalletFail++;
                            }

                            mysqli_stmt_free_result($updatestmt);
                            mysqli_stmt_close($updatestmt);
                        }

                        //                Find any transactions set to go to this wallet, if none of them are pending, change wallet status to complete (to account for legacy wallet statuses that haven't been correctly updated)
                        $sqlSelect = "SELECT TransactionStatus, ReceivingAddress FROM BitcoinTransactions WHERE ReceivingAddress = ? AND TransactionStatus = 'Pending';";
                        $stmtstatus = mysqli_stmt_init($conn);
                        if (mysqli_stmt_prepare($stmtstatus, $sqlSelect)) {
                            $receivingAddress = $row['WalletAddress'];
                            mysqli_stmt_bind_param($stmtstatus, "s", $receivingAddress);
                            mysqli_stmt_execute($stmtstatus);
                            $statusResults = mysqli_stmt_get_result($stmtstatus);

                            //                    If no pending transactions were found, change tranx status of wallet to complete
                            if (mysqli_num_rows($statusResults) > 0) {
                                Utility::AppendToLog($return, 'Btc Wallet ' . $walletId . ' still has pending transactions. ');
                                $sqlUpdate = "UPDATE BitcoinWallets SET TransactionStatus = 'Pending' WHERE WalletAddress = ? AND TransactionStatus != 'Pending';";

                                $legacystmt = mysqli_stmt_init($conn);
                                if (mysqli_stmt_prepare($legacystmt, $sqlUpdate)) {
                                    mysqli_stmt_bind_param($legacystmt, "s", $receivingAddress);
                                    mysqli_stmt_execute($legacystmt);
                                    Utility::AppendToLog($return, 'Updated Wallet' . $walletId . 'to Pending tranxStatus. ');
                                }
                                else {
                                    Utility::AppendToErrorLog($return, 'Problem preparing stmt to update Btc Wallets to Completed tranxStatus: ' . mysqli_error($conn));
                                }
                            }
                            else {
                                $sqlUpdate = "UPDATE BitcoinWallets SET TransactionStatus = 'Complete' WHERE WalletAddress = ?;";

                                $legacystmt = mysqli_stmt_init($conn);
                                if (mysqli_stmt_prepare($legacystmt, $sqlUpdate)) {
                                    mysqli_stmt_bind_param($legacystmt, "s", $receivingAddress);
                                    mysqli_stmt_execute($legacystmt);
                                    Utility::AppendToLog($return, 'Updated Wallet' . $walletId . 'to Completed tranxStatus. ');
                                }
                                else {
                                    Utility::AppendToErrorLog($return, 'Problem preparing stmt to update Btc Wallets to Completed tranxStatus: ' . mysqli_error($conn));
                                }
                            }

                            mysqli_free_result($statusResults);
                            mysqli_stmt_close($stmtstatus);
                        }
                        else {
                            Utility::AppendToErrorLog($return, 'Unable to prepare Check Btc wallet transaction status statement: ' . mysqli_error($conn));
                        }
                    }

                    mysqli_stmt_free_result($stmtcheck);
                    mysqli_stmt_close($stmtcheck);
                    //            Debug:
                    Utility::AppendToLog($return, 'Number of new wallets initialized: ' . $numFirstWalletPass);
                    if ($numFirstWalletFail > 0) {
                        Utility::AppendToErrorLog($return, 'Number of new Btc wallets failed to initialize:  ' . $numFirstWalletFail);
                    }
                }
                else {
                    mysqli_stmt_close($stmtcheck);
                    Utility::AppendToErrorLog($return, 'Problem preparing Btc LastBalance check statement: ' . mysqli_error($conn));
                }
                break;
            default:
                Utility::AppendToErrorLog($return, 'Incorrectly formatted crypto in UpdateWallTranx. ');
                break;
        }

        return $return;
    }

    function &HandleConfirmTransaction($conn, $crypto, $transactionId, $newBalance, $cryptoRate, $walletId, $actualTokens, $actualBonus, $actualContribution, $referredBy, $uid, $email, $return) {

        switch ($crypto) {
            case 'ETH':
                // Partition: Update EthereumTransactions
                $sqlUpdate = "UPDATE EthereumTransactions SET ActualReservedTokens = ?, ActualBonusTokens = ?, ContributionAmountReceived = ?, 
                              TransactionStatus = ?, EthereumRate = ?, ConfirmedAt = NOW() WHERE TransactionID = ?;";
                $stmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
                    $transactionStatus = 'Complete';
                    mysqli_stmt_bind_param($stmt, "dddsdi", $actualTokens, $actualBonus, $actualContribution, $transactionStatus, $cryptoRate, $transactionId);
                    mysqli_stmt_execute($stmt);
                    Utility::AppendToLog($return, 'Updated Eth Transaction ' . $transactionId . ' Status. ');
                }
                else {
                    Utility::AppendToErrorLog($return, 'Problem preparing SQL statement: ' . mysqli_error($conn) . '. ');
                }
                mysqli_stmt_close($stmt);

                // Partition: Update EthereumWallets balance
                $sqlUpdate = "UPDATE EthereumWallets SET LastBalance = ? WHERE WalletID = ?;";

                $stmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
                    mysqli_stmt_bind_param($stmt, "ds", $newBalance, $walletId);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    Utility::AppendToLog($return, 'Updated Eth Wallet ' . $walletId . ' Balance. ');
                }
                else {
                    mysqli_stmt_close($stmt);
                    Utility::AppendToErrorLog($return, 'Problem preparing SQL statement: ' . mysqli_error($conn) . '. ');
                }

                // Partition: Send confirmation email for payment and bonus
                $gainfyImageLink = Utility::GetEmailLogoLink();
                $backgroundLink = Utility::GetEmailBgLink();
                $headers = Utility::GetEmailHeaders();
                $subject = "Gainfy Token Offering Contribution";
                $totalReserved = ((float)$actualTokens + (float)$actualBonus);
                $tranxConfEmailBody = prepareTranxConfEmailBody($gainfyImageLink, $backgroundLink, $actualContribution, $totalReserved, $crypto);

                mail($email, $subject, $tranxConfEmailBody, $headers);
                Utility::AppendToLog($return, 'Sent Eth Contribution ' . $transactionId . ' Confirmation email to ' . $email . '. ');

                // Partition: Trigger Referral Bonus
                if (isset($referredBy)) {
                    TriggerReferralBonus($conn, $return, $email, $uid, $referredBy, $actualTokens);
                }

                break;
            case 'BTC':
                // Partition: Update BitcoinTransaction
                $sqlUpdate = "UPDATE BitcoinTransactions SET ActualReservedTokens = ?, ActualBonusTokens = ?, ContributionAmountReceived = ?, 
TransactionStatus = ?, BitcoinRate = ?, ConfirmedAt = NOW() WHERE TransactionID = ?;";
                $stmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
                    $transactionStatus = 'Complete';
                    mysqli_stmt_bind_param($stmt, "dddsdi", $actualTokens, $actualBonus, $actualContribution, $transactionStatus, $cryptoRate, $transactionId);
                    mysqli_stmt_execute($stmt);
                    Utility::AppendToLog($return, 'Updated Btc Transaction ' . $transactionId . ' Status. ');
                }
                else {
                    Utility::AppendToErrorLog($return, 'Problem preparing SQL statement: ' . mysqli_error($conn) . '. ');
                }
                mysqli_stmt_close($stmt);

                // Partition: Update BitcoinWallet balance
                $sqlUpdate = "UPDATE BitcoinWallets SET LastBalance = ? WHERE WalletID = ?;";

                $stmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
                    mysqli_stmt_bind_param($stmt, "ds", $newBalance, $walletId);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    Utility::AppendToLog($return, 'Updated Btc Wallet ' . $walletId . ' Balance');
                }
                else {
                    mysqli_stmt_close($stmt);
                    Utility::AppendToErrorLog($return, 'Problem preparing SQL statement: ' . mysqli_error($conn) . '. ');
                }

                // Partition: Send confirmation email for payment and bonus
                $gainfyImageLink = Utility::GetEmailLogoLink();
                $backgroundLink = Utility::GetEmailBgLink();
                $headers = Utility::GetEmailHeaders();
                $subject = "Gainfy Token Offering Contribution";
                $totalReserved = ((float)$actualTokens + (float)$actualBonus);
                $tranxConfEmailBody = prepareTranxConfEmailBody($gainfyImageLink, $backgroundLink, $actualContribution, $totalReserved, $crypto);

                mail($email, $subject, $tranxConfEmailBody, $headers);
                Utility::AppendToLog($return, 'Sent Btc Contribution ' . $transactionId . ' Confirmation email to ' . $email . '. ');

                // Partition: Trigger Referral Bonus
                if (isset($referredBy)) {
                    TriggerReferralBonus($conn, $return, $email, $uid, $referredBy, $actualTokens);
                }

                break;
            default:
                Utility::AppendToErrorLog($return, 'Error Handling Tranx Confirmation. Invalid Crypto Type.');
        }

        return $return;
    }

    function &TriggerReferralBonus($conn, $return, $email, $uid, $referrerUid, $actualTokens) {
        $subject = "Gainfy STO Referral Bonus";
        $gainfyImageLink = Utility::GetEmailLogoLink();
        $backgroundLink = Utility::GetEmailBgLink();
        $headers = Utility::GetEmailHeaders();

        $sqlUpdate = "UPDATE Referrals SET ContributionCount = ContributionCount + 1, ContributionValue = ContributionValue + ? WHERE UniqueID = ?;";
        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
            mysqli_stmt_bind_param($stmt, "ds", $actualTokens, $uid);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            Utility::AppendToLog($return, 'Updated Referral count');
        }
        else {
            mysqli_stmt_close($stmt);
            Utility::AppendToErrorLog($return, 'Problem preparing SQL statement: ' . mysqli_error($conn) . '. ');
        }


        // Partition: Trigger Referral Bonus Emails
        $sqlSelect = "SELECT EmailAddress FROM Accounts WHERE UniqueID = ?;";
        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlSelect)) {
            mysqli_stmt_bind_param($stmt, "s", $referrerUid);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            // Send to referrer (person who referred the user who made the contribution)
            $referrerEmail = mysqli_fetch_assoc($result)['EmailAddress'];
            $referralBonus = Utility::GetReferralBonus();
            $refToReferrerEmail = prepareToReferrerEmail($gainfyImageLink, $backgroundLink, $referralBonus, $email);

            // Send to referee (person who made contribution)
            $refToRefereeEmail = prepareToRefereeEmail($gainfyImageLink, $backgroundLink, $referralBonus, $referrerEmail);

            mail($email, $subject, $refToRefereeEmail, $headers);

            mail($referrerEmail, $subject, $refToReferrerEmail, $headers);

            mysqli_free_result($result);
            mysqli_stmt_close($stmt);

            Utility::AppendToLog($return, 'Sent Referral Bonus Email');
        }
        else {
            mysqli_stmt_close($stmt);
            Utility::AppendToErrorLog($return, 'Unable to prepare fetch referrer email statement' . mysqli_error($conn) . '. ');
        }

        return $return;
    }

    function &killOldTranx($conn, $return, $crypto) {
        $gainfyImageLink = Utility::GetEmailLogoLink();
        $backgroundLink = Utility::GetEmailBgLink();
        $headers = Utility::GetEmailHeaders();
        $subject = "Gainfy STO Contribution Expired";

        switch ($crypto) {
            case 'ETH':
                $ttl = Utility::GetEthTtl();
                $sqlSelect = "SELECT EthereumTransactions.UniqueID, Accounts.EmailAddress, EthereumTransactions.DateAndTime, EthereumTransactions.TransactionID,
                    EthereumTransactions.ReceivingAddress, EthereumTransactions.ExpectedReservedTokens, EthereumTransactions.ExpectedContributionAmount
                    FROM EthereumTransactions
                       INNER JOIN Accounts ON EthereumTransactions.UniqueID = Accounts.UniqueID
                    WHERE TransactionStatus = 'Pending'
                      AND DateAndTime <= DATE_SUB(NOW(), INTERVAL ? DAY);";

                $selstmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($selstmt, $sqlSelect)) {
                    mysqli_stmt_bind_param($selstmt, "i", $ttl);
                    mysqli_stmt_execute($selstmt);
                    $results = mysqli_stmt_get_result($selstmt);

                    while ($row = mysqli_fetch_assoc($results)) {
                        $timestamp = $row['DateAndTime'];
                        $email = $row['EmailAddress'];
                        $walletAddress = $row['ReceivingAddress'];
                        $expectedTokens = $row['ExpectedReservedTokens'];
                        $expectedContribution = $row['ExpectedContributionAmount'];
                        $tranxId = $row['TransactionID'];
                        $emailBody = prepareTranxExpireEmail($gainfyImageLink, $backgroundLink, $timestamp, $walletAddress, $tranxId, $expectedTokens, $expectedContribution, $ttl, $crypto);

                        mail($email, $subject, $emailBody, $headers);
                        Utility::AppendToLog($return, 'Sent contribution expiration email to ' . $email . ' for ETH TranxID: ' . $tranxId);
                    }

                    mysqli_free_result($results);
                    Utility::AppendToLog($return, 'Sent email to users whose eth transactions are being canceled');
                }
                else {
                    Utility::AppendToErrorLog($return, 'Unable to prepare SQL statement, Select eth expired contributions: ' . mysqli_error($conn) . '. ');
                }
                mysqli_stmt_close($selstmt);

                $sqlUpdate = "UPDATE EthereumTransactions SET TransactionStatus = 'Canceled' WHERE TransactionStatus = 'Pending' AND DateAndTime <= DATE_SUB(NOW(), INTERVAL ? DAY);";

                $stmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
                    mysqli_stmt_bind_param($stmt, "i", $ttl);
                    mysqli_stmt_execute($stmt);
                    Utility::AppendToLog($return, 'Canceled outdated eth transactions. ');
                }
                else {
                    Utility::AppendToErrorLog($return, 'Unable to prepare SQL statement, Cancel eth expired contributions: ' . mysqli_error($conn) . '. ');
                }
                mysqli_stmt_close($stmt);
                break;
            case 'BTC':
                $ttl = Utility::GetBtcTtl();
                $sqlSelect = "SELECT BitcoinTransactions.UniqueID, Accounts.EmailAddress, BitcoinTransactions.DateAndTime, BitcoinTransactions.TransactionID,
                    BitcoinTransactions.ReceivingAddress, BitcoinTransactions.ExpectedReservedTokens, BitcoinTransactions.ExpectedContributionAmount
                    FROM BitcoinTransactions
                       INNER JOIN Accounts ON BitcoinTransactions.UniqueID = Accounts.UniqueID
                    WHERE TransactionStatus = 'Pending'
                      AND DateAndTime <= DATE_SUB(NOW(), INTERVAL ? DAY);";

                $selstmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($selstmt, $sqlSelect)) {
                    mysqli_stmt_bind_param($selstmt, "i", $ttl);
                    mysqli_stmt_execute($selstmt);
                    $results = mysqli_stmt_get_result($selstmt);

                    while ($row = mysqli_fetch_assoc($results)) {
                        $timestamp = $row['DateAndTime'];
                        $email = $row['EmailAddress'];
                        $walletAddress = $row['ReceivingAddress'];
                        $expectedTokens = $row['ExpectedReservedTokens'];
                        $expectedContribution = $row['ExpectedContributionAmount'];
                        $tranxId = $row['TransactionID'];
                        $emailBody = prepareTranxExpireEmail($gainfyImageLink, $backgroundLink, $timestamp, $walletAddress, $tranxId, $expectedTokens, $expectedContribution, $ttl, $crypto);

                        mail($email, $subject, $emailBody, $headers);
                        Utility::AppendToLog($return, 'Sent contribution expiration email to ' . $email . ' for BTC TranxID: ' . $tranxId);
                    }

                    mysqli_free_result($results);
                    Utility::AppendToLog($return, 'Sent email to users whose btc transactions are being canceled');
                }
                else {
                    Utility::AppendToErrorLog($return, 'Unable to prepare SQL statement, Select expired btc contributions: ' . mysqli_error($conn) . '. ');
                }
                mysqli_stmt_close($selstmt);

                $sqlUpdate = "UPDATE BitcoinTransactions SET TransactionStatus = 'Canceled' WHERE TransactionStatus = 'Pending' AND DateAndTime <= DATE_SUB(NOW(), INTERVAL ? DAY);";

                $stmt = mysqli_stmt_init($conn);
                if (mysqli_stmt_prepare($stmt, $sqlUpdate)) {
                    mysqli_stmt_bind_param($stmt, "i", $ttl);
                    mysqli_stmt_execute($stmt);
                    Utility::AppendToLog($return, 'Canceled outdated btc transactions. ');
                }
                else {
                    Utility::AppendToErrorLog($return, 'Unable to prepare SQL statement, Cancel expired btc contributions: ' . mysqli_error($conn) . '. ');
                }
                mysqli_stmt_close($stmt);
                break;
            default:
                Utility::AppendToErrorLog($return, 'Incorrect formatting of $crypto in killOldTranx. ');
                break;
        }

        return $return;
    }

    function prepareTranxExpireEmail($gainfyLogo, $bgImg, $timestamp, $walletAddress, $tranxId, $expectedTokens, $expectedContribution, $ttl, $crypto) {
        $tnxNo = 'GAIN' . $crypto . $tranxId;
        return <<<EOD
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Contribution Expired</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <style>
        b, strong {
            font-weight: 600;
        }

        a:link, a:visited {
            text-decoration: none;
            color: #4185b9;
        }

        a:hover, a:focus, a:active {
            outline: 0;
            color: #46bdf4;
        }

        .background {
            font-family: "Poppins", sans-serif;
            color: #6e7571;
            font-size: 16pt;
            line-height: 1.86;
            font-weight: 300;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            position: relative;
            background: rgb(233, 244, 255);
            background-image: url('https://sto.gainfy.com/assets/images/bg-ath.png');
            background-attachment: fixed;
            background-origin: content-box;
            background-position: center;
            -webkit-background-size: 100%;
            background-size: 100%;
            min-width: 320px;
            display: flex;
            flex-flow: column;
            flex-basis: auto;
            flex-wrap: wrap;
            flex-grow: inherit;
            -webkit-flex-direction: column;
            padding: 0;
            margin: 0;
            border: none;
        }

        .email-body {
            display: flex;
            width: 100%;
            justify-content: space-evenly;
            vertical-align: auto;
            text-justify: auto;
            padding: 0;
            margin: 0;
            border: none;
        }

        .gainfy-logo {
            align-content: center;
            justify-content: center;
            display: flex;
            display: -webkit-flex;
            padding: 5px 10px 20px 10px;
            margin: auto;
        }

        .header-title {
            text-align: center;
            justify-content: center;
            align-content: center;
            padding: 5px 5px 5px 5px;
            margin: auto;
            color: #6e7571;
        }

        .header-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
        }

        .body-table {
            /*border: 2px solid black;*/
            margin: 20px auto 5px auto;
            padding: 5px 5px 5px 5px;
            justify-content: space-evenly;
            flex-direction: column;
            -webkit-flex-direction: column;
            text-align: left;
            width: 60%;
        }

        .footer-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
            padding: 8px 20px 5px 20px;
        }

        .footer-table td {
            padding: 8px;
        }

        .footer-bar {
            width: 100%;
            border: none;
            height: 2px;
            color: #6e7571;
            background-color: #6e7571;
            overflow-x: auto;
        }

        .footer-links {
            justify-content: space-evenly;
            flex-direction: row;
            -webkit-flex-direction: row;
            color: #6e7571;
            overflow-x: auto;
            text-overflow: inherit;
        }

        .upper-text, .lower-text {
            overflow-x: auto;
            text-overflow: inherit;
            text-align: left;
            width: 85%;
        }

        p {
            font-size: 14pt;
            padding: 5px 20px 15px 20px;
            text-align: inherit;
            color: #474e4a;
        }

        .data-table {
            border-top: 1px solid #6e7571;
            border-bottom: 1px solid #6e7571;
            padding: 10px;
            justify-content: center;
            align-content: center;
            -webkit-align-content: center;
            -webkit-justify-content: center;
            margin: 20px auto 20px auto;
        }

           .data-table th {
            border-bottom: 1px solid #6e7571;
            padding: 10px 0 15px 0;
            text-align: center;
            color: #6e7571;
        }

        .data-table td {
            padding: 13px 8px 5px 8px;
            text-align: left;
            color: #6e7571;
        }

        .exit-text {
            text-align: left;
            padding: 20px 0 0 0;
        }



    </style>
</head>

<body>
    <div class='background'>
        <table class='email-body'>
            <tr class='header-row'>
                <td>
                    <table class='header-table'>
                        <tr class='gainfy-logo'>
                            <td>
                                <img src='https://sto.gainfy.com/images/logo2x.png' style='height: 160px; width: 400px;' />
                            </td>
                        </tr>
                        <tr class='header-title'>
                            <td>
                                <h2><strong>Your contribution has expired!</strong></h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='body'>
                <td>
                    <table class='body-table'>
                        <tr class='upper-text'>
                            <td>
                                <table class='upper-text-table'>
                                    <tr class='paragraph-one'>
                                        <td>
                                            <p>Your expected contribution of $expectedContribution was not received within $ttl day(s), and therefore has now expired. As stated on the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/tokens.html'><strong>Reserve Tokens</strong></a> page, any $crypto contributions not received within $ttl day(s) will expire thereafter.</p>
                                        </td>
                                    </tr>
                                    <tr class='paragraph-two'>
                                        <td>
                                            <p>If you would like to reserve GAIN tokens, please visit the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/Tokens.html'><strong>Reserve Tokens</strong></a> page and schedule a new contribution. To view the status of all your contributions, navigate to the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/referrals.html'><strong>Transactions</strong></a> page and select the $crypto transactions tab.
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='data'>
                            <td>
                                <table class='data-table'>
                                    <tr class='data-table-header'>
                                        <th colspan='2'><strong>Expired Contribution</strong></th>
                                    </tr>
                                    <tr>
                                        <td><strong>Tnx No:</strong></td>
                                        <td>$tnxNo</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Expected Contribution:</strong></td>
                                        <td>$expectedContribution $crypto</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Expected Tokens:</strong></td>
                                        <td>$expectedTokens GAIN</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Receiving Wallet:</strong></td>
                                        <td>$walletAddress</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Scheduled On:</strong></td>
                                        <td>$timestamp</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='lower-text'>
                            <td>
                                <p>Be sure to check us out on <a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'><strong>Twitter</strong></a>, contact us on our <a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'><strong> Telegram</strong></a> group, and visit the official <a rel='noreferrer' target='_blank' href='https://gainfy.com'><strong>Gainfy website</strong></a>.</p>
                            </td>
                        </tr>
                        <tr class='exit-text'>
                            <td>
                                <p><strong>Best wishes, <br /><br />The Gainfy Team</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='footer'>
                <td>
                    <table class='footer-table'>
                        <tr>
                            <th colspan='9'>
                                <hr class='footer-bar' />
                            </th>
                        </tr>
                        <tr class='footer-links'>
                            <td><a rel='noreferrer' target='_blank' href='https://gainfy.com'>Gainfy Foundation</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' href='mailto:team@gainfy.com'>Contact Us</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'>Twitter</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://www.linkedin.com/company/11247412/'> Linkedin</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'>Telegram</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
EOD;
    }

    function prepareToRefereeEmail($gainfyLogo, $bgImg, $refBonusAmount, $referrerEmail) {
        return <<<EOD
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Referral Bonus</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <style>
        b, strong {
            font-weight: 600;
        }

        a:link, a:visited {
            text-decoration: none;
            color: #4185b9;
        }

        a:hover, a:focus, a:active {
            outline: 0;
            color: #46bdf4;
        }

        .background {
            font-family: "Poppins", sans-serif;
            color: #6e7571;
            font-size: 16pt;
            line-height: 1.86;
            font-weight: 300;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            position: relative;
            background: rgb(233, 244, 255);
            background-image: url('https://sto.gainfy.com/assets/images/bg-ath.png');
            background-attachment: fixed;
            background-origin: content-box;
            background-position: center;
            -webkit-background-size: 100%;
            background-size: 100%;
            min-width: 320px;
            display: flex;
            flex-flow: column;
            flex-basis: auto;
            flex-wrap: wrap;
            flex-grow: inherit;
            -webkit-flex-direction: column;
            padding: 0;
            margin: 0;
            border: none;
        }

        .email-body {
            display: flex;
            width: 100%;
            justify-content: space-evenly;
            vertical-align: auto;
            text-justify: auto;
            padding: 0;
            margin: 0;
            border: none;
        }

        .gainfy-logo {
            align-content: center;
            justify-content: center;
            display: flex;
            display: -webkit-flex;
            padding: 5px 10px 20px 10px;
            margin: auto;
        }

        .header-title {
            text-align: center;
            justify-content: center;
            align-content: center;
            padding: 5px 5px 5px 5px;
            margin: auto;
            color: #6e7571;
        }

        .header-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
        }

        .body-table {
            /*border: 2px solid black;*/
            margin: 20px auto 5px auto;
            padding: 5px 5px 5px 5px;
            justify-content: space-evenly;
            flex-direction: column;
            -webkit-flex-direction: column;
            text-align: left;
            width: 60%;
        }

        .footer-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
            padding: 8px 20px 5px 20px;
        }

        .footer-table td {
            padding: 8px;
        }

        .footer-bar {
            width: 100%;
            border: none;
            height: 2px;
            color: #6e7571;
            background-color: #6e7571;
            overflow-x: auto;
        }

        .footer-links {
            justify-content: space-evenly;
            flex-direction: row;
            -webkit-flex-direction: row;
            color: #6e7571;
            overflow-x: auto;
            text-overflow: inherit;
        }

        .upper-text, .lower-text {
            overflow-x: auto;
            text-overflow: inherit;
            text-align: left;
            width: 85%;
        }

        p {
            font-size: 14pt;
            padding: 5px 20px 15px 20px;
            text-align: inherit;
            color: #474e4a;
        }

        .data-table {
            border-top: 1px solid #6e7571;
            border-bottom: 1px solid #6e7571;
            padding: 10px;
            justify-content: center;
            align-content: center;
            -webkit-align-content: center;
            -webkit-justify-content: center;
            margin: 20px auto 20px auto;
        }

        .data-table th {
            border-bottom: 1px solid #6e7571;
            padding: 10px 0 15px 0;
            text-align: center;
            color: #6e7571;
        }

        .data-table td {
            padding: 13px 8px 5px 8px;
            text-align: left;
            color: #6e7571;
        }


        .exit-text {
            text-align: left;
            padding: 20px 0 0 0;
        }



    </style>
</head>

<body>
    <div class='background'>
        <table class='email-body'>
            <tr class='header-row'>
                <td>
                    <table class='header-table'>
                        <tr class='gainfy-logo'>
                            <td>
                                <img src='https://sto.gainfy.com/images/logo2x.png' style='height: 160px; width: 400px;' />
                            </td>
                        </tr>
                        <tr class='header-title'>
                            <td>
                                <h2><strong>You've received bonus GAIN tokens!</strong></h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='body'>
                <td>
                    <table class='body-table'>
                        <tr class='upper-text'>
                            <td>
                                <table class='upper-text-table'>
                                    <tr class='paragraph-one'>
                                        <td>
                                            <p>Because you've utilized the Gainfy Token Offering Referral Program, you have received bonus GAIN tokens for your last contribution! You can view more information on the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/referrals.html'><strong>Referral system</strong></a> here.</p>
                                        </td>
                                    </tr>
                                    <tr class='paragraph-two'>
                                        <td>
                                            <p>To earn more bonus tokens, spread the word about the Gainfy Token Offering by sending your <a rel='noreferrer' href='https://sto.gainfy.com/referrals.html'><strong>referral link</strong></a> to your friends and family. Every time someone you've referred makes a contribution of their own, you both earn extra tokens!</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='data'>
                            <td>
                                <table class='data-table'>
                                    <tr class='data-table-header'>
                                        <th colspan='2'><strong>Bonus Details</strong></th>
                                    </tr>
                                    <tr>
                                        <td><strong>Referred By:</strong></td>
                                        <td>$referrerEmail</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Your Bonus:</strong></td>
                                        <td>$refBonusAmount GAIN</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Referrer Bonus:</strong></td>
                                        <td>$refBonusAmount GAIN</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='lower-text'>
                            <td>
                                <table class='lower-text-table'>
                                    <tr class='lower-paragraph-one'>
                                        <td>
                                            <p>Learn more about reserving tokens by checking out the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/tokens.html'><strong>Reserve Tokens</strong></a> page. The earlier in the Token Offering you make your contribution, the more bonus GAIN you receive!</p>
                                        </td>
                                    </tr>
                                    <tr class='lower-paragraph-two'>
                                        <td>
                                            <p>In the meantime - be sure to check us out on <a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'><strong>Twitter</strong></a>, contact us through our <a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'><strong> Telegram</strong></a> group, and visit the official <a rel='noreferrer' target='_blank' href='https://gainfy.com'><strong>Gainfy website</strong></a>.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='exit-text'>
                            <td>
                                <p><strong>Best wishes, <br /><br />The Gainfy Team</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='footer'>
                <td>
                    <table class='footer-table'>
                        <tr>
                            <th colspan='9'>
                                <hr class='footer-bar' />
                            </th>
                        </tr>
                        <tr class='footer-links'>
                            <td><a rel='noreferrer' target='_blank' href='https://gainfy.com'>Gainfy Foundation</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' href='mailto:team@gainfy.com'>Contact Us</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'>Twitter</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://www.linkedin.com/company/11247412/'> Linkedin</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'>Telegram</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
EOD;
    }

    function prepareToReferrerEmail($gainfyLogo, $bgImg, $refBonusAmount, $refereeEmail) {
        return <<<EOD
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Referral Bonus</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <style>
        b, strong {
            font-weight: 600;
        }

        a:link, a:visited {
            text-decoration: none;
            color: #4185b9;
        }

        a:hover, a:focus, a:active {
            outline: 0;
            color: #46bdf4;
        }

        .background {
            font-family: "Poppins", sans-serif;
            color: #6e7571;
            font-size: 16pt;
            line-height: 1.86;
            font-weight: 300;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            position: relative;
            background: rgb(233, 244, 255);
            background-image: url('https://sto.gainfy.com/assets/images/bg-ath.png');
            background-attachment: fixed;
            background-origin: content-box;
            background-position: center;
            -webkit-background-size: 100%;
            background-size: 100%;
            min-width: 320px;
            display: flex;
            flex-flow: column;
            flex-basis: auto;
            flex-wrap: wrap;
            flex-grow: inherit;
            -webkit-flex-direction: column;
            padding: 0;
            margin: 0;
            border: none;
        }

        .email-body {
            display: flex;
            width: 100%;
            justify-content: space-evenly;
            vertical-align: auto;
            text-justify: auto;
            padding: 0;
            margin: 0;
            border: none;
        }

        .gainfy-logo {
            align-content: center;
            justify-content: center;
            display: flex;
            display: -webkit-flex;
            padding: 5px 10px 20px 10px;
            margin: auto;
        }

        .header-title {
            text-align: center;
            justify-content: center;
            align-content: center;
            padding: 5px 5px 5px 5px;
            margin: auto;
            color: #6e7571;
        }

        .header-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
        }

        .body-table {
            /*border: 2px solid black;*/
            margin: 20px auto 5px auto;
            padding: 5px 5px 5px 5px;
            justify-content: space-evenly;
            flex-direction: column;
            -webkit-flex-direction: column;
            text-align: left;
            width: 60%;
        }

        .footer-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
            padding: 8px 20px 5px 20px;
        }

        .footer-table td {
            padding: 8px;
        }

        .footer-bar {
            width: 100%;
            border: none;
            height: 2px;
            color: #6e7571;
            background-color: #6e7571;
            overflow-x: auto;
        }

        .footer-links {
            justify-content: space-evenly;
            flex-direction: row;
            -webkit-flex-direction: row;
            color: #6e7571;
            overflow-x: auto;
            text-overflow: inherit;
        }

        .upper-text, .lower-text {
            overflow-x: auto;
            text-overflow: inherit;
            text-align: left;
            width: 85%;
        }

        p {
            font-size: 14pt;
            padding: 5px 20px 15px 20px;
            text-align: inherit;
            color: #474e4a;
        }

        .data-table {
            border-top: 1px solid #6e7571;
            border-bottom: 1px solid #6e7571;
            padding: 10px;
            justify-content: center;
            align-content: center;
            -webkit-align-content: center;
            -webkit-justify-content: center;
            margin: 20px auto 20px auto;
        }

        .data-table th {
            border-bottom: 1px solid #6e7571;
            padding: 10px 0 15px 0;
            text-align: center;
            color: #6e7571;
        }

        .data-table td {
            padding: 13px 8px 5px 8px;
            text-align: left;
            color: #6e7571;
        }


        .exit-text {
            text-align: left;
            padding: 20px 0 0 0;
        }



    </style>
</head>

<body>
    <div class='background'>
        <table class='email-body'>
            <tr class='header-row'>
                <td>
                    <table class='header-table'>
                        <tr class='gainfy-logo'>
                            <td>
                                <img src='https://sto.gainfy.com/images/logo2x.png' style='height: 160px; width: 400px;' />
                            </td>
                        </tr>
                        <tr class='header-title'>
                            <td>
                                <h2><strong>You've received bonus GAIN tokens!</strong></h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='body'>
                <td>
                    <table class='body-table'>
                        <tr class='upper-text'>
                            <td>
                                <table class='upper-text-table'>
                                    <tr class='paragraph-one'>
                                        <td>
                                            <p>Because you've utilized the Gainfy Token Offering Referral Program, you have received bonus GAIN tokens from the contribution of someone you referred! You can view more information about your referrals on the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/referrals.html'><strong>Referral Page</strong></a>.</p>
                                        </td>
                                    </tr>
                                    <tr class='paragraph-two'>
                                        <td>
                                            <p>To earn more bonus tokens, spread the word about the Gainfy Token Offering by sending your <a rel='noreferrer' href='https://sto.gainfy.com/referrals.html'><strong>referral link</strong></a> to your friends and family. Every time someone you've referred makes a contribution of their own, you both earn extra tokens!</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='data'>
                            <td>
                                <table class='data-table'>
                                    <tr class='data-table-header'>
                                        <th colspan='2'><strong>Bonus Details</strong></th>
                                    </tr>
                                    <tr>
                                        <td><strong>Referred:</strong></td>
                                        <td>$refereeEmail</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Referee Bonus (Them)</strong></td>
                                        <td>$refBonusAmount GAIN</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Referrer Bonus (You)</strong></td>
                                        <td>$refBonusAmount GAIN</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='lower-text'>
                            <td>
                                <table class='lower-text-table'>
                                    <tr class='lower-paragraph-one'>
                                        <td>
                                            <p>Learn more about reserving tokens by checking out the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/tokens.html'><strong>Reserve Tokens</strong></a> page. The earlier in the Token Offering you make your contribution, the more bonus GAIN you receive!</p>
                                        </td>
                                    </tr>
                                    <tr class='lower-paragraph-two'>
                                        <td>
                                            <p>Don't forget to check us out on <a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'><strong>Twitter</strong></a>, contact us through our <a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'><strong> Telegram</strong></a> group, and visit the official <a rel='noreferrer' target='_blank' href='https://gainfy.com'><strong>Gainfy website</strong></a>.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='exit-text'>
                            <td>
                                <p><strong>Best wishes, <br /><br />The Gainfy Team</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='footer'>
                <td>
                    <table class='footer-table'>
                        <tr>
                            <th colspan='9'>
                                <hr class='footer-bar' />
                            </th>
                        </tr>
                        <tr class='footer-links'>
                            <td><a rel='noreferrer' target='_blank' href='https://gainfy.com'>Gainfy Foundation</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' href='mailto:team@gainfy.com'>Contact Us</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'>Twitter</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://www.linkedin.com/company/11247412/'> Linkedin</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'>Telegram</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
EOD;
    }

    function prepareTranxConfEmailBody($gainfyLogo, $bgImg, $actualContribution, $totalReservation, $crypto) {
        return <<<EOD
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Contribution Received</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <style>
        b, strong {
            font-weight: 600;
        }

        a:link, a:visited {
            text-decoration: none;
            color: #4185b9;
        }

        a:hover, a:focus, a:active {
            outline: 0;
            color: #46bdf4;
        }

        .background {
            font-family: "Poppins", sans-serif;
            color: #6e7571;
            font-size: 16pt;
            line-height: 1.86;
            font-weight: 300;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            position: relative;
            background: rgb(233, 244, 255);
            background-image: url('https://sto.gainfy.com/assets/images/bg-ath.png');
            background-attachment: fixed;
            background-origin: content-box;
            background-position: center;
            -webkit-background-size: 100%;
            background-size: 100%;
            min-width: 320px;
            display: flex;
            flex-flow: column;
            flex-basis: auto;
            flex-wrap: wrap;
            flex-grow: inherit;
            -webkit-flex-direction: column;
            padding: 0;
            margin: 0;
            border: none;
        }

        .email-body {
            display: flex;
            width: 100%;
            justify-content: space-evenly;
            vertical-align: auto;
            text-justify: auto;
            padding: 0;
            margin: 0;
            border: none;
        }

        .gainfy-logo {
            align-content: center;
            justify-content: center;
            display: flex;
            display: -webkit-flex;
            padding: 5px 10px 20px 10px;
            margin: auto;
        }

        .header-title {
            text-align: center;
            justify-content: center;
            align-content: center;
            padding: 5px 5px 5px 5px;
            margin: auto;
            color: #6e7571;
        }

        .header-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
        }

        .body-table {
            /*border: 2px solid black;*/
            margin: 20px auto 5px auto;
            padding: 5px 5px 5px 5px;
            justify-content: space-evenly;
            flex-direction: column;
            -webkit-flex-direction: column;
            text-align: left;
            width: 60%;
        }

        .footer-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
            padding: 8px 20px 5px 20px;
        }

        .footer-table td {
            padding: 8px;
        }

        .footer-bar {
            width: 100%;
            border: none;
            height: 2px;
            color: #6e7571;
            background-color: #6e7571;
            overflow-x: auto;
        }

        .footer-links {
            justify-content: space-evenly;
            flex-direction: row;
            -webkit-flex-direction: row;
            color: #6e7571;
            overflow-x: auto;
            text-overflow: inherit;
        }

        .upper-text, .lower-text {
            overflow-x: auto;
            text-overflow: inherit;
            text-align: left;
            width: 85%;
        }

        p {
            font-size: 14pt;
            padding: 5px 20px 15px 20px;
            text-align: inherit;
            color: #474e4a;
        }

        .data-table {
            border-top: 1px solid #6e7571;
            border-bottom: 1px solid #6e7571;
            padding: 10px;
            justify-content: center;
            align-content: center;
            -webkit-align-content: center;
            -webkit-justify-content: center;
            margin: 20px auto 20px auto;
        }

        .data-table th {
            border-bottom: 1px solid #6e7571;
            padding: 10px 0 15px 0;
            text-align: center;
            color: #6e7571;
        }

        .data-table td {
            padding: 13px 8px 5px 8px;
            text-align: left;
            color: #6e7571;
        }


        .exit-text {
            text-align: left;
            padding: 20px 0 0 0;
        }



    </style>
</head>

<body>
    <div class='background'>
        <table class='email-body'>
            <tr class='header-row'>
                <td>
                    <table class='header-table'>
                        <tr class='gainfy-logo'>
                            <td>
                                <img src='https://sto.gainfy.com/images/logo2x.png' style='height: 160px; width: 400px;' />
                            </td>
                        </tr>
                        <tr class='header-title'>
                            <td>
                                <h2><strong>Gainfy has Received your contribution!</strong></h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='body'>
                <td>
                    <table class='body-table'>
                        <tr class='upper-text'>
                            <td>
                                <table class='upper-text-table'>
                                    <tr class='paragraph-one'>
                                        <td>
                                            <p>We've received your contribution, and your tokens have been reserved! You may view additional <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/transactions.html'><strong>transaction info</strong></a> here.</p>
                                        </td>
                                    </tr>
                                    <tr class='paragraph-two'>
                                        <td>
                                            <p>Be sure to <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/kyc.html'><strong>Complete your KYC / AML and / or Accredited Investor info</strong></a> and to designate where you would like your
                                                GAIN tokens to be sent by updating your return address on the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/account.html'><strong>Account Page</strong></a>!</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='data'>
                            <td>
                                <table class='data-table'>
                                    <tr class='data-table-header'>
                                        <th colspan='2'><strong>Contribution Details</strong></th>
                                    </tr>
                                    <tr>
                                        <td><strong>Your Contribution:</strong></td>
                                        <td>$actualContribution $crypto</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Token Reservation:</strong></td>
                                        <td>$totalReservation GAIN (incl. phase bonus)</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='lower-text'>
                            <td>
                                <table class='lower-text-table'>
                                    <tr class='lower-paragraph-one'>
                                        <td>
                                            <p>To earn extra tokens, be sure to take a look at our <a rel='noreferrer' href='https://sto.gainfy.com/referrals.html'>Referral Program</a> and get rewarded for spreading the word about Gainfy!
                                        </td>
                                    </tr>
                                    <tr class='lower-paragraph-two'>
                                        <td>
                                            <p>Don't forget to check us out on <a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'><strong>Twitter</strong></a>, contact us through our <a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'><strong> Telegram</strong></a> group, and visit the official <a rel='noreferrer' target='_blank' href='https://gainfy.com'><strong>Gainfy website</strong></a>.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='exit-text'>
                            <td>
                                <p><strong>Best wishes, <br /><br />The Gainfy Team</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='footer'>
                <td>
                    <table class='footer-table'>
                        <tr>
                            <th colspan='9'>
                                <hr class='footer-bar' />
                            </th>
                        </tr>
                        <tr class='footer-links'>
                            <td><a rel='noreferrer' target='_blank' href='https://gainfy.com'>Gainfy Foundation</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' href='mailto:team@gainfy.com'>Contact Us</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'>Twitter</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://www.linkedin.com/company/11247412/'> Linkedin</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'>Telegram</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
EOD;
    }