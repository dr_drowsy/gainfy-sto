<?php
    include "connection.php";
    $UserID= filter_input(INPUT_POST, 'UserID', FILTER_SANITIZE_STRING);
    $return = new stdClass();

    $sqlDisplayActivity ="SELECT * FROM Accounts WHERE UniqueID = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sqlDisplayActivity)) {
        $return->error = 'Unable to prepare fetch account statement';
    }
    else {
        mysqli_stmt_bind_param($stmt, 's', $UserID);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);

        if (mysqli_num_rows($results) < 1) {
            $return->accountCheck = false;
            $return->log = "No matching account info found";
        }
        else {
            while ($row = mysqli_fetch_all($results)) {
                $arr = $row;
            }

            $FirstName = $arr[0][1];
            $LastName = $arr[0][2];
            $Email = $arr[0][3];
            $Twitter = $arr[0][4];
            $Telegram = $arr[0][5];
            $DOB = $arr[0][6];
            $Citizenship = $arr[0][7];

            if(isset($FirstName) && isset($LastName) && isset($DOB) && isset($Citizenship)) {
                $return->accountCheck = true;
            }
            else {
                $return->accountCheck = false;
            }
        }
        mysqli_stmt_close($stmt);
    }

    mysqli_close($conn);
    $json = json_encode($return);
    echo $json;

?>