<?php
    require_once "connection.php";
//if (isset($_POST['$UserID'])) {

$UserID= filter_input(INPUT_POST, 'UserID', FILTER_SANITIZE_STRING);

            $sqlDisplayUserInfo ="Select * FROM KycCivic Where UniqueID= ?;";
            $stmt = mysqli_stmt_init($conn);
            if(!mysqli_stmt_prepare($stmt, $sqlDisplayUserInfo))
                {
                    echo "sql statement failed";
                }
                else{
                    mysqli_stmt_bind_param($stmt, 's', $UserID);
                    mysqli_stmt_execute($stmt);
                    // mysqli_stmt_bind_result($stmt, $address);
                    //
                    //  mysqli_stmt_fetch($stmt);
                    mysqli_stmt_bind_result($stmt, $result);
                    $results = mysqli_stmt_get_result($stmt);

                    while($row = mysqli_fetch_all($results))
                    {
                        $arr = $row;
                    }
                     mysqli_stmt_close($stmt);
                 }

            $eUniqueID = $arr[0][0];

            if ($eUniqueID != '')
            {
                echo'
                <h2 class="user-panel-title">Identity Verification - KYC</h2>
                    <div class="gaps-2x"></div>
                <div class="status status-verified">
                <span class="status-text">Our KYC Process is Powered by:</span>
                <div class="gaps-2x"></div>
                    <div>
                     <img src="images/civic_logo-vert.png">
                    </div>
                    <div class="gaps-2x"></div>

                    <span class="status-text">Your Identity has been Verified.</span>
                    <div class="gaps-2x"></div>
                </div><!-- .status -->';
            }
            else{
                echo'
                <h2 class="user-panel-title">Identity Verification - KYC</h2>
                <p>To comply with regulation each participant will have to go through indentity verification (KYC). So please complete our fast and secure verification process to participate in our token sale. You can proceed from here to verify your indentity and also you can check your application status if you have already submitted. </p>
                <div class="gaps-2x"></div>

                    <div class="status status-empty">
                        <div id="testSpinner" class="center" style="display:none;">
                            <span class="status-text">Please wait while we process your identity.</span>
                            <div class="gaps-2x"></div>
                            <img id="imgSpinner" src="images/civicLoad.gif" style="height: 30%; width: 30%;"/>
                            <div class="gaps-2x"></div>
                            <div class="note note-plane note-danger" style="font-size:20px">
                            <p >If the process fails, please try again in a few minutes.</p></div>
                        </div>
                    <div id="infoBox">
                        <span class="status-text">Our KYC Process is Powered by:</span>
                        <div class="gaps-2x"></div>
                        <div> <img src="images/civic_logo-vert.png"></div>
                        <div class="gaps-2x"></div>
                        <div class="note note-plane note-danger" style="font-size:20px">
                        <p >You have not completed your KYC Verification</p></div>
                        <div class="gaps-2x"></div>
                        <button  id="kycButton" class="civic-button" type="button">
                        <svg class="civic-logo-icon" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <path d="M3,12 C3,16.7557705 6.8112793,21 12,21 C16.3410645,21 19.8986122,18.5324628 20.9909576,15 L24,15 C22.8386328,20.1411987 18.861084,24 12,24 C4.36572266,24 4.40760283e-16,18.8982832 0,12.0000449 C0,5.10180664 4.38793945,0 12,0 C19.0664062,0 22.8386328,3.85880133 24,9 L20.9909576,9 C19.8986122,5.46744739 16.6115723,3 12,3 C6.49707031,3 3,7.24413967 3,12 Z M12,8 C13.6569,8 15,9.28596049 15,10.872371 C15,12.006383 13.9967,12.9866275 13,13.4535793 L13,17 L11,17 L11,13.4535793 C10.0032,12.9866275 9,12.006383 9,10.872371 C9,9.28596049 10.3432,8 12,8 Z" fill="#FFFFFF" /></svg>
                        Connect with Civic </button>
                    </div>
                    </div>

                <div class="gaps-1x"></div>
                <div class="row">
                <div class="col-sm-5">
                <button  id="whatsCivic" class="civic-button" type="button">
                What is Civic?
                </button>
                </div>


                </div>
                <div class="gaps-2x"></div>

                <div class="note note-md note-info note-plane">
                    <em class="fas fa-info-circle"></em>
                    <p>Some of contries and regions will not able to pass KYC process and therefore are restricted from token sale.</p>
                </div>';
                echo '';
            }


            // $sqlInsertpart1 = "Select VerificationLevel From KycCivic ";
            // $sqlInsertpart2 ="(`UniqueID`, `verificationLevel`,`IdType`,`IdNumber`,
            //                     `IdName`, `IdDOB`, `IdDOI`, `IdDOE`, `IdImage`, `idImageMd5`, `Country`,`email`,`phone`)
            //                     Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            // $sqlInsertFull = $sqlInsertpart1.$sqlInsertpart2;
            //
            // $stmt = mysqli_stmt_init($conn);
            //     if(!mysqli_stmt_prepare($stmt, $sqlInsertFull))
            //     {
            //         echo "sql statement failed";
            //     }
            //     else{
            //         mysqli_stmt_bind_param($stmt, "sssssssssssss", $UserID, $vLevel, $idType, $idNumber,
            //                                                 $idName, $idDOB, $idDOI, $idDOE, $idImage, $idImageMd5, $country, $email,$phone);
            //
            //         mysqli_stmt_execute($stmt);
            //         mysqli_stmt_close($stmt);
            //             }




?>
