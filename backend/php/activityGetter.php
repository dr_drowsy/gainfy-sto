<?php

    require_once 'connection.php';
    require_once 'utility/Utility.php';

    echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

    function getActivity($conn) {
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $return = new stdClass();

        $sqlSelect = "SELECT * FROM UserActivity WHERE UniqueID = ? ORDER BY Date DESC LIMIT 35;";
        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlSelect)) {
            mysqli_stmt_bind_param($stmt, "s", $uid);
            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);

            $return->data = [];
            while ($row = mysqli_fetch_assoc($results)) {
                array_push($return->data, $row);
            }

            mysqli_stmt_close($stmt);
            mysqli_free_result($results);
        }
        else Utility::AppendToErrorLog($return, "Unable to prepare Fetch User Activity " . mysqli_error($conn));

        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }

/*$UserID= filter_input(INPUT_POST, 'UserID', FILTER_SANITIZE_STRING);

$sqlDisplayActivity ="Select * FROM UserActivity Where UniqueID= ?;";
$stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sqlDisplayActivity))
    {
        //echo "sql statement failed";
    }
    else{
        mysqli_stmt_bind_param($stmt, "s", $UserID);

       mysqli_stmt_execute($stmt);

//       mysqli_stmt_bind_result($stmt, $result);
       $results = mysqli_stmt_get_result($stmt);

       while($row = mysqli_fetch_all($results))
       {
           $arr = $row;
       }

       $arrSize = sizeof($arr);
       for($i=$arrSize-1; $i>=1; $i--)
       {
               echo'  <tr>
                      <td class="activity-time">'.$arr[$i][1].'</td>
                      <td class="activity-device">'.$arr[$i][2].'</td>
                      <td class="activity-browser">'.$arr[$i][3].'</td>
                      <td class="activity-ip">'.$arr[$i][4].'</td>
                  </tr>';

       }
       mysqli_stmt_close($stmt);
   }
mysqli_close($conn);*/
?>
