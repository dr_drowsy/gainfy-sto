<?php
    require_once "connection.php";
    require_once "./utility/Utility.php";

    echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

    //<editor-fold desc="Ethereum Transactions">
    function newEthTranx($conn) {
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $expectedReserved = filter_input(INPUT_POST, 'expectedTokens', FILTER_SANITIZE_SPECIAL_CHARS);
        $expectedEth = filter_input(INPUT_POST, 'expectedCrypto', FILTER_SANITIZE_SPECIAL_CHARS);
        $bonusRate = Utility::GetBonusRate();
        $bonus = $expectedReserved * $bonusRate;
        $totalReserved = $expectedReserved + $bonus;

        $ethPrice = Utility::GetEthExchangeRate();

        $maxWalletAmountUsd = Utility::GetMaxWalletUsd();
        $maxWalletAmountCrypto = ($maxWalletAmountUsd / (float)$ethPrice);


        //        Get existing Assigned Wallet
        $sqlSelect = "SELECT WalletAddress, TransactionStatus, LastBalance FROM EthereumWallets WHERE UniqueID = ? AND (TransactionStatus = 'Complete' OR TransactionStatus = 'Pending');";
        $stmt = mysqli_stmt_init($conn);
        $return = new stdClass();
        $return->data['expectedContribution'] = $expectedEth;
        $return->data['expectedReserve'] = $expectedReserved;
        $return->data['expectedBonus'] = $bonus;
        $return->data['totalReserved'] = $totalReserved;


        if (mysqli_stmt_prepare($stmt, $sqlSelect)) {
            mysqli_stmt_bind_param($stmt, "s", $uid);
            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);
            $return->data['result'] = $results;
            $return->errorMsg = mysqli_error($conn);

            $validWallet = array();
            while ($row = mysqli_fetch_row($results)) {
//                Check if each possible wallet is below the max stored amount
                if ($row[2] < $maxWalletAmountCrypto) {
                    array_push($validWallet, $row);
                }
            }

            if (!empty($validWallet)) {
                $assignedWallet = null;
                foreach ($validWallet as $result) {
                    if ($result[1] == "Complete") {
                        $assignedWallet = $result[0];
                    }
                }

                if (empty($assignedWallet)) {
                    //                    Didn't find wallet with Completed Tranx, grab first element which is Pending
                    $assignedWallet = $validWallet[0][0];
                    $status = $validWallet[0][1];
                    $return->rowLog = "Wallet " . $assignedWallet . " Status: " . $status;
                    $return->foundExistingWallet = true;
                    $return->data['assignedWallet'] = $assignedWallet;
                    $return->log = $return->log ?? '' . " No Completed Tranx wallet found. Grabbing first Pending Tranx wallet at address " . $assignedWallet;
                }
                else {
                    //                    Found a Wallet with Completed Tranx
                    $return->foundExistingWallet = true;
                    $return->data['assignedWallet'] = $assignedWallet;
                    $return->log = $return->log ?? '' . " Found existing wallet with Completed Tranx at address " . $assignedWallet;
                }
                mysqli_free_result($results);
                mysqli_stmt_close($stmt);
            }
            else {
                //                If a Completed Tranx address wasn't found, look for unassigned wallet to assign to this user
                mysqli_free_result($results);
                mysqli_stmt_close($stmt);
                getUnassignedWallet($conn, $uid, $return);
            }
        }
        else {
            mysqli_stmt_close($stmt);
            $return->existingWallet = false;
            $return->errorMsg = $return->errorMsg ?? '' . " SQL Statement Failed --> " . mysqli_error($conn);
        }

        //        Make sure a wallet has been assigned before writing to DB
        if (empty($return->data['assignedWallet'])) {
            $return->tranxSuccess = false;
            $return->errorMsg = $return->errorMsg ?? '' . " Error, AssignedWallet has not been set as was expected. Aborting insert. ";
        }
        else {
            //        Insert Transaction into EthereumTransactions Table
            insertEthereumTranx($conn, $uid, (float)$ethPrice, $bonusRate, $return);
        }

        //        Encode Data and Return
        mysqli_kill($conn, $conn->thread_id);
        mysqli_close($conn);
        $json = json_encode($return);
        return $json;

    }

    function &getUnassignedWallet($conn, $uid, $return) {
        $sqlSelect = "SELECT UniqueID, WalletAddress, WalletID FROM EthereumWallets WHERE (UniqueID IS NULL OR UniqueID = '');";
        $result = mysqli_query($conn, $sqlSelect);
        if (!empty($result)) {
            //            Loop through results and double check that the Wallet isn't assigned
            $newWalletId = null;
            while ($row = mysqli_fetch_assoc($result)) {
                if ($row['UniqueID'] == null || $row['UniqueID'] == '') {
                    $newWalletId = $row['WalletID'];
                    $return->foundNewWallet = true;
                    $return->data['assignedWallet'] = $row['WalletAddress'];
                    $return->log = $return->log ?? '' . " Successfully found unassigned wallet at address " . $row['WalletAddress'];
                    break;
                }
            }

            //            Update the database to reflect the new Wallet Assignment
            $sqlUpdate = "UPDATE EthereumWallets SET UniqueID = ?, TransactionStatus = 'Pending' WHERE WalletID = ?;";
            $stmt = mysqli_stmt_init($conn);

            if (mysqli_stmt_prepare($stmt, $sqlUpdate) && !empty($newWalletId)) {
                mysqli_stmt_bind_param($stmt, "ss", $uid, $newWalletId);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                $return->log = $return->log ?? '' . " Successfully updated this EthWallet assignment to user " . $uid;
            }
            else {
                //                Failed to prepare update statement
                $return->assignedNewWallet = false;
                $return->errorMsg = $return->errorMsg ?? '' . " Failed to prepare SQL update statement --> " . mysqli_error($conn);
                //                Try calling method again
                return (getUnassignedWallet($conn, $uid, $return));
            }

        }
        else {
            //            Uh oh, We've run out of new wallets
            $return->foundNewWallet = false;
            $return->errorMsg = $return->errorMsg ?? '' . " No unassigned Eth wallets could be found --> " . mysqli_error($conn);
        }

        mysqli_free_result($result);
        return $return;
    }

    function &insertEthereumTranx($conn, $uid, $ethPrice, $bonusRate ,$return) {
        $return->data['expectedRate'] = $ethPrice;


        $sqlInsert = "INSERT INTO EthereumTransactions 
(UniqueID, ReceivingAddress, ExpectedReservedTokens, ExpectedReservedBonus, ExpectedContributionAmount, DateAndTime, EthereumRate, ActualReservedTokens, ActualBonusTokens, TransactionStatus, BonusRate) 
VALUES (?, ?, ?, ?, ?, NOW(), ?, NULL, NULL, 'Pending', ?);";

        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlInsert)) {
            $assignedWallet = $return->data['assignedWallet'];
            $expectedReserve = (float)$return->data['expectedReserve'];
            $expectedBonus = (float)$return->data['expectedBonus'];
            $expectedContribution = (float)$return->data['expectedContribution'];

            mysqli_stmt_bind_param($stmt, "ssddddd", $uid, $assignedWallet, $expectedReserve, $expectedBonus, $expectedContribution, $ethPrice, $bonusRate);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            $return->tranxSuccess = true;
            $return->log = $return->log ?? '' . " Inserted New Ethereum Transaction to EthereumTransactions table with data --> [" . $uid . " " . $return->data['assignedWallet'] . ", " . $return->data['expectedReserve'] . ", " .
                $return->data['expectedBonus'] . ", " . $return->data['expectedContribution'] . ", " . $ethPrice . ", " . $bonusRate . "] ";
        }
        else {
            mysqli_stmt_close($stmt);
            $return->tranxSuccess = false;
            $return->errorMsg = $return->errorMsg ?? '' . " Unable to prepare SQL insert eth transaction statement " . mysqli_error($conn);
        }

        $qrCodeUrl = "https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=" . $return->data['assignedWallet'] . "&choe=UTF-8";
        $return->data['qrCodeUrl'] = $qrCodeUrl;

        return $return;
    }
    //</editor-fold>

    //<editor-fold desc="Bitcoin Transactions">
    function newBtcTranx($conn) {
        $return = new stdClass();
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $expectedReserved = filter_input(INPUT_POST, 'expectedTokens', FILTER_SANITIZE_SPECIAL_CHARS);
        $expectedBtc = filter_input(INPUT_POST, 'expectedCrypto', FILTER_SANITIZE_SPECIAL_CHARS);
        $bonusRate = Utility::GetBonusRate();
        $bonus = $expectedReserved * $bonusRate;
        $totalReserved = $expectedReserved + $bonus;
        $minTokens = Utility::GetMinContributionTokens();

        $btcPrice = Utility::GetBtcExchangeRate();

        $maxWalletAmountUsd = Utility::GetMaxWalletUsd();
        $maxWalletAmountCrypto = ($maxWalletAmountUsd / (float)$btcPrice);

        if ($expectedReserved < $minTokens) {
            Utility::AppendToErrorLog($return, 'Submitted contribution is below the minimum accepted value');
            $return->errorCode = ErrorCode::CONTRIBUTION_TOO_LOW;
            mysqli_close($conn);
            $json = json_encode($return);
            return $json;
        }


        //        Get existing Assigned Wallet
        $sqlSelect = "SELECT WalletAddress, TransactionStatus, LastBalance FROM BitcoinWallets WHERE UniqueID = ? AND (TransactionStatus = 'Complete' OR TransactionStatus = 'Pending');";
        $stmt = mysqli_stmt_init($conn);
        $return->data['expectedContribution'] = $expectedBtc;
        $return->data['expectedReserve'] = $expectedReserved;
        $return->data['expectedBonus'] = $bonus;
        $return->data['totalReserved'] = $totalReserved;

        if (mysqli_stmt_prepare($stmt, $sqlSelect)) {
            mysqli_stmt_bind_param($stmt, "s",$uid);
            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);
            $return->data['result'] = $results;
            $return->errorMsg = mysqli_error($conn);

            $validWallet = array();
            while ($row = mysqli_fetch_row($results)) {
                //                Check if each possible wallet is below the max stored amount
                if ($row[2] < $maxWalletAmountCrypto) {
                    array_push($validWallet, $row);
                }
            }

            if (!empty($validWallet)) {
                $assignedWallet = null;
                foreach ($validWallet as $result) {
                    if ($result[1] == "Complete") {
                        $assignedWallet = $result[0];
                    }
                }

                if (empty($assignedWallet)) {
                    //                    Didn't find wallet with Completed Tranx, grab first element which is Pending
                    $assignedWallet = $validWallet[0][0];
                    $status = $validWallet[0][1];
                    $return->rowLog = "Wallet " . $assignedWallet . " Status: " . $status;
                    $return->foundExistingWallet = true;
                    $return->data['assignedWallet'] = $assignedWallet;
                    $return->log = $return->log??'' . " No Completed Tranx wallet found. Grabbing first Pending Tranx wallet at address " . $return->data['assignedWallet'];
                }
                else {
                    //                    Found a Wallet with Completed Tranx
                    $return->foundExistingWallet = true;
                    $return->data['assignedWallet'] = $assignedWallet;
                    $return->log = $return->log??'' . " Found existing wallet with Completed Tranx at address " . $assignedWallet;
                }
                mysqli_free_result($results);
                mysqli_stmt_close($stmt);
            }
            else {
                //                If a Completed Tranx address wasn't found, look for unassigned wallet to assign to this user
                mysqli_free_result($results);
                mysqli_stmt_close($stmt);
                getUnassignedBtcWallet($conn, $uid, $return);
            }
        }
        else {
            mysqli_stmt_close($stmt);
            $return->existingWallet = false;
            $return->errorMsg = $return->errorMsg??'' . " SQL Statement Failed --> " . mysqli_error($conn);
        }

        //        Make sure a wallet has been assigned before writing to DB
        if (empty($return->data['assignedWallet'])) {
            $return->tranxSuccess = false;
            $return->errorMsg = $return->errorMsg??'' . " Error, AssignedWallet has not been set as was expected. Aborting insert. ";
        }
        else {
            //        Insert Transaction into EthereumTransactions Table
            $return = insertBitcoinTranx($conn, $uid, (float)$btcPrice, $bonusRate, $return);
        }

        //        Encode Data and Return
        mysqli_close($conn);
        $json = json_encode($return);
        return $json;

    }

    function &getUnassignedBtcWallet($conn, $uid, $return) {
        $sqlSelect = "SELECT UniqueID, WalletAddress, WalletID FROM BitcoinWallets WHERE (UniqueID IS NULL OR UniqueID = '');";
        $result = mysqli_query($conn, $sqlSelect);
        if (!empty($result)) {
            //            Loop through results and double check that the Wallet isn't assigned
            $newWalletId = null;
            while($row = mysqli_fetch_assoc($result)) {
                if ($row['UniqueID'] == null || $row['UniqueID'] == '') {
                    $newWalletId = $row['WalletID'];
                    $return->foundNewWallet = true;
                    $return->data['assignedWallet'] = $row['WalletAddress'];
                    $return->log = $return->log??'' . " Successfully found unassigned wallet at address " . $row['WalletAddress'];
                    break;
                }
            }

            //            Update the database to reflect the new Wallet Assignment
            $sqlUpdate = "UPDATE BitcoinWallets SET UniqueID = ?, TransactionStatus = 'Pending' WHERE WalletID = ?;";
            $stmt = mysqli_stmt_init($conn);

            if (mysqli_stmt_prepare($stmt, $sqlUpdate) && !empty($newWalletId)) {
                mysqli_stmt_bind_param($stmt, "ss",$uid,$newWalletId);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                $return->log = $return->log??'' . " Successfully updated this EthWallet assignment to user " . $uid;
            }
            else {
                //                Failed to prepare update statement
                $return->assignedNewWallet = false;
                $return->errorMsg = $return->errorMsg??'' . " Failed to prepare SQL update statement --> " . mysqli_error($conn);
                //                Try calling method again
                return (getUnassignedBtcWallet($conn, $uid, $return));
            }

        }
        else {
            //            Uh oh, We've run out of new wallets
            $return->foundNewWallet = false;
            $return->errorMsg = $return->errorMsg??'' . " No unassigned Eth wallets could be found --> " . mysqli_error($conn);
        }

        mysqli_free_result($result);
        return $return;
    }

    function &insertBitcoinTranx($conn, $uid, $btcPrice, $bonusRate, $return) {
        $return->data['expectedRate'] = $btcPrice;


        $sqlInsert = "INSERT INTO BitcoinTransactions 
(UniqueID, ReceivingAddress, ExpectedReservedTokens, ExpectedReservedBonus, ExpectedContributionAmount, DateAndTime, BitcoinRate, ActualReservedTokens, ActualBonusTokens, ContributionAmountReceived, TransactionStatus, BonusRate) 
VALUES (?, ?, ?, ?, ?, NOW(), ?, NULL, NULL, NULL, 'Pending', ?);";

        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlInsert)) {
            $assignedWallet = $return->data['assignedWallet'];
            $expectedReserve = (float)$return->data['expectedReserve'];
            $expectedBonus = (float)$return->data['expectedBonus'];
            $expectedContribution = (float)$return->data['expectedContribution'];

            mysqli_stmt_bind_param($stmt, "ssddddd",$uid, $assignedWallet, $expectedReserve, $expectedBonus, $expectedContribution, $btcPrice, $bonusRate);
            mysqli_stmt_execute($stmt);
            $success = mysqli_stmt_get_result($stmt);
            if ($success) {
                $return->log = $return->log??'' . " Successfully inserted Bitcoin contribution";
            }
            mysqli_stmt_close($stmt);
            $return->tranxSuccess = true;
            $return->log = $return->log ?? '' . " Inserted New Bitcoin Tranaction to BitcoinTransactions table with data --> [" . $uid . " " . $return->data['assignedWallet'] . ", " . $return->data['expectedReserve'] . ", " .
                $return->data['expectedBonus'] . ", " . $return->data['expectedContribution'] . ", " . $btcPrice . " , " . $bonusRate . "] ";
        }
        else {
            $return->tranxSuccess = false;
            $return->errorMsg = $return->errorMsg??'' . " Unable to prepare SQL insert eth transaction statement " . mysqli_error($conn);
        }

        $qrCodeUrl = "https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=" . $return->data['assignedWallet'] . "&choe=UTF-8";
        $return->data['qrCodeUrl'] = $qrCodeUrl;

        return $return;
    }
    //</editor-fold>

?>
