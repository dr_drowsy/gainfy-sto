<?php

    abstract class ErrorCode {
        const CONTRIBUTION_TOO_LOW = '20';
        const CONTRIBUTION_TOO_HIGH = '21';
        const CANT_PREPARE_STATEMENT = '22';
        const NO_BITCOIN_WALLETS = '23';
        const NO_ETHEREUM_WALLETS = '24';
    }

    abstract class Utility {

        /* Declare Constant Here */
        const ACTIVITY_LOG_DELAY = 30; // in minutes
        const CONTRIBUTION_PADDING = 5000000;
        const SOFT_CAP = 25000000;
        const HARD_CAP = 50000000;
        const DEFAULT_TOKEN_SUPPLY = 200000000;
        const BONUS_RATE = 0.01;
        const OMEGA_USD = 0.05; // Minimum wallet balance change to trigger contribution confirmation
        const USD_TO_GAIN = 2;
        const GAIN_TO_USD = 0.5;
        const URL_CHAR_LIMIT = 1900;
        const REFERRAL_BONUS = 25;
        const MAX_WALLET_USD = 5000;
        const BTC_TIME_TO_LIVE = 2; // 2 days
        const ETH_TIME_TO_LIVE = 2;
        const MIN_CONTRIBUTION_USD = 100;
//        const GAINFY_LOGO_LINK = "https://cdn-images-1.medium.com/max/1600/1*RjCPMIKLWSjdR2yhhI4AFA.png";
        const GAINFY_LOGO_LINK = "https://sto.gainfy.com/images/logo2x.png";
        const EMAIL_BG_LINK = "https://sto.gainfy.com/assets/images/bg-ath.png"; //TODO: Change this email link to an officially hosted one
        const EMAIL_HEADERS = "MIME-Version: 1.0" . "\r\n" . "Content-type:text/html;charset=UTF-8" . "\r\n" . "From: <Contribution@sto.gainfy.com>" . "\r\n";

        /* End Constants */
        public static function DebugDump($value, $message = null) {
            if (isset($message))
                echo $message;
            var_dump($value);
//            echo "<script>console.log(" . var_dump($value) . ")</script>";
        }

        public static function DebugImplode($value, $message = null) {
            if (isset($message))
                print $message;
            $display = implode((array)$value, ' ');
            echo "
<script>
console.log($display);
</script>
";
        }

        public static function DebugArray($arr) {
            if (is_array($arr)) {
                $count = 0;
                foreach ($arr as $element) {
                    echo $count.': '.$element;
                    $count++;
                }
            }
            else {
                echo "DebugArray input is not of type array";
            }
        }

        public static function GetHardCap() {
            return self::HARD_CAP;
        }

        public static function GetSoftCap() {
            return self::SOFT_CAP;
        }

        public static function GetTokenSupply() {
            return self::DEFAULT_TOKEN_SUPPLY;
        }

        public static function GetEmailLogoLink() {
            return self::GAINFY_LOGO_LINK;
        }

        public static function GetEmailBgLink() {
            return self::EMAIL_BG_LINK;
        }

        public static function GetEmailHeaders() {
            return self::EMAIL_HEADERS;
        }

        public static function SatoshiToBtc($satoshi) {
            return ((float)$satoshi / (float)(pow (10, 8)));
        }

        public static function BtcToSatoshi($btc) {
            return ((float)$btc * (float)(pow (10, 8)));
        }

        public static function WeiToEth($wei) {
            return ((float)$wei / (float)(pow (10, 18)));
        }

        public static function EthToWei($eth) {
            return ((float)$eth * (float)(pow (10, 18)));
        }

        public static function GetMinContributionTokens() {
            return (int)self::UsdToGain(self::GetMinContributionUsd());
        }

        public static function GetMinContributionUsd() {
            return (int)self::MIN_CONTRIBUTION_USD;
        }

        public static function GetBtcTtl() {
            return (int)self::BTC_TIME_TO_LIVE;
        }

        public static function GetEthTtl() {
            return (int)self::ETH_TIME_TO_LIVE;
        }

        public static function GetBtcWalletBalance($address, $raw = false) {
            $apiUrlAlt = "https://blockchain.info/de/q/addressbalance/"; // Doesn't allow multiple addresses
            $apiUrl = "https://blockchain.info/balance?active=";
            $urlQuery = $apiUrl . $address;

            $curl = curl_init($urlQuery);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curlResult = curl_exec($curl);
            $jsonResult = json_decode($curlResult);

            if ($raw)
                return $jsonResult;
            else
                return self::SatoshiToBtc($jsonResult->{$address}->{'total_received'});
        }

        public static function GetEthWalletBalance($address, $raw = false) {
            $etherscanApiKeyFooter = "&tag=latest&apikey=TBJVU6FBAWW3CHYI7NKT4BBDEHHSAEA9KF";
            $apiUrl = "https://api.etherscan.io/api?module=account&action=balance&address=";

            $urlQuery = $apiUrl . $address . $etherscanApiKeyFooter;

            $curl = curl_init($urlQuery);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curlResult = curl_exec($curl);
            $jsonResult = json_decode($curlResult);

            if ($raw)
                return $jsonResult;
            else
                return self::WeiToEth(($jsonResult->{'result'}));
        }

        public static function ExecuteBalanceCurl($urlQuery, $asArray = false) {
            $curl = curl_init($urlQuery);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $curlResult = curl_exec($curl);
            return json_decode($curlResult, $asArray);
        }

        public static function GetEthExchangeRate() {
            $curl = curl_init("https://api.coinbase.com/v2/prices/ETH-USD/spot");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curlResult = curl_exec($curl);
            $ethRate = json_decode($curlResult, true);
            curl_close($curl);
            return (float)($ethRate['data']['amount']);
        }

        public static function GetBtcExchangeRate() {
            $curl = curl_init("https://api.coinbase.com/v2/prices/BTC-USD/spot");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $curlResult = curl_exec($curl);
            $btcRate = json_decode($curlResult, true);
            curl_close($curl);
            return (float)($btcRate['data']['amount']);
        }

        public static function GetMaxWalletUsd() {
            return self::MAX_WALLET_USD;
        }

        public static function GetReferralBonus() {
            return self::REFERRAL_BONUS;
        }

        public static function &AppendToLog($return, $append) {
            if (isset($return->log)) {
                $return->log .= $append . nl2br(", \r\n");
            }
            else {
                $return->log = $append . nl2br(", \r\n");
            }
            return $return;
        }

        public static function count_array_values($my_array, $match)
        {
            $count = 0;

            foreach ($my_array as $key => $value)
            {
                if ($value == $match)
                {
                    $count++;
                }
            }

            return $count;
        }

        public static function count_array_by_key($my_array, $match)
        {
            $count = 0;

            foreach ($my_array as $key => $value)
            {
                if ($key == $match)
                {
                    $count++;
                }
            }

            return $count;
        }

        public static function &ExportToLog($return, $export) {
            if (isset($return->log)) {
                $return->log .= (var_export($export, true) . nl2br(", \r\n"));
            }
            else {
                $return->log = (var_export($export, true) . nl2br(", \r\n"));
            }
            return $return;
        }

        public static function &AppendToErrorLog($return, $append) {
            if (isset($return->errorLog)) {
                $return->errorLog .= $append . nl2br(", \r\n");
            }
            else {
                $return->errorLog = $append . nl2br(", \r\n");
            }
            return $return;
        }

        public static function GetCharLimit() {
            return self::URL_CHAR_LIMIT;
        }

        public static function UsdToGain($usd) {
            return $usd * self::USD_TO_GAIN;
        }

        public static function GainToUsd($gain) {
            return $gain * self::GAIN_TO_USD;
        }

        public static function BtcToGain($rate, $btc) {
             return (self::UsdToGain(self::BtcToUsd($rate, $btc)));
        }

        public static function EthToGain($rate, $eth) {
            return (self::UsdToGain(self::EthToUsd($rate, $eth)));
        }

        public static function GainToEth($rate, $gain) {
            return (self::UsdToEth($rate, self::GainToUsd($gain)));
        }

        public static function GainToBtc($rate, $gain) {
            return (self::UsdToBtc($rate, self::GainToUsd($gain)));
        }

        public static function BtcToUsd($rate, $btc) {
            return $btc * $rate;
        }

        public static function EthToUsd($rate, $eth) {
            return $eth * $rate;
        }

        public static function UsdToBtc($rate, $usd) {
            return $usd / $rate;
        }

        public static function UsdToEth($rate, $usd) {
            return $usd / $rate;
        }

        public static function TokensToBonus($tokens, $bonus = null) {
            return $tokens * ($bonus ?? self::BONUS_RATE);
        }

        public static function GetBonusRate() {
            return self::BONUS_RATE;
        }
        public static function GetOmegaUsd() {
            return self::OMEGA_USD;
        }
        public static function GetUsdToGain() {
            return self::USD_TO_GAIN;
        }

        // Not in use yet
        public static function prepareTranxCanceledEmail($gainfyLogo, $bgImg, $timestamp, $walletAddress, $tranxId, $expectedTokens, $expectedContribution, $crypto) {
            $tnxNo = 'GAIN' . $crypto . $tranxId;
            return <<<EOD
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Contribution Canceled</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <style>
        b, strong {
            font-weight: 600;
        }

        a:link, a:visited {
            text-decoration: none;
            color: #4185b9;
        }

        a:hover, a:focus, a:active {
            outline: 0;
            color: #46bdf4;
        }

        .background {
            font-family: "Poppins", sans-serif;
            color: #6e7571;
            font-size: 16pt;
            line-height: 1.86;
            font-weight: 300;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            position: relative;
            background: rgb(233, 244, 255);
            background-image: url('https://sto.gainfy.com/assets/images/bg-ath.png');
            background-attachment: fixed;
            background-origin: content-box;
            background-position: center;
            -webkit-background-size: 100%;
            background-size: 100%;
            min-width: 320px;
            display: flex;
            flex-flow: column;
            flex-basis: auto;
            flex-wrap: wrap;
            flex-grow: inherit;
            -webkit-flex-direction: column;
            padding: 0;
            margin: 0;
            border: none;
        }

        .email-body {
            display: flex;
            width: 100%;
            justify-content: space-evenly;
            vertical-align: auto;
            text-justify: auto;
            padding: 0;
            margin: 0;
            border: none;
        }

        .gainfy-logo {
            align-content: center;
            justify-content: center;
            display: flex;
            display: -webkit-flex;
            padding: 5px 10px 20px 10px;
            margin: auto;
        }

        .header-title {
            text-align: center;
            justify-content: center;
            align-content: center;
            padding: 5px 5px 5px 5px;
            margin: auto;
            color: #6e7571;
        }

        .header-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
        }

        .body-table {
            /*border: 2px solid black;*/
            margin: 20px auto 5px auto;
            padding: 5px 5px 5px 5px;
            justify-content: space-evenly;
            flex-direction: column;
            -webkit-flex-direction: column;
            text-align: left;
            width: 60%;
        }

        .footer-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
            padding: 8px 20px 5px 20px;
        }

        .footer-table td {
            padding: 8px;
        }

        .footer-bar {
            width: 100%;
            border: none;
            height: 2px;
            color: #6e7571;
            background-color: #6e7571;
            overflow-x: auto;
        }

        .footer-links {
            justify-content: space-evenly;
            flex-direction: row;
            -webkit-flex-direction: row;
            color: #6e7571;
            overflow-x: auto;
            text-overflow: inherit;
        }

        .upper-text, .lower-text {
            overflow-x: auto;
            text-overflow: inherit;
            text-align: left;
            width: 85%;
        }

        p {
            font-size: 14pt;
            padding: 5px 20px 15px 20px;
            text-align: inherit;
            color: #474e4a;
        }

        .data-table {
            border-top: 1px solid #6e7571;
            border-bottom: 1px solid #6e7571;
            padding: 10px;
            justify-content: center;
            align-content: center;
            -webkit-align-content: center;
            -webkit-justify-content: center;
            margin: 20px auto 20px auto;
        }

        .data-table th {
            border-bottom: 1px solid #6e7571;
            padding: 10px 0 15px 0;
            text-align: center;
            color: #6e7571;
        }

        .data-table td {
            padding: 13px 8px 5px 8px;
            text-align: left;
            color: #6e7571;
        }


        .exit-text {
            text-align: left;
            padding: 20px 0 0 0;
        }



    </style>
</head>

<body>
    <div class='background'>
        <table class='email-body'>
            <tr class='header-row'>
                <td>
                    <table class='header-table'>
                        <tr class='gainfy-logo'>
                            <td>
                                <img src='https://sto.gainfy.com/images/logo2x.png' style='height: 160px; width: 400px;' />
                            </td>
                        </tr>
                        <tr class='header-title'>
                            <td>
                                <h2><strong>Your contribution has been canceled!</strong></h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='body'>
                <td>
                    <table class='body-table'>
                        <tr class='upper-text'>
                            <td>
                                <table class='upper-text-table'>
                                    <tr class='paragraph-one'>
                                        <td>
                                            <p>Your scheduled contribution of $expectedContribution was has been manually canceled.</p>
                                        </td>
                                    </tr>
                                    <tr class='paragraph-two'>
                                        <td>
                                            <p>If you would like to reserve GAIN tokens - or have already initiated a transaction to this address, please visit the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/Tokens.html'><strong>Reserve Tokens</strong></a> page and schedule a new contribution. To view the status of all your contributions, navigate to the <a rel='noreferrer' target='_blank' href='https://sto.gainfy.com/referrals.html'><strong>Transactions Page</strong></a> and select the relevant transactions tab.
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='data'>
                            <td>
                                <table class='data-table'>
                                    <tr class='data-table-header'>
                                        <th colspan='2'><strong>Cancellation Details</strong></th>
                                    </tr>
                                    <tr>
                                        <td><strong>Tnx No:</strong></td>
                                        <td>$tnxNo</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Expected Contribution:</strong></td>
                                        <td>$expectedContribution $crypto</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Expected Tokens:</strong></td>
                                        <td>$expectedTokens GAIN</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Receiving Wallet:</strong></td>
                                        <td>$walletAddress</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Scheduled On:</strong></td>
                                        <td>$timestamp</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='lower-text'>
                            <td>
                                <table class='lower-text-table'>
                                    <tr class='lower-paragraph-one'>
                                        <td>
                                            <p>To earn extra tokens, be sure to take a look at our <a rel='noreferrer' href='https://sto.gainfy.com/referrals.html'>Referral Program</a> and get rewarded for spreading the word about Gainfy!
                                        </td>
                                    </tr>
                                    <tr class='lower-paragraph-two'>
                                        <td>
                                            <p>Don't forget to check us out on <a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'><strong>Twitter</strong></a>, contact us through our <a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'><strong> Telegram</strong></a> group, and visit the official <a rel='noreferrer' target='_blank' href='https://gainfy.com'><strong>Gainfy website</strong></a>.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='exit-text'>
                            <td>
                                <p><strong>Best wishes, <br /><br />The Gainfy Team</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='footer'>
                <td>
                    <table class='footer-table'>
                        <tr>
                            <th colspan='9'>
                                <hr class='footer-bar' />
                            </th>
                        </tr>
                        <tr class='footer-links'>
                            <td><a rel='noreferrer' target='_blank' href='https://gainfy.com'>Gainfy Foundation</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' href='mailto:team@gainfy.com'>Contact Us</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'>Twitter</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://www.linkedin.com/company/11247412/'> Linkedin</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'>Telegram</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
EOD;
        }

        public static function prepareWelcomeEmail() {
            return <<<EOD
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Welcome To Gainfy</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <style>
        b, strong {
            font-weight: 600;
        }

        a:link, a:visited {
            text-decoration: none;
            color: #4185b9;
        }

        a:hover, a:focus, a:active {
            outline: 0;
            color: #46bdf4;
        }

        .background {
            font-family: "Poppins", sans-serif;
            color: #6e7571;
            font-size: 16pt;
            line-height: 1.86;
            font-weight: 300;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            position: relative;
            height: 100vh;
            background: rgb(233, 244, 255);
            background-image: url('https://sto.gainfy.com/assets/images/bg-ath.png');
            background-attachment: fixed;
            background-origin: content-box;
            background-position: center;
            -webkit-background-size: 100%;
            background-size: 100%;
            min-width: 320px;
            display: flex;
            flex-flow: column;
            flex-basis: auto;
            flex-wrap: wrap;
            flex-grow: inherit;
            -webkit-flex-direction: column;
            padding: 0;
            margin: 0;
            border: none;
        }

        .email-body {
            display: flex;
            width: 100%;
            justify-content: space-evenly;
            vertical-align: auto;
            text-justify: auto;
            padding: 0;
            margin: 0;
            border: none;
        }

        .gainfy-logo {
            align-content: center;
            justify-content: center;
            display: flex;
            display: -webkit-flex;
            padding: 5px 10px 20px 10px;
            margin: auto;
        }

        .header-title {
            text-align: center;
            justify-content: center;
            align-content: center;
            padding: 5px 5px 5px 5px;
            margin: auto;
            color: #6e7571;
        }

        .header-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
        }

        .body-table {
            /*border: 2px solid black;*/
            margin: 20px auto 5px auto;
            padding: 5px 5px 5px 5px;
            justify-content: space-evenly;
            flex-direction: column;
            -webkit-flex-direction: column;
            text-align: left;
            width: 60%;
        }

        .footer-table {
            flex-direction: column;
            -webkit-flex-direction: column;
            justify-content: center;
            margin: auto;
            padding: 8px 20px 5px 20px;
        }

        .footer-table td {
            padding: 8px;
        }

        .footer-bar {
            width: 100%;
            border: none;
            height: 2px;
            color: #6e7571;
            background-color: #6e7571;
            overflow-x: auto;
        }

        .footer-links {
            justify-content: space-evenly;
            flex-direction: row;
            -webkit-flex-direction: row;
            color: #6e7571;
            overflow-x: auto;
            text-overflow: inherit;
        }

        .upper-text, .lower-text {
            overflow-x: auto;
            text-overflow: inherit;
            text-align: left;
            width: 85%;
        }

        p {
            font-size: 14pt;
            padding: 5px 20px 15px 20px;
            text-align: inherit;
            color: #474e4a;
        }

        .data-table {
            border-top: 1px solid #6e7571;
            border-bottom: 1px solid #6e7571;
            padding: 10px;
            justify-content: center;
            align-content: center;
            -webkit-align-content: center;
            -webkit-justify-content: center;
            margin: 20px auto 20px auto;
        }

        ol li{
            font-size: 14pt;
            padding: 2px 0 2px 0;
            font-weight: lighter;
            color: #6e7571;
        }

        .data-table th {
            border-bottom: 1px solid #6e7571;
            padding: 10px 0 15px 0;
            text-align: center;
            color: #6e7571;
        }

        .data-table td {
            padding: 13px 8px 5px 8px;
            text-align: left;
            color: #6e7571;
        }


        .exit-text {
            text-align: left;
            padding: 20px 0 0 0;
        }



    </style>
</head>

<body>
    <div class='background'>
        <table class='email-body'>
            <tr class='header-row'>
                <td>
                    <table class='header-table'>
                        <tr class='gainfy-logo'>
                            <td>
                                <img src='https://sto.gainfy.com/images/logo2x.png' style='height: 160px; width: 400px;' />
                            </td>
                        </tr>
                        <tr class='header-title'>
                            <td>
                                <h2><strong>Welcome to the Gainfy Community!</strong></h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='body'>
                <td>
                    <table class='body-table'>
                        <tr class='upper-text'>
                            <td>
                                <table class='upper-text-table'>
                                    <tr class='paragraph-one'>
                                        <td>
                                            <p>Thank you for registering for the GAIN token offering!</p>
                                        </td>
                                    </tr>
                                    <tr class='paragraph-two'>
                                        <td>
                                            <p>We hope that you're as excited about Gainfy and revolutionizing the Health Care industry as we are! Now that you've created your account, there are a few things we recommend you do:
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='data'>
                            <td>
                                <ol>
                                    <li>Submit your account information on the <a href="https://sto.gainfy.com/account.html" rel='noreferrer' target='_blank'>Account Page</a></li>
                                    <li>Provide your token receiving address on the Wallet Address tab of the <a href="https://sto.gainfy.com/account.html" rel='noreferrer' target='_blank'>Account Page</a></li>
                                    <li>Complete your KYC/AML and / or Accredited Investor info on the <a href="https://sto.gainfy.com/kyc.html" rel='https://sto.gainfy.com' target='_blank'>KYC Application Page</a></li>
                                </ol>
                            </td>
                        </tr>
                        <tr class='lower-text'>
                            <td>
                                <table class='lower-text-table'>
                                    <tr class='lower-paragraph-one'>
                                        <td>
                                            <p>To earn extra tokens, be sure to take a look at our <a rel='noreferrer' href='https://sto.gainfy.com/referrals.html'>Referral Program</a> and get rewarded for spreading the word about Gainfy!
                                        </td>
                                    </tr>
                                    <tr class='lower-paragraph-two'>
                                        <td>
                                            <p>Don't forget to check us out on <a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'><strong>Twitter</strong></a>, contact us through our <a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'><strong> Telegram</strong></a> group, and visit the official <a rel='noreferrer' target='_blank' href='https://gainfy.com'><strong>Gainfy website</strong></a>.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class='exit-text'>
                            <td>
                                <p><strong>Best wishes, <br /><br />The Gainfy Team</strong></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class='footer'>
                <td>
                    <table class='footer-table'>
                        <tr>
                            <th colspan='9'>
                                <hr class='footer-bar' />
                            </th>
                        </tr>
                        <tr class='footer-links'>
                            <td><a rel='noreferrer' target='_blank' href='https://gainfy.com'>Gainfy Foundation</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' href='mailto:team@gainfy.com'>Contact Us</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='http://www.twitter.com/gainfy'>Twitter</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://www.linkedin.com/company/11247412/'> Linkedin</a></td>
                            <td>|</td>
                            <td><a rel='noreferrer' target='_blank' href='https://t.me/gainfy_chat'>Telegram</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
EOD;
        }

    }