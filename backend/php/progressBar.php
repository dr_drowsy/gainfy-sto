<?php

    /**
     * Deprecated Code!
     * This file is no longer in use. Functionality has been integrated with tokenSaleSupply.php
     */

    require_once "connection.php";

    filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

    function getTokenSaleProgress($conn) {
        $return = new stdClass();
        $sqlSelect = "SELECT TokenSupply FROM TokenSupply;";
        if ($result = mysqli_query($conn, $sqlSelect)) {
            $return->result = $result;

            mysqli_free_result($result);
        }
        else {
            Utility::AppendToErrorLog($return, 'Unable to prepare select token supply query: ' . mysqli_error($conn) . '. ');
        }

        mysqli_close($conn);
        $json = json_encode($return);
        echo $json;
    }

$UserID= filter_input(INPUT_POST, 'UserID', FILTER_SANITIZE_STRING);

$TotalContributionsReceived= 0;
$ActualTotalReservedTokens = 0;
$ActualTotalBonusTokens = 0;

$sqlGetTransactions = "Select SUM(ContributionAmountReceived), SUM(ActualReservedTokens), SUM(ActualBonusTokens) From EthereumTransactions;";
if($result = mysqli_query($conn,$sqlGetTransactions)){

    $row = mysqli_fetch_array($result);

    $TotalContributionsReceived = $row[0];
    $ActualTotalReservedTokens =  $row[1];
    $ActualTotalBonusTokens =  $row[2];
            }
$TotalAllTokens = (float)$ActualTotalReservedTokens + (float)$ActualTotalBonusTokens;

$sqlGetSupply= "Select * From TokenSupply;";
if($ogSupply = mysqli_query($conn,$sqlGetSupply)){
$arow = mysqli_fetch_array($ogSupply);

$TokenSupply = $arow[0];
$BtcContributed = $arow[1];
$EthContributed = $arow[2];
$UsContributed = $arow[3];
}

$TokenSupply = (float)$TokenSupply - (float)$TotalAllTokens;
$newTokenSupply= number_format($TokenSupply);

$progress = ((200000000-(float)$TokenSupply)/100000000)*100;

$TotalAllTokens = number_format(14446846 + (float)$TotalAllTokens);


echo'<h4>Token Sale Progress</h4>

<div class="progress-bar">
    <div class="progress-hcap" style="width:100%">
        <div><span></span></div>
    </div>
    <div class="progress-scap" style="width:50%">
        <div>Soft cap <span>25,000,000</span></div>
    </div>
    <div class="progress-psale" style="width:'.(float)$progress.'%">
        <div>Pre Sale <span></span></div>
    </div>
    <div class="progress-percent" style="width:'.(float)$progress.'%"></div>
</div>
<ul class="progress-info">
    <li><span>Reserved - </span>'.($TotalAllTokens).'GAIN</li>
    <li><span>Available -</span> '.$newTokenSupply.' GAIN</li>
</ul>';
