<?php

    require_once 'connection.php';
    require_once 'utility/Utility.php';

    echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);

    $firstLogin = false;

    function logActivity($conn) {
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);

        $return = new stdClass();
        $deviceName = getDeviceName($_SERVER['HTTP_USER_AGENT']);
        $browserName = getBrowserName($_SERVER['HTTP_USER_AGENT']);
        $userIp = getUserIp();

        /* Select From UserActivity */
        $sqlSelect = "SELECT Date, Device, Browser, IP, IPLogNumber, NOW() - INTERVAL ? MINUTE FROM UserActivity WHERE UniqueID = ? ORDER BY Date DESC LIMIT 1;";
        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlSelect)) {
            $logDelay = Utility::ACTIVITY_LOG_DELAY;
            mysqli_stmt_bind_param($stmt, "is", $logDelay, $uid);
            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);

            $result = mysqli_fetch_all($results);
            if ($row = $result[0]) {
                $lastActivity = $row[0];
                $lastDevice = $row[1];
                $lastBrowser = $row[2];
                $lastIp = $row[3];
                $ipLogNumber = $row[4];
                $timeDelayAgo = $row[5];
//                Utility::AppendToLog($return, "lastActivity: $lastActivity, lastDevice: $lastDevice, lastBrowser: $lastBrowser, lastIp = $lastIp, ipLog = $ipLogNumber, timeDelayAgo = $timeDelayAgo");
            }
            else {
                /* First Time Login */
                Utility::AppendToLog($return, "First time login detected");
                sendWelcomeEmail($email);
            }

            mysqli_stmt_close($stmt);

            /* Push To UserActivity If: New Device, New Browser, New IP, Or Time Elapsed > Log Delay */
            if (!isset($lastActivity) || $lastIp !== $userIp || $lastDevice !== $deviceName || $lastBrowser !== $browserName || strtotime($lastActivity) < strtotime($timeDelayAgo)) {

                logNewActivity($conn, $uid, $deviceName, $browserName, $userIp, $return);
            }

        }
        else Utility::AppendToErrorLog($return, "Unable to prepare Select User Activity " . mysqli_error($conn));

        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }

    function &logNewActivity($conn, $uid, $deviceName, $browserName, $userIp, $return) {
        $sqlInsert = "INSERT INTO UserActivity (UniqueID, Date, Device, Browser, IP) VALUES (?, NOW(), ?, ?, ?);";
        $stmt = mysqli_stmt_init($conn);
        if (mysqli_stmt_prepare($stmt, $sqlInsert)) {
            mysqli_stmt_bind_param($stmt, "ssss", $uid,$deviceName, $browserName, $userIp);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);

            $date = date("Y-m-d H:m:s");
            Utility::AppendToLog($return, "Logged new user activity: Date=$date, Device=$deviceName, Browser=$browserName, IP=$userIp");
        }
        else Utility::AppendToErrorLog($return, "Unable to prepare log new activity " . mysqli_error($conn));

        return $return;
    }

    function sendWelcomeEmail($email) {
        $subject = "Gainfy Token Offering Registration";
        $emailBody = Utility::prepareWelcomeEmail();
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <Welcome@sto.gainfy.com>' . "\r\n";

        mail($email, $subject, $emailBody, $headers);
    }

    function getUserIp() {
        // Pull from Share Internet
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
        // Pull from Proxy
        else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        // Pull from Default View IP
        else $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    function getBrowserName($userAgent) {
        if (strpos($userAgent, 'Opera') || strpos($userAgent, 'OPR/')) return 'Opera';
        elseif (strpos($userAgent, 'Edge')) return 'Edge';
        elseif (strpos($userAgent, 'Chrome')) return 'Chrome';
        elseif (strpos($userAgent, 'Safari')) return 'Safari';
        elseif (strpos($userAgent, 'Firefox')) return 'Firefox';
        elseif (strpos($userAgent, 'MSIE') || strpos($userAgent, 'Trident/7')) return 'Internet Explorer';
        else return 'Other';
    }

    function getDeviceName($userAgent) {
        if (strpos($userAgent, 'Windows'))return 'Windows';
        elseif (strpos($userAgent, 'iPhone')) return 'iPhone';
        elseif (strpos($userAgent, 'Mac')) return 'Mac';
        elseif (strpos($userAgent, 'Linux')) return 'Linux';
        elseif (strpos($userAgent, 'Android')) return 'Android';
        else return 'Other';
    }

// Old Code
/*$sqlDisplayActivity ="Select `Date`, NOW() -Interval 20 Minute , `IP` FROM UserActivity Where UniqueID= ? ORDER BY `Date` DESC Limit 1;";
$stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sqlDisplayActivity))
    {
        //echo "sql statement failed";
    }
    else{
        mysqli_stmt_bind_param($stmt, "s", $UserID);

       mysqli_stmt_execute($stmt);

       mysqli_stmt_bind_result($stmt, $result);
       $results = mysqli_stmt_get_result($stmt);

       while($row = mysqli_fetch_all($results))
       {
           $arr = $row;
       }

    $lastTime = $arr[0][0];
    $currTime = $arr[0][1];
    $lastIP = $arr[0][2];
       mysqli_stmt_close($stmt);
   }*/
/*function get_user_ip(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}*/
/*function get_browser_name($user_agent)
{
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
    elseif (strpos($user_agent, 'Edge')) return 'Edge';
    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
    elseif (strpos($user_agent, 'Safari')) return 'Safari';
    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
    else return 'Other';
}*/
/*function get_device_name($user_agent)
{
    if (strpos($user_agent, 'Windows'))return 'Windows';
    elseif (strpos($user_agent, 'iPhone')) return 'iPhone';
    elseif (strpos($user_agent, 'Mac')) return 'Mac';
    elseif (strpos($user_agent, 'Linux')) return 'Linux';
    elseif (strpos($user_agent, 'Android')) return 'Android';

    return 'Other';
}*/
/*$deviceName = get_device_name($_SERVER['HTTP_USER_AGENT']);
$browserName = get_browser_name($_SERVER['HTTP_USER_AGENT']);
$userIP = get_user_ip();

if((strtotime($lastTime)<strtotime($currTime)) || ($lastIP !== $userIP) || $lastTime = '' )
{
$sqlInsertActivity ="INSERT INTO UserActivity(`UniqueID`, `Date`,`Device`,`Browser`,`IP`) Values(?, NOW(), ?, ?, ?);";
$stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sqlInsertActivity))
    {
    }
    else{
        mysqli_stmt_bind_param($stmt, "ssss", $UserID, $deviceName , $browserName, $userIP);

       mysqli_stmt_execute($stmt);
       mysqli_stmt_close($stmt);
            }
            $sqlFirstLog ="Select COUNT(`IPLogNumber`) FROM UserActivity Where UniqueID = ?;";
            $stmt = mysqli_stmt_init($conn);
                if(!mysqli_stmt_prepare($stmt, $sqlFirstLog))
                {
                    //echo "sql statement failed";
                }
                else{
                    mysqli_stmt_bind_param($stmt, 's', $UserID);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_bind_result($stmt, $result);
                    $results = mysqli_stmt_get_result($stmt);

                    while($row = mysqli_fetch_all($results))
                    {
                        $arr = $row;
                    }
                     mysqli_stmt_close($stmt);
                 }

                 $count= $arr[0][0];
                if($count == '1')
                {
                    $firstLogin = true;
                }
                if($firstLogin == true)
                {
                        $gainfyImageLink="https://cdn-images-1.medium.com/max/1600/1*RjCPMIKLWSjdR2yhhI4AFA.png";
                        $subject = "Gainfy Token Offering Registration";
                        $firstLoginEmail = '
                        <html>
                        <head>
                        <p style="font-size:22px"><strong>Welcome to the Gainfy Community!</strong></p>
                        </head>
                        <body>
                        <img src="'.$gainfyImageLink.'" style="height: 160px; width: 400px;"/>
                        <p style="font-size:18px">Thank you for registering for the GAIN Token Offering.</p>
                        <p>Be sure to <a href="https://sto.gainfy.com/kyc.html"><strong>complete your KYC/AML and /or Accredited Investor info</strong></a>
                        and to designate where you would like your tokens to be sent to by updating <a href="https://sto.gainfy.com/account.html"><strong>your return address</strong></a> on the account page!</p>
                        <p>Stop by the official <a href="https://gainfy.com/"><strong>Gainfy website</strong></a> to learn more about the sale.</p>
                        <p>Best,</p>
                        <p>The Gainfy Team</p>
                        <p>Gainfy Foundation | <a href="http://www.gainfy.com/">GAINFY</a></p>
                        <p><a href="mailto:team@gainfy.com">team@gainfy.com</a></p>
                        <p>----------------------------------------</p>
                        <p><a href="http://www.twitter.com/gainfy">Twitter</a>|<a href="https://www.linkedin.com/company/11247412/">Linkedin</a>| <a href="https://t.me/gainfy_chat">Telegram</a></p>
                        </body>
                        </html>
                        ';
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $headers .= 'From: <Welcome@sto.gainfy.com>' . "\r\n";

                       mail($Email,$subject,$firstLoginEmail,$headers);

                }


    }

mysqli_close($conn);*/

?>
