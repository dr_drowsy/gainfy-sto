<?php

    require_once(__DIR__ . '/connection.php');
    require_once(__DIR__ . '/utility/Utility.php');

    if (filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)) {
        echo filter_input(INPUT_POST, 'method', FILTER_SANITIZE_STRING)($conn);
    }

    function &updateTokenSaleSupply($conn, $returnRef = null) {
        if ($returnRef) $return = $returnRef;
        else $return = new stdClass();

        $stmt = mysqli_stmt_init($conn);
        $sqlJoinSelect = "
          SELECT 
          SUM(EthereumTransactions.ContributionAmountReceived), SUM(BitcoinTransactions.ContributionAmountReceived), 
          SUM(EthereumTransactions.ActualReservedTokens), SUM(BitcoinTransactions.ActualReservedTokens),
          SUM(EthereumTransactions.ActualBonusTokens), SUM(BitcoinTransactions.ActualBonusTokens)
          FROM EthereumTransactions
          INNER JOIN BitcoinTransactions ON BitcoinTransactions.UniqueID = EthereumTransactions.UniqueID
          WHERE BitcoinTransactions.TransactionStatus = 'Complete' OR EthereumTransactions.TransactionStatus = 'Complete';";

        if (mysqli_stmt_prepare($stmt, $sqlJoinSelect)) {
            mysqli_stmt_execute($stmt);
            $results = mysqli_stmt_get_result($stmt);
            $EthContributionReceived = 0; $BtcContributionReceived = 0; $EthReservedTokens = 0;
            $BtcReservedTokens = 0; $EthBonusTokens = 0; $BtcBonusTokens = 0;

            $all = mysqli_fetch_all($results)[0];
            $EthContributionReceived = $all[0];
            $BtcContributionReceived = $all[1];
            $EthReservedTokens = $all[2];
            $BtcReservedTokens = $all[3];
            $EthBonusTokens = $all[4];
            $BtcBonusTokens = $all[5];

            /* Padding Values */
            $UsContributionReceived = Utility::CONTRIBUTION_PADDING;
            $UsReservedTokens = Utility::UsdToGain($UsContributionReceived);
            $UsBonusTokens = $UsReservedTokens * 0.05;
            $TotalUsTokens = $UsReservedTokens + $UsBonusTokens;
            /* Padding Values */

            $TotalAllTokens = $EthReservedTokens + $BtcReservedTokens + $EthBonusTokens + $BtcBonusTokens + $TotalUsTokens;
            $DefaultTokenSupply = Utility::GetTokenSupply();
            $TokenSupply = $DefaultTokenSupply - $TotalAllTokens;

            $sqlUpdate = "UPDATE TokenSupply SET TokenSupply = ?, BtcContributed = ?, EthContributed = ?, UsContributed = ?;";
            $updateStmt = mysqli_stmt_init($conn);
            if (mysqli_stmt_prepare($updateStmt, $sqlUpdate)) {
                mysqli_stmt_bind_param($updateStmt, "dddd", $TokenSupply, $BtcContributionReceived, $EthContributionReceived, $UsContributionReceived);
                mysqli_stmt_execute($updateStmt);
            }
            else {
                Utility::AppendToErrorLog($return, 'Unable to prepare update token supply statement: ' . mysqli_error($conn) . '. ');
            }

            mysqli_stmt_close($updateStmt);
        }
        else {
            Utility::AppendToErrorLog($return, "Problem preparing join select statement. " . mysqli_error($conn) . '. ');
        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        if ($returnRef) return $return;
        else {
            $json = json_encode($return);
            return $json;
        }
    }

    function getTokenSaleSupply($conn) {
        $return = new stdClass();

        $CurrentTokenSupply = 0; $BtcContributed = 0; $EthContributed = 0; $UsContributed = 0; $DefaultSupply = Utility::GetTokenSupply();
        $sqlSelect = "SELECT * FROM TokenSupply";
        if ($result = mysqli_query($conn, $sqlSelect)) {
            $all = mysqli_fetch_all($result)[0];
            $reserved = $DefaultSupply - (float)$all[0];

            $return->TokenSupply = ((float)$all[0]);
            $return->BtcContributed = (float)$all[1];
            $return->EthContributed = (float)$all[2];
            $return->UsContributed = (float)$all[3];
            $return->Reserved = ($reserved);
            $return->Progress = (($reserved / Utility::GetHardCap()) * 100);
        }
        else {
            Utility::AppendToErrorLog($return, 'Unable to prepare select token supply query. ' . mysqli_error($conn) . '. ');
        }

//        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        $json = json_encode($return);
        return $json;
    }
?>
